CREATE TABLE public.pxy_secured_channel_configuration_keystore_details
(
    id integer NOT NULL,
    loading_method character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pxy_secured_channel_configuration_keystore_details_pkey PRIMARY KEY (id)
);

CREATE TABLE public.pxy_secured_channel_configuration_truststore_details
(
    id integer NOT NULL,
    loading_method character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pxy_secured_channel_configuration_truststore_details_pkey PRIMARY KEY (id)
);

CREATE TABLE public.pxy_secured_channel_configuration
(
    id integer NOT NULL,
    mutual_authentication boolean NOT NULL,
    keystore_details_id integer,
    truststore_details_id integer,
    CONSTRAINT pxy_secured_channel_configuration_pkey PRIMARY KEY (id),
    CONSTRAINT fk_a3wqivpldncd8qoatiyntwgiy FOREIGN KEY (truststore_details_id)
        REFERENCES public.pxy_secured_channel_configuration_truststore_details (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_a985bc1x5iqeb8uicvad77gxc FOREIGN KEY (keystore_details_id)
        REFERENCES public.pxy_secured_channel_configuration_keystore_details (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_secured_channel_configuration_protocols
(
    secured_channel_configuration_id integer NOT NULL,
    protocol_types_enabled character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT fk_k7vj6qpy9wq7oixrw9jivymrs FOREIGN KEY (secured_channel_configuration_id)
        REFERENCES public.pxy_secured_channel_configuration (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_secured_channel_configuration_cipher_suites
(
    secured_channel_configuration_id integer NOT NULL,
    cipher_suite_types_enabled character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT fk_63s4kvcff6cw2u41hrr0nwtd0 FOREIGN KEY (secured_channel_configuration_id)
        REFERENCES public.pxy_secured_channel_configuration (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.pxy_connection_tls
(
    cipher_suite_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    cipher_suite_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    handshake_notification_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    handshake_notification_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    init_certificate_subject character varying(255) COLLATE pg_catalog."default",
    protocol_between_init_proxy character varying(255) COLLATE pg_catalog."default",
    protocol_between_resp_proxy character varying(255) COLLATE pg_catalog."default",
    proxy_certificate_subject character varying(255) COLLATE pg_catalog."default",
    renegociation_counter integer,
    resp_certificate_subject character varying(255) COLLATE pg_catalog."default",
    connection_id integer NOT NULL,
    CONSTRAINT pxy_connection_tls_pkey PRIMARY KEY (connection_id),
    CONSTRAINT fk_ig03k7eql6id2vduqhivsemg7 FOREIGN KEY (connection_id)
        REFERENCES public.pxy_connection (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE SEQUENCE public.pxy_secured_channel_configuration_keystore_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.pxy_secured_channel_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.pxy_secured_channel_configuration_truststore_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.pxy_secured_channel_configuration_truststore_details OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_keystore_details OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_cipher_suites OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration OWNER to gazelle;
ALTER TABLE public.pxy_secured_channel_configuration_protocols OWNER to gazelle;
ALTER TABLE public.pxy_connection_tls OWNER to gazelle;

INSERT INTO public.pxy_secured_channel_configuration_truststore_details(id,loading_method) VALUES (1, 'JVM');
INSERT INTO public.pxy_secured_channel_configuration_keystore_details(id,loading_method) VALUES (1, 'JVM');
INSERT INTO pxy_secured_channel_configuration(id, mutual_authentication, keystore_details_id, truststore_details_id) VALUES (1, true, 1, 1);
INSERT INTO public.pxy_secured_channel_configuration_protocols VALUES (1,'TLSv1'), (1,'TLSv11'), (1,'TLSv12');
INSERT INTO public.pxy_secured_channel_configuration_cipher_suites VALUES (1,'TLS_RSA_WITH_AES_128_CBC_SHA'), (1,'TLS_DHE_RSA_WITH_AES_128_GCM_SHA256'), (1,'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256'), (1,'TLS_DHE_RSA_WITH_AES_256_GCM_SHA384'), (1,'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384');


ALTER TABLE public.tm_configuration ADD COLUMN securedChannel BOOLEAN NOT NULL DEFAULT FALSE;


-- [PROXY-229] Filter secured or not secured messages
ALTER TABLE public.pxy_connection ADD COLUMN secured BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE public.pxy_connection set secured = false;

