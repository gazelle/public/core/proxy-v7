INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'evs_client_url', 'https://gazelle.ihe.net/EVSClient/');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'storage_dicom', '/opt/proxy/DICOM');
--Not use for the moment
--INSERT INTO app_configuration (id, variable, value) VALUES (3, 'cas_service', 'http://gazelle.ihe.net/proxy/');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_works_without_cas', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_release_notes_url', 'http://gazelle.ihe.net/jira/browse/PROXY#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_documentation', 'http://gazelle.ihe.net/content/proxy-0');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_issue_tracker', 'http://gazelle.ihe.net/jira/browse/PROXY');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'proxy_ip_addresses', 'to be defined by an admin user');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'min_proxy_port', '10000');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'max_proxy_port', '11000');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'time_zone', 'Europe/Paris');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_url', 'http://localhost:8080/proxy');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cas_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'proxy_oid', '1.1.1.1.1');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dcmdump_path', '/usr/bin/dcmdump');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'jms_communication_is_enabled', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'admin_only_mode', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'link_to_cgu', 'cgu link that need to be changed');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'proxy_persistent_channels_file_path','/opt/proxy/proxyPersistentChannels.json');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'socket_service_url','http://localhost:8081/sockets');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'channel_sync_period_seconds','900');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'socket_healthcheck_period_seconds','60');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'initial_healthcheck_delay_seconds','30');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'datahouse_ui_url','http://localhost:3000/datahouse-ui');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_email','dev@test.fr');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_name','developer');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_title','developer');


SELECT pg_catalog.setval('app_configuration_id_seq', 23, true);

INSERT INTO public.pxy_secured_channel_configuration_truststore_details(id,loading_method) VALUES (1, 'JVM');
INSERT INTO public.pxy_secured_channel_configuration_keystore_details(id,loading_method) VALUES (1, 'JVM');
INSERT INTO pxy_secured_channel_configuration(id, mutual_authentication, keystore_details_id, truststore_details_id) VALUES (1, true, 1, 1);
INSERT INTO public.pxy_secured_channel_configuration_protocols VALUES (1,'TLSv1'), (1,'TLSv11'), (1,'TLSv12'), (1,'TLSv13');
INSERT INTO public.pxy_secured_channel_configuration_cipher_suites VALUES (1,'TLS_RSA_WITH_AES_128_CBC_SHA'), (1,'TLS_DHE_RSA_WITH_AES_128_GCM_SHA256'), (1,'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256'),
                                                                          (1,'TLS_DHE_RSA_WITH_AES_256_GCM_SHA384'), (1,'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384'), (1, 'TLS_AES_128_GCM_SHA256');

