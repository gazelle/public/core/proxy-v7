#!/bin/bash

# Path to the file
FILE="/opt/proxy/proxyPersistentChannels.json"

# Check if the file exists
if [ ! -f "$FILE" ]; then
    # If the file doesn't exist, create it
    touch "$FILE"
else
    # If the file exists, check and modify the content
    if grep -q "RAW" "$FILE"; then
        # Replace TcpRAW with TCP_RAW in the file
        sed -i 's/RAW/RAW_TCP/g' "$FILE"
    fi
fi
