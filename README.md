# Gazelle Proxy

<!-- TOC -->
* [Gazelle Proxy](#gazelle-proxy)
  * [Build and tests](#build-and-tests)
  * [Installation manual](#installation-manual)
    * [Configuration for sso-client-v7](#configuration-for-sso-client-v7)
  * [User manual](#user-manual)
  * [Release note](#release-note)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Installation manual

The Gazelle Proxy user manual is available here: https://gazelle.ihe.net/gazelle-documentation/Proxy/installation.html

### Configuration for sso-client-v7

As Gazelle Proxy depends on sso-client-v7, it needs to be configured properly to work. You can follow the README of sso-client-v7 
available here: [sso-client-v7](https://gitlab.inria.fr/gazelle/library/sso-client-v7)

## User manual

The Gazelle Proxy user manual is available here: https://gazelle.ihe.net/gazelle-documentation/Proxy/user.html

## Release note

The Gazelle Proxy release note is available here: https://gazelle.ihe.net/gazelle-documentation/Proxy/release-note.html
