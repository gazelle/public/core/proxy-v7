package net.ihe.gazelle.exceptions;

public class PersistentChannelManagerBeanException extends RuntimeException {

    public PersistentChannelManagerBeanException(String message) {super(message); }
}
