package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.client.ProxyClientException;
import net.ihe.gazelle.proxy.client.ProxyHttpClient;
import net.ihe.gazelle.proxy.model.ws.channel.ChannelStateException;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.channel.SocketRequest;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ChannelSyncService {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ChannelSyncService.class);


    public void syncChannels(ProxyChannelRefList proxyChannelRefList, ProxyHttpClient proxyHttpClient){
        List<ProxyChannel> channelsFromService = proxyHttpClient.getAllStartedChannels();

        logger.info("Channels from proxy: {}", proxyChannelRefList.getProxyChannels());

        List<ProxyChannel> missingInChannelService = getDiff(proxyChannelRefList.getProxyChannels(), channelsFromService);
        if (!missingInChannelService.isEmpty()) {
            logger.warn("Missing {} channel(s) in Socket Service from Proxy: {}, starting channels...", missingInChannelService.size(), missingInChannelService);
            SocketRequest socketRequest = SocketRequest.buildFromChannels(missingInChannelService);
            List<ProxyChannel> startedChannels = proxyHttpClient.startChannels(socketRequest);
            assertProxyChannelsCreated(startedChannels, missingInChannelService);
            logger.info("Channels {} started in Socket Service", missingInChannelService);
        }
        else{
            logger.info("All channels are present in proxy");
        }
        List<ProxyChannel> missingInProxy = getDiff(channelsFromService, proxyChannelRefList.getProxyChannels());
        if (!missingInProxy.isEmpty()) {
            logger.warn("Missing {} channel(s) in Proxy from Socket Service: {}, deleting channels...", missingInProxy.size(), missingInProxy);
            for (ProxyChannel proxyChannel : missingInProxy) {
                proxyHttpClient.stopChannel(proxyChannel.getProxyPort());
            }
            logger.info("Channels {} deleted in Proxy", missingInProxy);
        }
        else{
            logger.info("All channels are present in Socket Service");
        }
    }

    private List<ProxyChannel> getDiff(Iterable<? extends ProxyChannel> searchingElements, Iterable<? extends ProxyChannel> baseElements) {
        List<ProxyChannel> result = new ArrayList<>();
        for (ProxyChannel proxyChannel : searchingElements) {
            if (!channelExistInList(proxyChannel, baseElements)) {
                result.add(proxyChannel);
            }
        }
        return result;
    }

    private boolean channelExistInList(ProxyChannel proxyChannel, Iterable<? extends ProxyChannel> channelsFromService) {
        for (ProxyChannel channelFromService : channelsFromService) {
            if (channelFromService.getProxyPort() == proxyChannel.getProxyPort()) {
                return true;
            }
        }
        return false;
    }


    public void assertProxyChannelsCreated(List<? extends ProxyChannel> actual, List<? extends ProxyChannel> expected){
        StringBuilder sb = new StringBuilder();
        for (ProxyChannel channel : expected) {
            if(!actual.contains(channel)){
                sb.append(channel.getProxyPort());
                sb.append(",");
            }
        }
        if(sb.length() > 0){
            sb.deleteCharAt(sb.length() - 1);
            throw new ChannelStartException("Could not start channel(s): "+ sb);
        }
    }
}
