/**
 * Copyright 2009 IHE International (http://www.ihe.net)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.action;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.proxy.admin.model.ChannelConfiguration;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.client.ChannelStopException;
import net.ihe.gazelle.proxy.client.ProxyClientException;
import net.ihe.gazelle.proxy.client.SocketServiceStatus;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJsonService;
import net.ihe.gazelle.proxy.managers.KeyStoreManager;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import net.ihe.gazelle.proxy.model.channel.ChannelType;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;
import net.ihe.gazelle.proxy.model.ws.Actor;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;
import net.ihe.gazelle.proxy.model.ws.UnexpectedError;
import net.ihe.gazelle.proxy.model.ws.channel.ChannelStateException;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Redirect;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Name("channelManagerBean")
@Scope(ScopeType.PAGE)
public class ChannelManagerBean implements Serializable {

    private static final long serialVersionUID = 1879075873564126607L;

    private static final Logger log = LoggerFactory.getLogger(ChannelManagerBean.class);
    public static final String NO_PERMISSION_STRING = "You do not have the permission to do this";
    public static final String FAILED_TO_START_CHANNEL_ON_PORT = "Failed to start channel on port ";
    public static final String COULD_NOT_CONNECT_TO_SOCKET_SERVICE = "Could not connect to socket service";
    @In
    Identity identity;
    @In
    protected transient ProxyLocal proxyBean;
    private transient ChannelConfiguration channelConfiguration;
    private int channelToStop;

    @In
    protected transient SecuredChannelConfigurationManager securedChannelConfigurationManager;


    @In
    protected PersistentChannelManagerBean persistentChannelManagerBean;
    
    @In
    protected transient ProxyChannelDTOJsonService proxyChannelDTOJsonService;

    private transient TLSConfigurationService tlsConfigurationService;

    @In
    protected transient ProxyChannelRefList proxyChannelRefList;


    protected SecuredChannelConfiguration securedChannelConfiguration;
    protected transient KeyStoreManager keyStoreManager;
    protected transient TrustStoreManager trustStoreManager;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Map<String, String> additionalParameters = new HashMap<>();

    private boolean sniEnabled = true ;
    private final List<ProxyChannelDTOJson> failedChannels = new ArrayList<>();


    private final int minProxyPort = ApplicationConfigurationManager.instance().getMinProxyPort();
    private final int maxProxyPort = ApplicationConfigurationManager.instance().getMaxProxyPort();

    private String uploadErrorMessage = "";

    private boolean uploadPerformed = false;



    @Create
    public void init() {
        this.channelConfiguration = new ChannelConfiguration();
        this.channelConfiguration.setProxyProviderPort(ApplicationConfigurationManager.instance().getMinProxyPort());
        this.securedChannelConfiguration = securedChannelConfigurationManager.find();
        this.isValidKeyStore();
        this.isValidTrustStore();
        this.tlsConfigurationService = new TLSConfigurationService();
    }

    public boolean isValidTrustStore() {
        try {
            this.trustStoreManager = securedChannelConfigurationManager.getTrustStore(securedChannelConfiguration.getTrustStoreDetails());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isValidKeyStore() {
        try {
            this.keyStoreManager = securedChannelConfigurationManager.getKeyStore(securedChannelConfiguration.getKeyStoreDetails());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ChannelType getMessageType() {
        return this.channelConfiguration.getChannelType();
    }


    public boolean isSecureChannel() {
        return channelConfiguration.isSecureChannel();
    }

    public boolean isPersistent() { return channelConfiguration.isPersistent(); }

    public void setSecureChannel(boolean secureChannel) {
        this.channelConfiguration.setSecureChannel(secureChannel);
    }

    public void setPersistent(boolean persistent) {
        this.channelConfiguration.setPersistent(persistent);
    }


    public void setMessageType(ChannelType messageType) {
        this.channelConfiguration.setChannelType(messageType);
    }

    public List<ChannelType> getMessageTypes() {
        return ChannelType.getApplicativeValues();
    }

    public Integer getProxyPort() {
        return this.channelConfiguration.getProxyProviderPort();
    }

    public void setProxyPort(Integer proxyPort) {
        this.channelConfiguration.setProxyProviderPort(proxyPort);
    }

    public String getResponderIP() {
        return this.channelConfiguration.getProxyConsumerHost();
    }

    public void setResponderIP(String responderIP) {
        this.channelConfiguration.setProxyConsumerHost(responderIP.trim());
    }

    public Integer getResponderPort() {
        return this.channelConfiguration.getProxyConsumerPort();
    }

    public void setResponderPort(Integer responderPort) {
        this.channelConfiguration.setProxyConsumerPort(responderPort);
    }

    public void setChannelToStop(int proxyPort) {
        channelToStop = proxyPort;
    }

    public void setHttpRewrite(boolean httpRewrite) {
        if(httpRewrite) {
            additionalParameters.put(HttpMessageDTO.HTTP_REWRITE_KEY, "true");
        } else {
            additionalParameters.remove(HttpMessageDTO.HTTP_REWRITE_KEY);
        }
    }

    public boolean isHttpRewriteSupported() {
        return channelConfiguration.getChannelType() == ChannelType.HTTP;
    }

    public boolean getHttpRewrite() {
        return "true".equals(additionalParameters.get(HttpMessageDTO.HTTP_REWRITE_KEY));
    }

    public void setSniEnabled(boolean sniEnabled) {
        this.sniEnabled = sniEnabled;
    }

    public boolean getSniEnabled() {
        return sniEnabled;
    }

    public List<ProxyChannelDTOJson> getFailedChannels() {
        return failedChannels;
    }

    public boolean showFailedChannels(){
        return !failedChannels.isEmpty();
    }

    public boolean uploadIsFailed(){
        return uploadErrorMessage != null && !uploadErrorMessage.isEmpty();
    }

    public boolean isUploadPerformed() {
        return uploadPerformed;
    }

    public String getUploadErrorMessage() {
        return uploadErrorMessage;
    }

    public ChannelManagerBean setUploadErrorMessage(String uploadErrorMessage) {
        this.uploadErrorMessage = uploadErrorMessage;
        return this;
    }


    /// EXPOSED PUBLIC METHODS

    public String startChannel() {
        if (isAllowedAsAdmin()) {
            ProxyChannel proxyChannel = buildProxyChannel();
            try{
                startChannel(proxyChannel);
                if (isPersistent()) {
                    saveChannel(proxyChannel);
                }
                return "/channels.xhtml";
            }
            catch (Exception e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to start channel: " + e.getMessage());
                log.error(e.getMessage(), e);
                return null;
            }

        } else {
            FacesMessages.instance().add(Severity.ERROR, NO_PERMISSION_STRING);
        }
        return null;
    }

    /**
     * Stops and delete channel from JSON file
     * @param localPort port of the channel to stop
     */
    public void stopChannel(int localPort) {
        if (isAllowedAsAdmin()) {
            try{
                unPersistChannel(localPort);
                stopChannelAndRemoveFromList(localPort);
            }
            catch (ChannelStopException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Failed to stop channel with port " + localPort );
                log.error("Failed to stop channel with port " + localPort, e);
            }
            catch (ProxyClientException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        COULD_NOT_CONNECT_TO_SOCKET_SERVICE);
                log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            }

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, NO_PERMISSION_STRING);
        }
    }

    public GazelleListDataModel<ProxyChannel> getListOfChannels() {
        List<ProxyChannel> res = new ArrayList<>(proxyChannelRefList.getSrotedProxyChannelsList());
        return new GazelleListDataModel<>(res);
    }


    public void channelUploaderConfig(FileUploadEvent event){
        uploadChannels(event);
    }

    public void portToStop(int portToStop) {
        if (portToStop > 0) {
            setChannelToStop(portToStop);
        }
    }

    public void stopChannelReal() {
        stopChannel(channelToStop);
    }

    public void stopAndDeleteAllChannels() {
        if (!channelListIsEmpty()) {
            try{
                proxyBean.stopAllChannels();
                proxyChannelRefList.clear();
                persistentChannelManagerBean.deleteAllChannels();
                FacesMessages.instance().clearGlobalMessages();
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "All channels are now closed !");
            }
            catch (ChannelStopException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to stop all channels : " + e.getMessage());
                log.error(e.getMessage(), e);
            }
            catch (ProxyClientException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        COULD_NOT_CONNECT_TO_SOCKET_SERVICE);
                log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            }
        } else {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR, "Failed to stop channel : no proxy channel open !");
        }
    }

    public void stopAllUnPersistedChannels(){
        if(!channelListIsEmpty()){
            try{
                for(ProxyChannel proxyChannel : proxyChannelRefList.getProxyChannels()){
                    if(!proxyChannel.isPersistent()){
                        stopChannelAndRemoveFromList(proxyChannel.getProxyPort());
                    }
                }
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "All temporary channels are now closed !");
            }
            catch (ChannelStopException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Failed to stop all temporary channels : " + e.getMessage());
                log.error(e.getMessage(), e);
            }
            catch (ProxyClientException e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        COULD_NOT_CONNECT_TO_SOCKET_SERVICE);
                log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            }
        }
    }

    public void unPersistChannel(int port){
        try{
            ProxyChannel proxyChannel = proxyChannelRefList.getProxyChannel(port);
            if(proxyChannel != null){
                persistentChannelManagerBean.deleteChannel(port);
                proxyChannel.setPersistent(false);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Channel with port " + port + " became temporary");
            }
        }
        catch (Exception e){
            FacesMessages.instance().add(Severity.ERROR,
                    "Failed to un-persist channel " + port);
            log.error(e.getMessage(), e);
        }
    }

    public boolean isSocketServiceConnected(){
        return SocketServiceStatus.INSTANCE.isConnected();
    }




    public void syncWithSocketService(){
        try{
            proxyBean.syncChannels(proxyChannelRefList);
        }
        catch (ChannelStartException | ChannelStopException e){
            log.error("Cannot sync channels",e);
            FacesMessages.instance().add(Severity.ERROR, "Cannot sync channels: "+e.getMessage());
            Redirect redirect = Redirect.instance();
            redirect.setViewId("/channels.xhtml");
            redirect.execute();
        }
        catch (ProxyClientException e) {
            log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Could not connect to socket service: " + e.getMessage());
        }

    }

    public void downloadFailedChannelStackTrace(ProxyChannelDTOJson proxyChannelDTOJson){
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                .getResponse();
        ServletOutputStream servletOutputStream = null;
        response.setContentType("text/plain");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=\"failedChannelStackTrace.txt\"");
        try {
            servletOutputStream = response.getOutputStream();
            OutputStreamWriter streamWriter = new OutputStreamWriter(servletOutputStream, StandardCharsets.UTF_8);
            streamWriter.write(constructStacktrace(proxyChannelDTOJson.getUnexpectedError()));
            streamWriter.close();
            servletOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public boolean channelListIsEmpty() {
        Set<ProxyChannel> lp = proxyChannelRefList.getProxyChannels();
        return (lp == null || lp.isEmpty());
    }


    public void download(){
        if(!channelListIsEmpty()){
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            ServletOutputStream servletOutputStream = null;
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=\"proxyChannels.json\"");
            try {
                servletOutputStream = response.getOutputStream();
                OutputStreamWriter streamWriter = new OutputStreamWriter(servletOutputStream, StandardCharsets.UTF_8);
                objectMapper
                        .writerWithDefaultPrettyPrinter()
                        .writeValue(streamWriter, proxyChannelDTOJsonService.convertToProxyDTOJsons(proxyChannelRefList.getSrotedProxyChannelsList()));
                streamWriter.close();
                servletOutputStream.close();
                FacesContext.getCurrentInstance().responseComplete();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public void goHome() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/home.seam");
        } catch (IOException e) {
            log.error("Redirection failed", e);
        }
    }

    //////// MAIN INTERNAL METHODS TO START CHANNELS [ONLY ONES]

    private void startChannel(ProxyChannel proxyChannel) {
        if(!isAllowedAsAdmin()){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, NO_PERMISSION_STRING);
            throw new IllegalStateException(NO_PERMISSION_STRING);
        }
        if (proxyChannel.getProxyPort() < minProxyPort || proxyChannel.getProxyPort() > maxProxyPort) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Port " + proxyChannel.getProxyPort() + " is out of range [" + minProxyPort + ", " + maxProxyPort
                            + "]");
            throw new IllegalStateException("Port " + proxyChannel.getProxyPort() + " is out of range [" + minProxyPort + ", " + maxProxyPort
                    + "]");
        }
        if(proxyChannelRefList.contains(proxyChannel.getProxyPort())){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Port " + proxyChannel.getProxyPort() + " is already used");
            throw new IllegalStateException("Port " + proxyChannel.getProxyPort() + " is already used");
        }
        try {
            proxyBean.startChannel(proxyChannel);
            proxyChannelRefList.addProxyChannel(proxyChannel);
            FacesMessages.instance().add(Severity.INFO, "Channel started");
        } catch (ChannelStartException e) {
            log.error(FAILED_TO_START_CHANNEL_ON_PORT + proxyChannel.getProxyPort(), e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    FAILED_TO_START_CHANNEL_ON_PORT + proxyChannel.getProxyPort() + ": " + e.getMessage());
        }
        catch (ProxyClientException e) {
            log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Could not connect to socket service: " + e.getMessage());
        }
    }

    private void stopChannelAndRemoveFromList(int localPort){
        proxyBean.stopChannel(localPort);
        proxyChannelRefList.remove(localPort);

    }

    private void removeChannelIfPersistent(int localPort){
        ProxyChannel proxyChannel = proxyChannelRefList.getProxyChannel(localPort);
        if(proxyChannel != null && proxyChannel.isPersistent()){
            persistentChannelManagerBean.deleteChannel(localPort);
        }
    }

    private void handleUploadedChannel(ProxyChannelDTOJson proxyChannelDTOJson) {
        if (proxyChannelDTOJson.getProxyPort() < minProxyPort || proxyChannelDTOJson.getProxyPort() > maxProxyPort) {
            failedChannels.add(
                    proxyChannelDTOJson
                            .setUnexpectedError(createUnexpectedError(new Exception("Port " + proxyChannelDTOJson.getProxyPort() + " is out of range [" + minProxyPort + ", " + maxProxyPort
                                    + "]")))
            );
            return;
        }
        if(proxyChannelRefList.contains(proxyChannelDTOJson.getProxyPort())){
            failedChannels.add(
                    proxyChannelDTOJson
                            .setUnexpectedError(createUnexpectedError(new Exception("Port " + proxyChannelDTOJson.getProxyPort() + " is already used")))
            );
            return;
        }
        try{
            ProxyChannel proxyChannel = proxyChannelDTOJsonService.convertToProxyChannel(proxyChannelDTOJson);
            proxyBean.startChannel(proxyChannel);
            proxyChannelRefList.addProxyChannel(proxyChannel);
            if(proxyChannelDTOJson.isPersistent()){
                saveJsonChannel(proxyChannelDTOJson);
            }
        }
        catch (ChannelStartException e){
            failedChannels.add(
                    proxyChannelDTOJson
                            .setUnexpectedError(createUnexpectedError(e))
            );
        }
        catch (ProxyClientException e) {
            log.error(COULD_NOT_CONNECT_TO_SOCKET_SERVICE, e);
            failedChannels.add(
                    proxyChannelDTOJson
                            .setUnexpectedError(createUnexpectedError(e))
            );
        }
    }




    ////////// PRIVATE METHODS

        //////// BUILDERS

    private ProxyChannel buildProxyChannel(){
        String resolvedResponderIp = proxyChannelDTOJsonService.resolveIpIfHost(getResponderIP());
        log.warn("Resolved responder ip {} to {}", getResponderIP(), resolvedResponderIp);
        ProxyChannel proxy = new ProxyChannel()
                .setProxyPort(getProxyPort())
                .setType(getMessageType())
                .setResponder(
                        new Actor()
                                .setIp(resolvedResponderIp)
                                .setHostname(getResponderIP())
                                .setPort(getResponderPort())
                )
                .setAdditionalParameters(additionalParameters)
                .setPersistent(isPersistent())
                ;
        if(isSecureChannel()){
            TLSConfiguration tlsConfiguration = tlsConfigurationService.createTLSConfiguration(securedChannelConfigurationManager);
            tlsConfiguration.setSniEnabled(sniEnabled);
            proxy.setTlsConfiguration(tlsConfiguration);
        }
        return proxy;
    }

    private List<ParameterDTO> buildAdditionalParameters() {
        List<ParameterDTO> parametersList = new ArrayList<>();
        if(!channelConfiguration.getChannelType().equals(ChannelType.HTTP)) {
            return parametersList;
        }
        for (Map.Entry<String, String> entry : additionalParameters.entrySet()) {
            parametersList.add(
                    new ParameterDTO()
                            .setKey(entry.getKey())
                            .setValue(entry.getValue())
            );
        }
        return parametersList;
    }


    private String constructStacktrace(UnexpectedError unexpectedError){
        StringBuilder stringBuilder = new StringBuilder("caued by: " + unexpectedError.getMessage() + "\n");
        if(unexpectedError.getCause() != null){
            stringBuilder.append("caused by: ").append(constructStacktrace(unexpectedError.getCause())).append("\n");
        }
        return stringBuilder.toString();
    }


    private UnexpectedError createUnexpectedError(Throwable e) {
        UnexpectedError unexpectedError = new UnexpectedError();
        unexpectedError.setMessage(e.getMessage());
        if(e.getCause() != null) {
            unexpectedError.setCause(createUnexpectedError(e.getCause()));
        }
        return unexpectedError;
    }

    //////// CHECKERS
    private boolean isAllowedAsAdmin() {
        ApplicationConfigurationManagerLocal manager = ApplicationConfigurationManager.instance();
        return manager.isUserAllowedAsAdmin();
    }



    //////// UPLOAD CHANNELS

    private void uploadChannels(FileUploadEvent event){
        failedChannels.clear();
        uploadErrorMessage = "";
        uploadPerformed = true;
        if(!isAllowedAsAdmin()){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, NO_PERMISSION_STRING);
            uploadErrorMessage = NO_PERMISSION_STRING;
            return;
        }
        if(event.getUploadedFile().getContentType().contains("json")){
            try {
                if(event.getUploadedFile().getData() == null || event.getUploadedFile().getData().length == 0){
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Uploaded file is empty");
                    uploadErrorMessage = "Uploaded file is empty";
                    return;
                }
                ProxyChannelDTOJson[] proxyChannelDTOJsons = objectMapper.readValue(event.getUploadedFile().getData(), ProxyChannelDTOJson[].class);
                for (ProxyChannelDTOJson proxyChannelDTOJson : proxyChannelDTOJsons) {
                    handleUploadedChannel(proxyChannelDTOJson);
                }
                if(!failedChannels.isEmpty()){
                    uploadErrorMessage = "Not all channels have been started. Failed to start "+ failedChannels.size() + " channels " +
                            "and started " + (proxyChannelDTOJsons.length - failedChannels.size()) + " channels";

                }
            } catch (IOException e) {
                FacesMessages.instance().add(Severity.ERROR, "Failed to read uploaded file: " + e.getMessage());
                uploadErrorMessage = "Failed to read uploaded file: " + e.getMessage();
                log.error(e.getMessage(), e);
            }
        } else {
            FacesMessages.instance().add(Severity.ERROR, "Uploaded file is not a json file");
            uploadErrorMessage = "Uploaded file is not a json file";
        }

    }




    //////// SAVE CHANNELS

    private void saveChannel(ProxyChannel proxyChannel) {
        try{
            persistentChannelManagerBean.saveChannel(proxyChannel);
        }
        catch (Exception e){
            FacesMessages.instance().add(Severity.ERROR,
                    "Failed to save persistent channel " + proxyChannel.getProxyPort());
            log.error(e.getMessage(), e);
        }
    }

    private void saveJsonChannel(ProxyChannelDTOJson jsonChannel){
        try{
            persistentChannelManagerBean.saveChannel(jsonChannel);
        }
        catch (Exception e){
            FacesMessages.instance().add(Severity.ERROR,
                    "Failed to save json persistent channel " + jsonChannel.getProxyPort());
            log.error(e.getMessage(), e);
            throw e;
        }
    }




}
