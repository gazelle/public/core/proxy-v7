package net.ihe.gazelle.proxy.action;

public class ChannelSaveException extends RuntimeException{

    public ChannelSaveException(String message) {
        super(message);
    }

    public ChannelSaveException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelSaveException(Throwable cause) {
        super(cause);
    }

    public ChannelSaveException() {
        super();
    }
}
