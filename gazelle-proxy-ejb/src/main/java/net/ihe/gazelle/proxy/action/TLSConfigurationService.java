package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.managers.KeyStoreManager;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import net.ihe.gazelle.proxy.model.tls.keystore.CipherSuiteType;
import net.ihe.gazelle.proxy.model.tls.keystore.ProtocolType;
import net.ihe.gazelle.proxy.model.ws.tls.Certificate;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;
import net.ihe.gazelle.proxy.model.ws.tls.TLSCredentialsDTO;
import sun.misc.BASE64Encoder;
import sun.security.provider.X509Factory;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TLSConfigurationService {

    public TLSConfiguration createTLSConfiguration(SecuredChannelConfigurationManager securedChannelConfigurationManager) {
        SecuredChannelConfiguration securedChannelConfiguration = securedChannelConfigurationManager.find();
        KeyStoreManager keyStoreManager = securedChannelConfigurationManager.getKeyStore(securedChannelConfiguration.getKeyStoreDetails());
        TrustStoreManager trustStoreManager = securedChannelConfigurationManager.getTrustStore(securedChannelConfiguration.getTrustStoreDetails());
        return new TLSConfiguration()
                .setSupportedCipherSuites(cypherSuitesAsArray(securedChannelConfiguration.getCipherSuiteTypesEnabled()))
                .setTlsCredential(
                        new TLSCredentialsDTO()
                                .setCertificateChain(createCertificates(keyStoreManager.getCertificateChain(null)))
                                .setPrivateKey(privateKeyPEMEncoder(keyStoreManager.getPrivateKey(null).getEncoded()))
                )
                .setSupportedProtocols(protocolsAsArray(securedChannelConfiguration.getProtocolTypesEnabled()))
                .setTrustedCerts(createCertificates(trustStoreManager.getTrustedCertificates()))
                .setMTLS(securedChannelConfiguration.isMutualAuthentication())
                ;

    }

    private List<Certificate> createCertificates(List<X509Certificate> certificates) {
        List<Certificate> res = new ArrayList<>();
        for (X509Certificate x509Certificate : certificates) {
            try {
                res.add(new Certificate().setCertPEM(certificatePEMEncoder(x509Certificate.getEncoded())));
            } catch (CertificateEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return res;
    }

    private List<Certificate> createCertificates(X509Certificate[] trustedCertificates) {
        return createCertificates(Arrays.asList(trustedCertificates));
    }

    private byte[] certificatePEMEncoder(byte[] encoded){
        String result = X509Factory.BEGIN_CERT + "\n";
        BASE64Encoder encoder = new BASE64Encoder();
        result += encoder.encodeBuffer(encoded) + "\n";
        result += X509Factory.END_CERT;
        return result.getBytes();
    }

    private byte[] privateKeyPEMEncoder(byte[] encoded){
        String result = "-----BEGIN PRIVATE KEY-----\n";
        BASE64Encoder encoder = new BASE64Encoder();
        result += encoder.encodeBuffer(encoded) + "\n";
        result += "-----END PRIVATE KEY-----\n";
        return result.getBytes();
    }




    private String[] cypherSuitesAsArray(List<CipherSuiteType> cipherSuiteTypesEnabled) {
        String[] res = new String[cipherSuiteTypesEnabled.size()];
        int i = 0;
        for (CipherSuiteType cipherSuiteType : cipherSuiteTypesEnabled) {
            res[i] = cipherSuiteType.toString();
            i++;
        }
        return res;
    }

    private String[] protocolsAsArray(List<ProtocolType> protocolTypesEnabled) {
        String[] res = new String[protocolTypesEnabled.size()];
        int i = 0;
        for (ProtocolType protocolType : protocolTypesEnabled) {
            res[i] = protocolType.getName();
            i++;
        }
        return res;
    }

}
