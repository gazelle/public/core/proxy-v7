package net.ihe.gazelle.proxy.action;

public class ChannelDeleteException extends RuntimeException{

    public ChannelDeleteException(String message) {
        super(message);
    }

    public ChannelDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelDeleteException(Throwable cause) {
        super(cause);
    }
}
