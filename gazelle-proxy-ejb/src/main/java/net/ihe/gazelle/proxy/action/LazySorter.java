package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.channel.SocketRequest;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;

public class LazySorter implements Comparator<ProxyChannel> {

    private static final Logger log = LoggerFactory.getLogger(LazySorter.class);

    private final String sortField;

    private final SortOrder sortOrder;

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(ProxyChannel conf1, ProxyChannel conf2) {
        try {
//            Object value1 = Proxy.class.getField(this.sortField).get(conf1);
//            Object value2 = ProxyImpl.class.getField(this.sortField).get(conf2);

            Object value1 = SocketRequest.class.getField(this.sortField).get(conf1);
            Object value2 = SocketRequest.class.getField(this.sortField).get(conf2);

            log.info(sortField + " 1 original : " + value1 + " port : " + conf1.getProxyPort());
            log.info(sortField + " 2 original : " + value2 + " port : " + conf2.getProxyPort());
            log.info(sortField + " 1 init : " + value1 + " port : " + conf1.getProxyPort());
            log.info(sortField + " 2 init : " + value2 + " port : " + conf2.getProxyPort());
            if (value1 == null || value2 == null) {
                throw new IllegalArgumentException("cannot find sortField");
            }

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (Exception e) {
            log.error("" + e);
            throw new RuntimeException();
        }
    }
}
