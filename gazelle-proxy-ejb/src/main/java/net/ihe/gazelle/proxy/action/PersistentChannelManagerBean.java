package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.proxy.admin.model.ChannelConfiguration;
import net.ihe.gazelle.proxy.application.channel.ProxyChannelPersistenceService;
import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.client.ChannelStopException;
import net.ihe.gazelle.proxy.client.ProxyClientException;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJsonService;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Startup
@Scope(ScopeType.APPLICATION)
@Name("persistentChannelManagerBean")
public class PersistentChannelManagerBean {

    private static final long serialVersionUID = 4640937192454301370L;
    private static Logger log = LoggerFactory.getLogger(PersistentChannelManagerBean.class);

    private final ApplicationConfigurationManagerLocal managerLocal = new ApplicationConfigurationManager();
    private final String fileLocation = managerLocal.getProxyPersistantChannelsFilePath();
    private final File persistentJsonFile = new File(fileLocation);

    private Map<Integer, ChannelConfiguration> mapOfPersistentChannels;

    private List<ChannelConfiguration> listOfPersistentChannels;
    private Set<Integer> listOfPort;

    @In
    ProxyLocal proxyBean;

    @In
    ProxyChannelDTOJsonService proxyChannelDTOJsonService;

    @In
    ProxyChannelPersistenceService proxyChannelPersistenceService;

    @In
    private ProxyChannelRefList proxyChannelRefList;



    @Create
    public void init() {
        this.startPersistedChannels();
    }

    private void startPersistedChannels() {
        List<ProxyChannel> persistedChannels = proxyChannelDTOJsonService.convertToProxyChannels(proxyChannelPersistenceService.findAll());
        try{
            proxyBean.stopAllChannels();
            proxyBean.startChannels(persistedChannels);
        }
        catch (ChannelStartException | ChannelStopException e){
            log.error("Could not stop & start all persisted channels", e);
        }
        catch (ProxyClientException e) {
            log.error("Could not connect to socket service", e);
        }
        finally {
            // We add the channels to the list even if the socket service is not connected to retry the operation in the next sync
            proxyChannelRefList.addAll(persistedChannels);
        }
    }

    public void saveChannel(ProxyChannel proxyChannel) {
        try {
            proxyChannelPersistenceService.save(new ProxyChannelDTOJson(proxyChannel));
        } catch (Exception e) {
            throw new ChannelSaveException("Could not save DTO of ProxyChannel", e);
        }
    }

    public void saveChannel(ProxyChannelDTOJson proxyChannel) {
        try {
            proxyChannelPersistenceService.save(proxyChannel);
        } catch (Exception e) {
            throw new ChannelSaveException("Could not save DTO of ProxyChannel", e);
        }
    }

    public void deleteChannel(int port) {
        try {
            proxyChannelPersistenceService.delete(port);
        } catch (Exception e) {
            throw new ChannelDeleteException("Could not delete channel with port: "+port, e);
        }
    }

    public void deleteAllChannels() {
        try {
            proxyChannelPersistenceService.deleteAll();
        } catch (Exception e) {
            throw new ChannelDeleteException("Could not delete all channels", e);
        }
    }




}
