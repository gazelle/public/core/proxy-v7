package net.ihe.gazelle.proxy.model.tm;

import net.ihe.gazelle.proxy.model.channel.ChannelType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

@Entity
@Table(name = "tm_configuration", schema = "public")
@XmlAccessorType(XmlAccessType.NONE)
public class Configuration {

    @Id
    @GeneratedValue
    private int id;

    @XmlElement(name = "id")
    private int tmId;

    @ManyToOne
    private TestInstance testInstance;

    @Column(columnDefinition = "TEXT")
    @XmlElement
    private String name;

    @XmlElement
    private ChannelType type;

    @XmlElement
    private String host;

    @XmlElement
    private String hostname;

    @XmlElement
    private int port;

    @XmlElement
    private int proxyPort;

    @XmlElement
    private boolean securedChannel;
    @OneToMany(cascade = CascadeType.ALL)
    @XmlElement
    private List<Parameter> additionalParameters;

    public boolean isSecuredChannel() {
        return securedChannel;
    }

    public void setSecuredChannel(boolean securedChannel) {
        this.securedChannel = securedChannel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTmId() {
        return tmId;
    }

    public void setTmId(int tmId) {
        this.tmId = tmId;
    }

    public TestInstance getTestInstance() {
        return testInstance;
    }

    public void setTestInstance(TestInstance testInstance) {
        this.testInstance = testInstance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getHostname() {
        return this.hostname;
    }

    public List<Parameter> getAdditionalParameters() {
        return additionalParameters;
    }

    public void setAdditionalParameters(List<Parameter> additionalParameters) {
        this.additionalParameters = additionalParameters;
    }
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + tmId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Configuration other = (Configuration) obj;
        return tmId == other.tmId;
    }

    @Override
    public String toString() {
        return "Configuration [name=" + name + ", type=" + type + ", host=" + host + ", port=" + port + ", proxyPort="
                + proxyPort + "]";
    }

}
