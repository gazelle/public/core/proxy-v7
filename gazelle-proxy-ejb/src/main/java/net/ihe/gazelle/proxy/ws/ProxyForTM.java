package net.ihe.gazelle.proxy.ws;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.action.ProxyChannelRefList;
import net.ihe.gazelle.proxy.action.ProxyLocal;
import net.ihe.gazelle.proxy.action.TLSConfigurationService;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.dao.ProxyDAOTM;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.model.tm.Parameter;
import net.ihe.gazelle.proxy.model.tm.Step;
import net.ihe.gazelle.proxy.model.tm.TestInstance;
import net.ihe.gazelle.proxy.model.ws.Actor;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.*;
import javax.persistence.EntityManager;
import javax.xml.soap.SOAPException;
import java.net.InetAddress;
import java.util.*;

@WebService
@Stateless
@HandlerChain(file = "handlers.xml")
@Name("proxyForTMWebService")
public class ProxyForTM implements IProxyForTM {

    private static final Logger log = LoggerFactory.getLogger(ProxyForTM.class);


    @In
    protected SecuredChannelConfigurationManager securedChannelConfigurationManager;

    private TLSConfigurationService tlsConfigurationService = new TLSConfigurationService();

    @In
    private ProxyChannelRefList proxyChannelRefList;

    public ProxyForTM() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ihe.gazelle.proxy.ws.IProxyForTM#startAllChannels(java.util.List)
     */
    @WebMethod
    public
    @WebResult(name = "startedChannels")
    List<Configuration> startAllChannels(
            @WebParam(name = "configurationsList")
                    List<Configuration> configurations) throws SOAPException {
        if (null == configurations){
            throw new SOAPException("You must provide a configuration");
        }
        List<Configuration> startedChannels = new ArrayList<>();
        for (Configuration configuration : configurations) {
            try {
                configuration.setTestInstance(null);
                startChannel(configuration);
                startedChannels.add(configuration);
            }
            catch (Exception e) {
                log.error("Failed to start channel for configuration " + configuration, e);
            }
        }
        return startedChannels;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ihe.gazelle.proxy.ws.IProxyForTM#startTestInstance(net.ihe.gazelle
     * .proxy.model.tm.TestInstance)
     */
    @WebMethod
    public void startTestInstance(
            @WebParam(name = "testInstance")
                    TestInstance testInstance) throws TestInstanceConfigurationException {
        if (null == testInstance){
            throw new IllegalArgumentException("You must provide a test instance");
        }
        Calendar endTI = Calendar.getInstance();
        testInstance.setDate(endTI.getTime());

        endTI.add(Calendar.YEAR, 1);

        List<Step> steps = testInstance.getSteps();
        List<Configuration> configurations = testInstance.getConfigurations();

        if (steps != null) {
            for (Step step : steps) {
                // never stopped
                step.setDate(null);

                step.setTestInstance(testInstance);
                step.setMessage(null);
                List<Integer> ids = step.getReceiverIds();
                List<Configuration> stepConfigurations = new ArrayList<>();
                if (ids != null) {
                        stepConfigurations = getStepConfigurations(configurations, ids, true);
                }
                step.setReceivers(stepConfigurations);

                ids = step.getSenderIds();
                stepConfigurations = new ArrayList<>();
                if (ids != null) {
                    stepConfigurations = getStepConfigurations(configurations, ids, false);
                }
                step.setSenders(stepConfigurations);
            }
        }
        if (configurations != null) {
            for (int i = 0; i < configurations.size(); i++) {
                Configuration configuration = configurations.get(i);
                if (configuration != null) {
                    try{
                        configuration.setTestInstance(testInstance);
                        startChannel(configuration);
                    }
                    catch (Exception e){
                        log.error("Failed to start channel for configuration " + configuration, e);
                    }
                }
            }
        }

        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.persist(testInstance);

    }

    private List<Configuration> getStepConfigurations(List<Configuration> configurations, List<Integer> ids,
                                                      boolean receiver) {
        List<Configuration> stepConfigurations = new ArrayList<Configuration>();
        for (Integer id : ids) {
            Configuration stepConfiguration = getConfigurationByTmId(id, configurations);
            if (stepConfiguration != null &&
                    (!receiver || stepConfiguration.getProxyPort() > 0)) {
                stepConfigurations.add(stepConfiguration);

            }
        }
        return stepConfigurations;
    }

    private void startChannel(Configuration configuration) {
        if (configuration.getProxyPort() > 0 && configuration.getProxyPort() < 65536 && configuration.getPort() > 0
                && configuration.getPort() < 65536) {
            ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
            if (!proxyChannelRefList.contains(configuration.getProxyPort())) {
                try {
                    ProxyChannel proxyChannel = buildProxyChannelFromConfig(configuration);
                    proxyBean.startChannel(proxyChannel);
                    proxyChannelRefList.addProxyChannel(proxyChannel);
                }
                catch (Exception e) {
                    log.warn("Failed to start channel for configuration " + configuration, e);
                    throw new ChannelStartException("Failed to start channel for configuration " + configuration, e);
                }
            }
            else  {
                log.warn("Channel already started for configuration " + configuration);
                throw new ChannelStartException("Channel already started for configuration " + configuration);
            }
        }
        else {
            log.error("Proxy port {} is out of range [{},{}]", configuration.getProxyPort(), 0, 65535);
            throw new ChannelStartException("Proxy port " + configuration.getProxyPort() + " is out of range [0,65535]");
        }
    }

    private ProxyChannel buildProxyChannelFromConfig(Configuration configuration) {
        Map<String, String> additionalParameters = getParameter(configuration);
        ProxyChannel proxyChannel = new ProxyChannel()
                .setProxyPort(configuration.getProxyPort())
                .setResponder(
                        new Actor()
                                .setIp(resolveIpIfHost(configuration.getHost()))
                                .setPort(configuration.getPort())
                                .setHostname(configuration.getHostname())
                )
                .setType(configuration.getType())
               .setAdditionalParameters(additionalParameters)  //TM doesn't support additional parameters
                ;
        if (configuration.isSecuredChannel()) {
            proxyChannel.setTlsConfiguration(
                    tlsConfigurationService.createTLSConfiguration(securedChannelConfigurationManager)
            );
            proxyChannel.getTlsConfiguration().setSniEnabled(true);
        }
        return proxyChannel;
    }

    private Map<String, String> getParameter(Configuration configuration) {
        List<Parameter> additionalParameters = configuration.getAdditionalParameters();
        Map<String, String> additionalParameterDTOs = new HashMap<>();
        if ( additionalParameters != null && !additionalParameters.isEmpty()) {
            for (Parameter parameter : additionalParameters) {
                additionalParameterDTOs.put(parameter.getKey(), parameter.getValue());
            }
        }
        return additionalParameterDTOs;
    }

    private String resolveIpIfHost(String host) {
        if(isIp(host)) {
            return host;
        }
        try{
            return InetAddress.getByName(host).getHostAddress();
        } catch (Exception e) {
            log.error("Failed to resolve host " + host, e);
            return host;
        }
    }

    private boolean isIp(String host) {
        return host.matches("\\d+\\.\\d+\\.\\d+\\.\\d+");
    }



    private Configuration getConfigurationByTmId(int id, List<Configuration> configurations) {
        if (configurations==null)
            throw new IllegalArgumentException("configurations is null");
        for (Configuration configuration : configurations) {
            if (configuration.getTmId() == id) {
                return configuration;
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ihe.gazelle.proxy.ws.IProxyForTM#markTestStep(int)
     */
    @WebMethod
    public void markTestStep(
            @WebParam(name = "testStepId")
                    int testStepId) {
        ProxyDAOTM.updateStepDate(testStepId);
    }

    @WebMethod
    public String getMinProxyPort() {
        return ApplicationConfiguration.getValueOfVariable("min_proxy_port");
    }

    @WebMethod
    public String getMaxProxyPort() {
        return ApplicationConfiguration.getValueOfVariable("max_proxy_port");
    }

    @WebMethod
    public
    @WebResult(name = "channels")
    List<Configuration> getAllActiveChannels() {
        List<Configuration> list = new ArrayList<>();
//        ProxyLocal proxyBean = (ProxyLocal) Component.getInstance("proxyBean");
        List<ProxyChannel> proxyChannels = proxyChannelRefList.getSrotedProxyChannelsList();
        for (ProxyChannel proxy : proxyChannels) {
            Configuration connConfig = new Configuration();
            connConfig.setType(proxy.getType());
            connConfig.setProxyPort(proxy.getProxyPort());
            connConfig.setHost(proxy.getResponder().getHostname());
            connConfig.setPort(proxy.getResponder().getPort());
            connConfig.setSecuredChannel(proxy.isSecured());
            connConfig.setHostname(proxy.getResponder().getHostname());
            list.add(connConfig);
        }
        return list;
    }

    /**
     * Returns true if the provided proxy port is being listened on; false otherwise
     *
     * @param proxyPort : int
     * @return if channel exist
     */
    @WebMethod
    public boolean channelStarted(
            @WebParam(name = "proxyPort")
                    int proxyPort) {
        return proxyChannelRefList.contains(proxyPort);
    }
}
