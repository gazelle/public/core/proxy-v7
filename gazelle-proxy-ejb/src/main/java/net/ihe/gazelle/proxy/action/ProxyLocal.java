package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ProxyLocal {

    void startChannel(ProxyChannel proxyChannel);

    void startChannels(List<ProxyChannel> proxyChannels);

    void stopChannel(int localPort);

    void stopAllChannels();

    void syncChannels(ProxyChannelRefList proxyChannelRefList);

    public void restartAllChannels();

    public void restartAllSecuredChannels();

}
