package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class LazyProxyListDataModel extends LazyDataModel<ProxyChannel> {

    /**
     *
     */
    private static final long serialVersionUID = -9015238211789016416L;
    private final List<ProxyChannel> datasource;

    public LazyProxyListDataModel(List<ProxyChannel> datasource) {
        this.datasource = datasource;
    }

    @Override
    public ProxyChannel getRowData(String rowKey) {
        for (ProxyChannel conf : datasource) {
            if (conf.getResponder().getHostname().equals(rowKey)) {
                return conf;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(ProxyChannel conf) {
        return conf.getResponder().getHostname();
    }

    @Override
    public List<ProxyChannel> load(int first, int pageSize, String sortField, SortOrder sortOrder,
                                      Map<String, Object> filters) {
        List<ProxyChannel> data = new ArrayList<>();

        // filter
        for (ProxyChannel conf : datasource) {
            boolean match = true;

            if (filters != null) {
                for (Map.Entry<String, Object> filter : filters.entrySet()) {
                    try {
                        String filterProperty = filter.getKey();
                        Object filterValue = filter.getValue();

                        String fieldValue = String.valueOf(conf.getClass().getField(filterProperty).get(conf));

                        if (filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                        } else {
                            match = false;
                            break;
                        }
                    } catch (Exception e) {
                        match = false;
                    }
                }
            }

            if (match) {
                data.add(conf);
            }
        }

        // sort
        if (sortField != null) {
            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }

        // rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);

        // paginate
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }
}
