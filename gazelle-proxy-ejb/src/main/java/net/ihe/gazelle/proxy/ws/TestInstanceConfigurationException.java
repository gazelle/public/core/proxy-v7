package net.ihe.gazelle.proxy.ws;

public class TestInstanceConfigurationException extends RuntimeException{

    public TestInstanceConfigurationException() {
    }

    public TestInstanceConfigurationException(String message) {
        super(message);
    }

    public TestInstanceConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public TestInstanceConfigurationException(Throwable cause) {
        super(cause);
    }
}
