package net.ihe.gazelle.proxy.model.tm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

@Entity
@Table(name = "tm_parameter", schema = "public")
@XmlAccessorType(XmlAccessType.NONE)
public class Parameter {


    @Id
    @GeneratedValue
    private Long id;

    @XmlElement
    private String key;

    @XmlElement
    private String value;

    public String getKey() {
        return key;
    }

    public Parameter setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public Parameter setValue(String value) {
        this.value = value;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
