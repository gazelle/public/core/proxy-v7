package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;

import java.util.Date;

@MetaInfServices(PreferenceProvider.class)
public class ProxyPreferenceProvider implements PreferenceProvider {

    @Override
    public int compareTo(PreferenceProvider o) {
        return getWeight().compareTo(o.getWeight());
    }

    @Override
    public Boolean getBoolean(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            return Boolean.valueOf(prefAsString);
        } else {
            return null;
        }
    }

    @Override
    public Date getDate(String arg0) {
        return null;
    }

    @Override
    public Integer getInteger(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            try {
                return Integer.decode(prefAsString);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object getObject(Object arg0) {
        return null;
    }

    @Override
    public String getString(String key) {
        return ApplicationConfiguration.getValueOfVariable(key);
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return -100;
        } else {
            return 100;
        }
    }

    @Override
    public void setBoolean(String arg0, Boolean arg1) {

    }

    @Override
    public void setDate(String arg0, Date arg1) {

    }

    @Override
    public void setInteger(String arg0, Integer arg1) {

    }

    @Override
    public void setObject(Object arg0, Object arg1) {

    }

    @Override
    public void setString(String arg0, String arg1) {
    }

}
