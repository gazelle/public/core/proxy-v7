package net.ihe.gazelle.proxy.job;

import net.ihe.gazelle.proxy.action.ChannelSyncService;
import net.ihe.gazelle.proxy.action.ProxyChannelRefList;
import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.client.ChannelStopException;
import net.ihe.gazelle.proxy.client.ProxyClientException;
import net.ihe.gazelle.proxy.client.ProxyHttpClient;
import net.ihe.gazelle.proxy.model.ws.channel.ChannelStateException;
import org.jboss.seam.annotations.In;
import org.slf4j.Logger;

public class ChannelSyncJob implements Runnable {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ChannelSyncJob.class);

    private final ProxyChannelRefList proxyChannelRefList;
    private final ProxyHttpClient proxyHttpClient;

    private final ChannelSyncService channelSyncService;


    public ChannelSyncJob(ProxyHttpClient proxyHttpClient, ChannelSyncService channelSyncService, ProxyChannelRefList proxyChannelRefList) {
        this.proxyHttpClient = proxyHttpClient;
        this.channelSyncService = channelSyncService;
        this.proxyChannelRefList = proxyChannelRefList;
    }


    @Override
    public void run() {
        try{
            logger.info("Running ChannelSyncJob...");
            channelSyncService.syncChannels(proxyChannelRefList, proxyHttpClient);
        }
        catch (ChannelStartException | ChannelStopException e){
            logger.error("Error while syncing channels", e);
        }
        catch (ProxyClientException e){
            logger.error("Could not connect to proxy", e);
        }
        catch (Exception e){
            logger.error("Unexpected Error", e);
        }
    }




}
