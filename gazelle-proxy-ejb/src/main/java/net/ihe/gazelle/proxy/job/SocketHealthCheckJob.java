package net.ihe.gazelle.proxy.job;

import net.ihe.gazelle.proxy.action.ChannelSyncService;
import net.ihe.gazelle.proxy.action.ProxyChannelRefList;
import net.ihe.gazelle.proxy.client.ChannelStartException;
import net.ihe.gazelle.proxy.client.ChannelStopException;
import net.ihe.gazelle.proxy.client.ProxyClientException;
import net.ihe.gazelle.proxy.client.ProxyHttpClient;
import org.slf4j.Logger;

public class SocketHealthCheckJob implements Runnable {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SocketHealthCheckJob.class);


    private final ProxyHttpClient proxyHttpClient;

    private final ChannelSyncService channelSyncService;

    private final ProxyChannelRefList proxyChannelRefList;

    public SocketHealthCheckJob(ProxyHttpClient proxyHttpClient,
                                ChannelSyncService channelSyncService,
                                ProxyChannelRefList proxyChannelRefList) {
        this.proxyHttpClient = proxyHttpClient;
        this.channelSyncService = channelSyncService;
        this.proxyChannelRefList = proxyChannelRefList;

    }

    @Override
    public void run() {
        logger.info("Running SocketHealthCheckJob...");
        try{
            int numberOfChannelsInService = proxyHttpClient.countChannels();
            if(proxyChannelRefList != null){
                logger.info("======== Number of channels in proxy: {}", proxyChannelRefList.count());
                if (numberOfChannelsInService != proxyChannelRefList.count()) {
                    logger.warn("Number of channels in service is different from number of channels in proxy: in service:{} vs in proxy: {}", numberOfChannelsInService, proxyChannelRefList.count());
                    this.channelSyncService.syncChannels(proxyChannelRefList, proxyHttpClient);
                }
            }
        }
        catch (ChannelStartException | ChannelStopException e){
            logger.error("Could not start channel", e);
        }
        catch (ProxyClientException e){
            logger.error("Error while syncing channels", e);
        }
        catch (Exception e){
            logger.error("Unexpected Error", e);
        }
    }
}
