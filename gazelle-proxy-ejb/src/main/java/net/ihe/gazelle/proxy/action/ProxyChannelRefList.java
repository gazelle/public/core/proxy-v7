package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import org.infinispan.util.concurrent.ConcurrentHashSet;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@AutoCreate
@Name("proxyChannelRefList")
@Scope(org.jboss.seam.ScopeType.APPLICATION)
public class ProxyChannelRefList {

    private final Set<ProxyChannel> proxyChannels = Collections.newSetFromMap(new ConcurrentHashMap<ProxyChannel, Boolean>());

    public Set<ProxyChannel> getProxyChannels() {
        return proxyChannels;
    }

    public ProxyChannel getProxyChannel(int port) {
        for (ProxyChannel proxyChannel : proxyChannels) {
            if (proxyChannel.getProxyPort() == port) {
                return proxyChannel;
            }
        }
        return null;
    }

    public List<ProxyChannel> getSrotedProxyChannelsList(){
        List<ProxyChannel> list = new ArrayList<>(proxyChannels);
        Collections.sort(list);
        return list;
    }

    public void addProxyChannel(ProxyChannel proxyChannel) {
        this.proxyChannels.add(proxyChannel);
    }

    public boolean contains(int port) {
        for (ProxyChannel proxyChannel : proxyChannels) {
            if (proxyChannel.getProxyPort() == port) {
                return true;
            }
        }
        return false;
    }

    public int count() {
        return proxyChannels.size();
    }

    public void addAll(List<ProxyChannel> proxyChannels) {
        this.proxyChannels.addAll(proxyChannels);
    }

    public void remove(int port) {
        this.proxyChannels.remove(new ProxyChannel().setProxyPort(port));
    }

    public void clear() {
        this.proxyChannels.clear();
    }

}
