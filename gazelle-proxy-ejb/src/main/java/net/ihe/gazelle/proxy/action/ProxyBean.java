package net.ihe.gazelle.proxy.action;

import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;
import net.ihe.gazelle.proxy.client.*;
import net.ihe.gazelle.proxy.job.ChannelSyncJob;
import net.ihe.gazelle.proxy.job.SocketHealthCheckJob;
import net.ihe.gazelle.proxy.managers.SecuredChannelConfigurationManager;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.channel.SocketRequest;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;
import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Startup
@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("proxyBean")
public class ProxyBean implements ProxyLocal, Comparator<ProxyBean> {

    public static final int MIN_SYNC_PERIOD = 60;
    public static final int DEFAULT_INITIAL_HEALTHCHECK_DELAY = 30;
    public static final int INITIAL_SYNC_DELAY = 45;
    public static final int MIN_HEALTHCHECK_PERIOD = 10;

    public static Logger log = Logger.getLogger(ProxyBean.class);

    private final ScheduledExecutorService healthCheckExecutor = Executors.newSingleThreadScheduledExecutor();

    private final ScheduledExecutorService channelSyncExecutor = Executors.newSingleThreadScheduledExecutor();

    private final ChannelSyncService channelSyncService = new ChannelSyncService();

    private ProxyHttpClient proxyClient;

    private transient TLSConfigurationService tlsConfigurationService = new TLSConfigurationService();

    @In
    private ProxyChannelRefList proxyChannelRefList;

    @In
    private SecuredChannelConfigurationManager securedChannelConfigurationManager;

    private int initialHealthCheckDelay;

    @Create
    public void startProxy() {
        try {
            initialHealthCheckDelay = Integer.parseInt(ApplicationConfiguration.getValueOfVariable("initial_healthcheck_delay_seconds"));
            proxyClient = new ProxyHttpClientImpl();
            startSocketHealthCheck();
            startChannelSync();
        } catch (ProxyClientException e) {
            log.error(e);
            FacesMessages.instance().add(Severity.ERROR, "Error while getting the list of channels");
        }
    }


    @Override
    public void startChannel(ProxyChannel proxyChannel) {
        List<ProxyChannel> channelsFromSocketService = proxyClient.getAllStartedChannels();
        startChannel(proxyChannel, channelsFromSocketService);
    }

    @Override
    public void startChannels(List<ProxyChannel> proxyChannels){
        if(!proxyChannels.isEmpty()){
            try{
                List<ProxyChannel> startedChannels = proxyClient.startChannels(SocketRequest.buildFromChannels(proxyChannels));
                channelSyncService.assertProxyChannelsCreated(startedChannels, proxyChannels);
            }
            catch (ProxyClientException e){
                log.error("Cannot start channels on socket service",e);
                throw new ChannelStartException("Cannot start channels on socket service", e);
            }
        }
    }


    @Override
    public void stopChannel(int localPort) {
        stopChannelLocal(localPort);
    }

    @Override
    public void stopAllChannels() {
        try{
            proxyClient.stopAllChannels();
        }
        catch (ProxyClientException e){
            log.error("Could not stop all channels",e);
            throw new ChannelStopException("Could not stop all channels", e);
        }
    }



    @Override
    public void syncChannels(ProxyChannelRefList proxyChannelRefList) {
        channelSyncService.syncChannels(proxyChannelRefList, proxyClient);
    }

    public void restartAllChannels() {
        for (ProxyChannel proxy : proxyChannelRefList.getProxyChannels()) {
            restartSpecificChannel(proxy);
        }
    }

    public void restartAllSecuredChannels() {
        // reload the TLS configuration config
        TLSConfiguration tlsConfiguration = tlsConfigurationService.createTLSConfiguration(securedChannelConfigurationManager);
        for (ProxyChannel proxy : proxyChannelRefList.getProxyChannels()) {
            if(proxy.isSecured()){
                restartSpecificChannel(
                        proxy.setTlsConfiguration(new TLSConfiguration(tlsConfiguration, proxy.isSniEnabled()))
                );
            }
        }
    }

    private void restartSpecificChannel(ProxyChannel proxyChannel){
        stopChannel(proxyChannel.getProxyPort());
        startChannel(proxyChannel);
    }





    private void startChannel(ProxyChannel proxyChannel, List<ProxyChannel> channelsFromSocketService){
        try{
            proxyChannel.validate();
            stopChannelIfStartedInService(proxyChannel, channelsFromSocketService);
            List<ProxyChannel> proxyChannels = proxyClient.startChannels(SocketRequest.buildFromChannels(Collections.singletonList(proxyChannel)));
            if(proxyChannels.isEmpty()){
                throw new ChannelStartException("Channel not started, see socket service logs: "+proxyChannel);
            }
        }
        catch (IllegalStateException e){
            throw new ChannelStartException("Invalid channel object: "+proxyChannel, e);
        }
    }

    private void stopChannelLocal(int port){
        proxyClient.stopChannel(port);
    }

    private void stopChannelIfStartedInService(ProxyChannel proxyChannel, List<ProxyChannel> channelsFromSocketService){
        if(channelInList(proxyChannel, channelsFromSocketService)){
            proxyClient.stopChannel(proxyChannel.getProxyPort());
        }
    }


    @Destroy
    public void stopProxy() {
        try {
            stopAllChannels();
            if(!healthCheckExecutor.isShutdown()){
                healthCheckExecutor.shutdown();
            }
            if(!channelSyncExecutor.isShutdown()){
                channelSyncExecutor.shutdown();
            }
        }
        catch (Exception e){
            log.error("Error while stopping proxy", e);
        }
    }


    private String arrayToString(String[] array) {
        StringBuilder sb = new StringBuilder();
        for (String string : array) {
            sb.append(string);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private boolean channelInList(ProxyChannel proxyChannel, List<? extends ProxyChannel> proxyChannels){
        for(ProxyChannel proxyChannel1 : proxyChannels){
            if(proxyChannel.getProxyPort() == proxyChannel1.getProxyPort()){
                return true;
            }
        }
        return false;
    }




    @Override
    public int compare(ProxyBean o1, ProxyBean o2) {
        // TODO Auto-generated method stub
        return 0;
    }



    public int getSyncPeriod(){
        return Integer.parseInt(ApplicationConfiguration.getValueOfVariable("channel_sync_period_seconds"));
    }



    private void startChannelSync() {
        int period = Integer.parseInt(ApplicationConfiguration.getValueOfVariable("channel_sync_period_seconds"));
        if(period < MIN_SYNC_PERIOD){
            log.error(String.format("Channel sync period is less than min sync period seconds. Setting it to the min frequency: %d seconds", MIN_SYNC_PERIOD));
            period = MIN_SYNC_PERIOD;
        }
        channelSyncExecutor.scheduleAtFixedRate(new ChannelSyncJob(proxyClient, channelSyncService, proxyChannelRefList),
                INITIAL_SYNC_DELAY, period, TimeUnit.SECONDS);
    }

    private void startSocketHealthCheck() {
        int period = Integer.parseInt(ApplicationConfiguration.getValueOfVariable("socket_healthcheck_period_seconds"));
        if(period < MIN_HEALTHCHECK_PERIOD){
            log.error(String.format("Channel sync period is less than min sync period seconds. Setting it to the min frequency: %d seconds", MIN_HEALTHCHECK_PERIOD));
            period = MIN_HEALTHCHECK_PERIOD;
        }

        healthCheckExecutor.scheduleAtFixedRate(new SocketHealthCheckJob(proxyClient, channelSyncService, proxyChannelRefList),
                initialHealthCheckDelay, period, TimeUnit.SECONDS);
    }






}
