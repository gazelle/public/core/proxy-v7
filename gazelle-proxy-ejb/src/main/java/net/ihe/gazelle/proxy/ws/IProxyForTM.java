package net.ihe.gazelle.proxy.ws;

import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.model.tm.TestInstance;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.soap.SOAPException;
import java.util.List;

public interface IProxyForTM {

    @WebMethod
    List<Configuration>  startAllChannels(
            @WebParam(name = "configurationsList")
            List<Configuration> configurations) throws SOAPException;

    @WebMethod
    void startTestInstance(
            @WebParam(name = "testInstance")
            TestInstance testInstance) ;

    @WebMethod
    void markTestStep(
            @WebParam(name = "testStepId")
            int testStepId);

    @WebMethod
    String getMinProxyPort();

    @WebMethod
    String getMaxProxyPort();

}