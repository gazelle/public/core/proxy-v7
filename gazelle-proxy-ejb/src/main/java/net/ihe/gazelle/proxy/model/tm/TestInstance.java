package net.ihe.gazelle.proxy.model.tm;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tm_testInstance", schema = "public")
@XmlAccessorType(XmlAccessType.NONE)
public class TestInstance {

    @Id
    @GeneratedValue
    private int id;

    @XmlElement(name = "id")
    private int tmId;

    @OneToMany(mappedBy = "testInstance", cascade = CascadeType.ALL)
    @XmlElement
    private List<Configuration> configurations;

    @OneToMany(mappedBy = "testInstance", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("stepIndex")
    @XmlElement
    private List<Step> steps;

    private Date date;

    public TestInstance() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTmId() {
        return tmId;
    }

    public void setTmId(int tmId) {
        this.tmId = tmId;
    }

    public List<Configuration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<Configuration> configurations) {
        this.configurations = configurations;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + tmId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TestInstance other = (TestInstance) obj;
        return tmId == other.tmId;
    }
}
