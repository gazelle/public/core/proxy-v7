package net.ihe.gazelle.proxy.dao;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.proxy.interlay.dao.MessageFilter;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.tm.Configuration;
import net.ihe.gazelle.proxy.model.tm.Step;
import net.ihe.gazelle.proxy.model.tm.TestInstance;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.*;

public class MessageFilterStep<T extends AbstractMessage> implements MessageFilter<T> {

    public static String NEW_LINE = "\r\n";
    private static final Logger log = LoggerFactory.getLogger(MessageFilterStep.class);
    public Date startDate = null;
    public Date endDate = null;
    public Date lastModifDate = null;
    private final Step step;
    private boolean filterPathReceiver = true;
    private boolean filterPathFrom = true;
    private boolean filterDates = true;
    private boolean filterPersoDate = false;

    public MessageFilterStep(Step step) {
        super();
        this.step = step;
    }

    public static void main(String[] args) {
        Date lastModifDate = new Date();

        log.info(lastModifDate.toString());
        java.sql.Timestamp timeStampDate = new Timestamp(lastModifDate.getTime());
        log.info(timeStampDate.toString());
        Calendar cal = Calendar.getInstance();

        cal.setTime(lastModifDate);
        cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) + 1));
        lastModifDate = cal.getTime();
        timeStampDate = new Timestamp(lastModifDate.getTime());
        log.info(lastModifDate.toString());
        log.info(timeStampDate.toString());

        cal.setTimeInMillis(lastModifDate.getTime());
        cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) + 1));
        lastModifDate = cal.getTime();
        timeStampDate = new Timestamp(lastModifDate.getTime());
        log.info(lastModifDate.toString());
        log.info(timeStampDate.toString());
    }

    public Date getLastModifDate() {
        return lastModifDate;
    }

    public void setLastModifDate(Date lastModifDate) {
        this.lastModifDate = lastModifDate;
    }

    public boolean isFilterPersoDate() {
        return filterPersoDate;
    }

    public void setFilterPersoDate(boolean filterPersoDate) {
        this.filterPersoDate = filterPersoDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private Crit getFilter() {
        List<Crit> criterions = new ArrayList<Crit>();
        if (step != null) {
            TestInstance testInstance = step.getTestInstance();

            List<Crit> critSenders = new ArrayList<Crit>();
            List<Crit> critReceivers = new ArrayList<Crit>();

            List<Configuration> senders = step.getSenders();
            for (Configuration configuration : senders) {
                critSenders.add(new CritSimple("fromIP", "from", configuration.getHost(), CritSimpleType.EQ));
            }
            List<Configuration> receivers = step.getReceivers();
            for (Configuration configuration : receivers) {
                critReceivers.add(new CritSimple("proxyPort", "proxy port", configuration.getProxyPort(),
                        CritSimpleType.EQ));
            }

            Set<Crit> paths = new HashSet<Crit>();

            if (critSenders.size() == 0) {
                filterPathFrom = false;
            }
            if (critReceivers.size() == 0) {
                filterPathReceiver = false;
            }

            if (filterPathFrom && filterPathReceiver) {
                for (Crit critSender : critSenders) {
                    for (Crit critReceiver : critReceivers) {

                        Crit path = new CritLogical(true, critSender, critReceiver);
                        paths.add(path);

                    }
                }
            } else if (filterPathReceiver) {
                paths = new HashSet<MessageFilterStep.Crit>(critReceivers);
            } else if (filterPathFrom) {
                paths = new HashSet<MessageFilterStep.Crit>(critSenders);
            }

            if (paths.size() > 0) {
                Crit[] pathsArray = paths.toArray(new Crit[paths.size()]);
                Crit oneOfPath = new CritLogical(false, pathsArray);
                criterions.add(oneOfPath);
            }

            if (filterPersoDate) {
                if (endDate.before(startDate)) {
                    endDate = startDate;
                }

                criterions.add(new CritSimple("dateReceived", "date", startDate, CritSimpleType.GE));
                criterions.add(new CritSimple("dateReceived", "date", endDate, CritSimpleType.LE));
            }

            if (filterDates) {
                startDate = getStartDate(testInstance);
                // For the moment the end date is the last modification of TI
                endDate = getLastModifDate(testInstance);

                criterions.add(new CritSimple("dateReceived", "date", startDate, CritSimpleType.GE));
                criterions.add(new CritSimple("dateReceived", "date", endDate, CritSimpleType.LE));
            }
        }

        Crit criterion = null;
        if (criterions.size() > 0) {
            Crit[] criterionsArray = criterions.toArray(new Crit[criterions.size()]);
            criterion = new CritLogical(true, criterionsArray);
        }

        return criterion;
    }

    public void appendFilters(HQLQueryBuilder<T> criteria) {
        Crit filter = getFilter();
        if (filter != null) {
            criteria.addRestriction(filter.getCriterion());
        }
    }

    private Date getStartDate(TestInstance testInstance) {
        return testInstance.getDate();
    }

    private Date getLastModifDate(TestInstance testInstance) {
        setLastModifDate(startDate);
        Date lastModifDate = getLastModifDate();
        List<Step> steps = testInstance.getSteps();
        int indexOf = steps.indexOf(step);
        if (indexOf >= 0) {
            lastModifDate = steps.get(indexOf).getDate();
        }

        if (lastModifDate == null || startDate.compareTo(lastModifDate) >= 0) {
            setLastModifDate(startDate);
            Calendar cal = new GregorianCalendar();
            cal.setTime(getLastModifDate());
            cal.set(Calendar.YEAR, (cal.get(Calendar.YEAR) + 1));
            lastModifDate = cal.getTime();
        }
        return lastModifDate;
    }

    public boolean isFilterPathReceiver() {
        return filterPathReceiver;
    }

    public void setFilterPathReceiver(boolean filterPathReceiver) {
        this.filterPathReceiver = filterPathReceiver;
    }

    public boolean isFilterPathFrom() {
        return filterPathFrom;
    }

    public void setFilterPathFrom(boolean filterPathFrom) {
        this.filterPathFrom = filterPathFrom;
    }

    public boolean isFilterDates() {
        return filterDates;
    }

    public void setFilterDates(boolean filterDates) {
        this.filterDates = filterDates;
    }

    public String getStepFilter() {
        Crit criterion = getFilter();
        if (criterion != null) {
            StringBuilder sb = new StringBuilder();
            List<String> criterionString = criterion.asString();
            for (String string : criterionString) {
                sb.append(string).append(NEW_LINE);
            }
            String result = StringEscapeUtils.escapeHtml(sb.toString());
            return "<pre>" + result + "</pre>";
        } else {
            return "None";
        }
    }

    private enum CritSimpleType {
        EQ, GE, LE
    }

    private interface Crit {
        HQLRestriction getCriterion();

        List<String> asString();
    }

    private static class CritLogical implements Crit {

        private final boolean and;
        private final Crit[] crits;

        public CritLogical(boolean and, Crit... crits) {
            super();
            this.crits = crits;
            this.and = and;
        }

        public HQLRestriction getCriterion() {
            HQLRestriction result = null;
            for (Crit crit : crits) {
                HQLRestriction criterion = crit.getCriterion();
                if (result == null) {
                    result = criterion;
                } else {
                    if (and) {
                        result = HQLRestrictions.and(result, criterion);
                    } else {
                        result = HQLRestrictions.or(result, criterion);
                    }
                }
            }
            return result;
        }

        public List<String> asString() {
            String operator = "OR";
            if (and) {
                operator = "AND";
            }

            List<String> result = new ArrayList<String>();

            for (int i = 0; i < crits.length; i++) {

                List<String> critString = crits[i].asString();
                if (critString.size() == 1) {
                    result.add("  " + critString.get(0));
                } else {
                    result.add("(");
                    for (String crit1Line : critString) {
                        result.add("  " + crit1Line);
                    }
                    result.add(")");
                }
                if (i != crits.length - 1) {
                    result.add(operator);
                }
            }

            return result;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (and ? 1231 : 1237);
            result = prime * result + Arrays.hashCode(crits);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            CritLogical other = (CritLogical) obj;
            if (and != other.and) {
                return false;
            }
            return Arrays.equals(crits, other.crits);
        }

    }

    private static class CritSimple implements Crit {

        private final String field;
        private final String label;
        private final Object value;
        private final CritSimpleType type;

        public CritSimple(String field, String label, Object value, CritSimpleType type) {
            super();
            this.field = field;
            this.label = label;
            this.value = value;
            this.type = type;
        }

        public HQLRestriction getCriterion() {

            switch (type) {
            case EQ:
                return HQLRestrictions.eq(field, value);
            case GE:
                return HQLRestrictions.ge(field, value);
            case LE:
                return HQLRestrictions.le(field, value);
            }
            return null;
        }

        public List<String> asString() {
            String query = "";

            switch (type) {
            case EQ:
                query = label + " " + value;
                break;
            case GE:
                query = label + " > " + value;
                break;
            case LE:
                query = label + " < " + value;
                break;
            }
            return Collections.singletonList(query);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((field == null) ? 0 : field.hashCode());
            result = prime * result + ((type == null) ? 0 : type.hashCode());
            result = prime * result + ((value == null) ? 0 : value.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            CritSimple other = (CritSimple) obj;
            if (field == null) {
                if (other.field != null) {
                    return false;
                }
            } else if (!field.equals(other.field)) {
                return false;
            }
            if (type != other.type) {
                return false;
            }
            if (value == null) {
                return other.value == null;
            } else return value.equals(other.value);
        }

    }

}
