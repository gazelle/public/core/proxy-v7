package net.ihe.gazelle.proxy.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.proxy.application.channel.ProxyChannelPersistenceException;
import net.ihe.gazelle.proxy.application.channel.ProxyChannelPersistenceService;
import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@AutoCreate
@Name("proxyChannelPersistenceService")
@Scope(ScopeType.APPLICATION)
public class ProxyChannelPersistenceServiceJson implements ProxyChannelPersistenceService {

    @In
    ApplicationConfigurationManager applicationConfigurationManager;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final ObjectWriter objectWriter = objectMapper
            .writerFor(new TypeReference<List<ProxyChannelDTOJson>>(){})
            .withDefaultPrettyPrinter();
    private String fileLocation;

    @Create
    public void init() {
        this.fileLocation = applicationConfigurationManager.getProxyPersistantChannelsFilePath();
    }

    @Override
    public void save(ProxyChannelDTOJson proxyChannel) {
        try {
            List<ProxyChannelDTOJson> proxyChannels = readProxyChannels();
            proxyChannels.add(proxyChannel);
            String json = objectWriter.writeValueAsString(proxyChannels);
            Files.write(Paths.get(fileLocation), json.getBytes());
        } catch (JsonProcessingException e) {
            throw new ProxyChannelPersistenceException("Could not process json file at: "+fileLocation, e);
        } catch (IOException e) {
            throw new ProxyChannelPersistenceException("Could not write json file at: "+fileLocation, e);
        }
    }

    @Override
    public void delete(int port) {
        try {
            List<ProxyChannelDTOJson> proxyChannels = readProxyChannels();
            proxyChannels.remove(new ProxyChannelDTOJson().setProxyPort(port));
            String json = objectWriter.writeValueAsString(proxyChannels);
            Files.write(Paths.get(fileLocation), json.getBytes());
        } catch (JsonProcessingException e) {
            throw new ProxyChannelPersistenceException("Could not process json file at: "+fileLocation, e);
        } catch (IOException e) {
            throw new ProxyChannelPersistenceException("Could not write json file at: "+fileLocation, e);
        }
    }

    @Override
    public List<ProxyChannelDTOJson> findAll() {
        return readProxyChannels();
    }

    @Override
    public void deleteAll() {
        try {
            Files.write(Paths.get(fileLocation), "".getBytes());
        } catch (IOException e) {
            throw new ProxyChannelPersistenceException("Could not write json file at: "+fileLocation, e);
        }
    }

    private List<ProxyChannelDTOJson> readProxyChannels() {
        try {
            byte[] json = Files.readAllBytes(Paths.get(fileLocation));
            if(json.length == 0){
                return new ArrayList<>();
            }
            return objectMapper.readValue(json, new TypeReference<List<ProxyChannelDTOJson>>(){});
        } catch (IOException e) {
            throw new ProxyChannelPersistenceException("Could not read json file at: "+fileLocation, e);
        }
    }
}
