package net.ihe.gazelle.proxy.managers;


import net.ihe.gazelle.proxy.model.tls.trustore.TrustStoreLoadingMethod;
import net.ihe.gazelle.proxy.util.CertificateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

public abstract class TrustStoreManager implements X509TrustManager {

    static final Logger log = LoggerFactory.getLogger(TrustStoreManager.class);

    private List<X509Certificate> trustedCertificates;

    public TrustStoreManager() {
        super();
        this.trustedCertificates = getTrustedCertificates();
    }

    public List<TrustStoreLoadingMethod> getAvailableTrustStoreLoadingMethod() {
        return Arrays.asList(TrustStoreLoadingMethod.values());
    }

    public abstract List<X509Certificate> getTrustedCertificates();

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        try {
            CertificateUtil.doPKIXCheck(chain, trustedCertificates);
        } catch (CertPathValidatorException e) {
            StringBuilder sb = new StringBuilder("Client certificates chain not trusted.");
            sb.append(getSubjectDNAndIssuerDN(chain));
            throw new CertificateException(sb.toString());
        }

    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        try {
            CertificateUtil.doPKIXCheck(chain, trustedCertificates);
        } catch (CertPathValidatorException e) {
            StringBuilder sb = new StringBuilder("Responder certificates chain not trusted.");
            sb.append(getSubjectDNAndIssuerDN(chain));
            throw new CertificateException(sb.toString());
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return trustedCertificates.toArray(new X509Certificate[0]);
    }

    private String getSubjectDNAndIssuerDN(X509Certificate[] certificates) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i < certificates.length; i++) {
            sb.append("\nCertificate " + (i+1) + "\n");
            sb.append("Subject DN : ");
            sb.append(certificates[i].getSubjectDN());
            sb.append(". ");
            sb.append("Issuer DN : ");
            sb.append(certificates[i].getIssuerDN());
            sb.append(".");
        }

        return sb.toString();
    }
}
