package net.ihe.gazelle.proxy.model.ws.channel;

import net.ihe.gazelle.proxy.model.ws.tls.Certificate;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;
import net.ihe.gazelle.proxy.model.ws.tls.TLSCredentialsDTO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SocketRequest {

    private List<ProxyChannel> proxyChannels;
    private List<TLSCredentialsDTO> tlsCredentialsList;

    private List<Certificate> trustedCertificates;
    private Set<TLSConfiguration> tlsConfigurations;

    public SocketRequest() {
        this.tlsConfigurations = new HashSet<>();
        this.proxyChannels = new ArrayList<>();
        this.tlsCredentialsList = new ArrayList<>();
        this.trustedCertificates = new ArrayList<>();
    }

    public Set<TLSConfiguration> getTlsConfigurations() {
        return tlsConfigurations;
    }

    public TLSConfiguration getTlsConfiguration(String tlsConfigurationRef) {
        for (TLSConfiguration tlsConfiguration : tlsConfigurations) {
            if (tlsConfiguration.getId().equals(tlsConfigurationRef)) {
                return tlsConfiguration;
            }
        }
        return null;
    }


    public List<ProxyChannel> getProxyChannels() {
        return proxyChannels;
    }

    public List<TLSCredentialsDTO> getTlsCredentialsList() {
        return tlsCredentialsList;
    }

    public List<Certificate> getTrustedCertificates() {
        return trustedCertificates;
    }

    public SocketRequest addTlsConfiguration(TLSConfiguration tlsConfiguration) {
        tlsConfigurations.add(tlsConfiguration);
        return this;
    }

    public SocketRequest addProxyChannel(ProxyChannel proxyChannel) {
        proxyChannels.add(proxyChannel);
        return this;
    }

    public SocketRequest addTlsCredentials(TLSCredentialsDTO tlsCredentials) {
        tlsCredentialsList.add(tlsCredentials);
        return this;
    }

    public SocketRequest addTrustedCertificate(Certificate trustedCertificate) {
        trustedCertificates.add(trustedCertificate);
        return this;
    }

    public SocketRequest setProxyChannels(List<ProxyChannel> proxyChannels) {
        this.proxyChannels = proxyChannels;
        return this;
    }

    public SocketRequest setTlsCredentialsList(List<TLSCredentialsDTO> tlsCredentialsList) {
        this.tlsCredentialsList = tlsCredentialsList;
        return this;
    }

    public SocketRequest setTrustedCertificates(List<Certificate> trustedCertificates) {
        this.trustedCertificates = trustedCertificates;
        return this;
    }

    public SocketRequest setTlsConfigurations(Set<TLSConfiguration> tlsConfigurations) {
        this.tlsConfigurations = tlsConfigurations;
        return this;
    }

    public static SocketRequest buildFromChannels(List<? extends ProxyChannel> proxyChannels){
        SocketRequest socketRequest = new SocketRequest();
        for (ProxyChannel proxy : proxyChannels) {
            socketRequest.addProxyChannel(new ProxyChannel(proxy));
            if(proxy.isSecured()){
                socketRequest.addTlsConfiguration(proxy.getTlsConfiguration());
                socketRequest.getTrustedCertificates().addAll(proxy.getTlsConfiguration().getTrustedCerts());
                socketRequest.addTlsCredentials(proxy.getTlsConfiguration().getTlsCredentials());
            }
        }
        return socketRequest;
    }
}
