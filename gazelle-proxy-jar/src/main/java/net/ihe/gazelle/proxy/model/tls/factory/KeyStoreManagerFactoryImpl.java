package net.ihe.gazelle.proxy.model.tls.factory;

import net.ihe.gazelle.proxy.admin.model.KeyStoreDetails;
import net.ihe.gazelle.proxy.managers.KeyStoreManager;
import net.ihe.gazelle.proxy.managers.KeyStoreManagerFactory;
import net.ihe.gazelle.proxy.model.tls.managers.keystore.JVMKeyStoreManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("keyStoreManagerFactory")
public class KeyStoreManagerFactoryImpl implements KeyStoreManagerFactory {

    public KeyStoreManager getKeyStoreManager(KeyStoreDetails keyStoreDetails) {
        // choose KeyStoreManagerImplementation by StoreLoading method
        // for now, only JVM loading method is available
        return new JVMKeyStoreManager();
    }
}
