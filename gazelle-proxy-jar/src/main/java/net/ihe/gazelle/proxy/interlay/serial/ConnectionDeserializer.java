package net.ihe.gazelle.proxy.interlay.serial;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.ConnectionTls;
import net.ihe.gazelle.proxy.model.tls.keystore.CipherSuiteType;
import net.ihe.gazelle.proxy.model.tls.keystore.ProtocolType;
import net.ihe.gazelle.proxy.model.ws.ChannelSummary;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.TLSParameters;
import org.apache.commons.lang.RandomStringUtils;

import java.io.IOException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class ConnectionDeserializer implements ObjectDeserializer<Connection> {

    private final ObjectMapper mapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            ;

    protected ConnectionDTO connectionDTO;


    /**
     * {@inheritDoc}
     * <ul>
     *     <li>Items is used to get the connection id if set, for update purposes</li>
     * </ul>
     */
    @Override
    public Connection deserialize(String body, Item... items) {
        try {
            this.connectionDTO =  mapper.readValue(body, ConnectionDTO.class);
            Connection connection;
            if(connectionDTO.getChannelSummary().getProxyCertificateChain() != null){
                connection = createSecureConnection(connectionDTO.getChannelSummary());
            }
            else{
                connection = new Connection();
            }
            connection.setProxyChannelPort(connectionDTO.getChannelSummary().getProxyPort());
            connection.setPrivacyKey(RandomStringUtils.randomAlphanumeric(16));
            connection.setInitiatorHostname(connectionDTO.getInitiator().getHostname());
            connection.setInitiatorPort(connectionDTO.getInitiator().getPort());
            connection.setResponderHostname(connectionDTO.getChannelSummary().getResponder().getHostname());
            connection.setResponderPort(connectionDTO.getChannelSummary().getResponder().getPort());
            connection.setUuid("not-used");
            return connection;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private void setInitiatorTLSParameters(ConnectionTls connectionTls, TLSParameters initiatorTLSParameters) {
        connectionTls.setProtocolUsedBetweenInitAndProxy(ProtocolType.getProtocolTypeFromName(initiatorTLSParameters.getProtocol()));
        connectionTls.setCipherSuiteUsedBetweenInitAndProxy(CipherSuiteType.valueOf(initiatorTLSParameters.getCipherSuite()));
        if(initiatorTLSParameters.getCertificateChain() != null && !initiatorTLSParameters.getCertificateChain().isEmpty()){
            connectionTls.setInitiatorCertificateSubject(getCertificateSubject(initiatorTLSParameters.getCertificateChain().get(0).getCertPEM()));
        }
        else{
            connectionTls.setInitiatorCertificateSubject("Not defined");
        }
    }

    private void setResponderTLSParameters(ConnectionTls connectionTls, TLSParameters responderTLSParameters) {
        connectionTls.setProtocolUsedBetweenRespAndProxy(ProtocolType.getProtocolTypeFromName(responderTLSParameters.getProtocol()));
        connectionTls.setCipherSuiteUsedBetweenRespAndProxy(CipherSuiteType.valueOf(responderTLSParameters.getCipherSuite()));
        if(responderTLSParameters.getCertificateChain() != null && !responderTLSParameters.getCertificateChain().isEmpty()){
            connectionTls.setResponderCertificateSubject(getCertificateSubject(responderTLSParameters.getCertificateChain().get(0).getCertPEM()));
        }
        else{
            connectionTls.setResponderCertificateSubject("Not defined");
        }
    }

    private ConnectionTls createSecureConnection(ChannelSummary channelSummary) {
        ConnectionTls connection = new ConnectionTls();
        connection.setProxyCertificateSubject(getCertificateSubject(channelSummary.getProxyCertificateChain().get(0).getCertPEM()));
        if(channelSummary.getInitiatorTLSParameters() != null){
            setInitiatorTLSParameters(connection, channelSummary.getInitiatorTLSParameters());
            if(channelSummary.getResponderTLSParameters() != null){
                setResponderTLSParameters(connection, channelSummary.getResponderTLSParameters());
            }
            return connection;
        }
        throw new IllegalStateException("Channel is secured but TLS initiator parameters are not set");
    }

    private X509Certificate getCertificateFromPEM(byte[] pem) {
        if (pem == null) return null;
        try{
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            return (X509Certificate) factory.generateCertificate(new java.io.ByteArrayInputStream(pem));
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private String getCertificateSubject(byte[] pem){
        if(pem == null) return null;
        return getCertificateFromPEM(pem).getSubjectDN().toString();
    }

}
