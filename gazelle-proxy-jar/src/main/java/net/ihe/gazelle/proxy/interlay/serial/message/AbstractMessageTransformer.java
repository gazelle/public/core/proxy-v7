package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.application.record.MessageTransformer;
import net.ihe.gazelle.proxy.interlay.serial.UnexpectedErrorService;
import net.ihe.gazelle.proxy.model.channel.ProxySide;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.UnexpectedError;
import net.ihe.gazelle.proxy.model.ws.UnexpectedErrors;

public abstract class AbstractMessageTransformer<R extends Message, T extends AbstractMessage> implements MessageTransformer<R,T> {

    private final UnexpectedErrorService unexpectedErrorService = new UnexpectedErrorService();


    protected final T initMessage(R message, Connection connection){
        T msg = createMessage();
        if(message.getUnexpectedErrors() != null){
            msg.setMessageReceived(unexpectedErrorService.extractFullError(message.getUnexpectedErrors().getList()).getBytes());
            msg.setDecodeSuccess(false);
        }
        else{
            msg.setMessageReceived(message.getContent());
            msg.setDecodeSuccess(true);
        }
        ProxySide proxySide = computeProxySide(connection, message);
        msg.setDateReceived(message.getCaptureDate());
        msg.setConnection(connection);
        msg.setProxySide(proxySide);
        msg.setProxyPort(connection.getProxyChannelPort());
        if(proxySide == ProxySide.REQUEST){
            msg.setFromIP(message.getSender().getIp());
            msg.setLocalPort(message.getSender().getPort());
            msg.setToIP(message.getReceiver().getIp());
            msg.setRemotePort(message.getReceiver().getPort());
        }
        else{
            msg.setFromIP(message.getReceiver().getIp());
            msg.setLocalPort(message.getReceiver().getPort());
            msg.setToIP(message.getSender().getIp());
            msg.setRemotePort(message.getSender().getPort());
        }
        return msg;

    }

    private ProxySide computeProxySide(Connection connection, Message message) {
        if(connection.getResponderHostname().equalsIgnoreCase(message.getReceiver().getHostname())
                && connection.getResponderPort().equals(message.getReceiver().getPort())){
            return ProxySide.REQUEST;
        }
        return ProxySide.RESPONSE;
    }


    protected abstract T createMessage();


}


