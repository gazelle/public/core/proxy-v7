package net.ihe.gazelle.proxy.model.tls.managers.truststore;

import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import net.ihe.gazelle.proxy.model.tls.structure.TLSCredentials;
import net.ihe.gazelle.proxy.model.tls.tools.TlsCredentialsLoader;
import net.ihe.gazelle.proxy.model.tls.trustore.TrustStoreType;

import java.security.cert.X509Certificate;
import java.util.List;

public class JVMTrustStoreManager extends TrustStoreManager {

    @Override
    public List<X509Certificate> getTrustedCertificates() {
        TLSCredentials proxyCredentials = null;

        String keyStorePath = System.getProperty("javax.net.ssl.trustStore");
        String keyStorePassword = System.getProperty("javax.net.ssl.trustStorePassword");
        TrustStoreType trustStoreType = TrustStoreType.JKS;
        List<X509Certificate> trustedCertificates = TlsCredentialsLoader.loadAllTrustCertEntryCredentials(keyStorePath, keyStorePassword, trustStoreType);
        return trustedCertificates;
    }
}
