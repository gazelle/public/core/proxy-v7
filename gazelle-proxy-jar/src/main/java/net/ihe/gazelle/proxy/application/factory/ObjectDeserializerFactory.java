package net.ihe.gazelle.proxy.application.factory;

import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.model.ws.Item;

public interface ObjectDeserializerFactory {

    <T> ObjectDeserializer<T> createObjectDeserializer();

     boolean enabled(Item item);

}
