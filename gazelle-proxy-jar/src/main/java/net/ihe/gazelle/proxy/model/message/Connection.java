package net.ihe.gazelle.proxy.model.message;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "pxy_connection")
@Inheritance(strategy = InheritanceType.JOINED)
public class Connection implements java.io.Serializable {

    private static final long serialVersionUID = 6402109080942012360L;

    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(mappedBy = "connection")
    @OrderBy("dateReceived")
    private Set<AbstractMessage> messages;

    @Column(name = "privacy_key")
    private String privacyKey;
    private String uuid;

    @Column(name = "proxy_channel_port")
    private Integer proxyChannelPort;

    @Column(name = "initiator_hostname")
    private String initiatorHostname;

    @Column(name = "initiator_port")
    private Integer initiatorPort;

    @Column(name = "responder_hostname")
    private String responderHostname;

    @Column(name = "responder_port")
    private Integer responderPort;

    private boolean secured = false;

    private transient List<AbstractMessage> sortedMessages = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrivacyKey() {
        return privacyKey;
    }

    public void setPrivacyKey(String privacyKey) {
        this.privacyKey = privacyKey;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Set<AbstractMessage> getMessages() {
        return messages;
    }

    public void setMessages(Set<AbstractMessage> messages) {
        this.messages = messages;
    }

    protected void setSecured(boolean secured) {
        this.secured = secured;
    }

    public boolean isSecured() {
        return secured;
    }

    public Integer getProxyChannelPort() {
        return proxyChannelPort;
    }

    public void setProxyChannelPort(Integer proxyChannelPort) {
        this.proxyChannelPort = proxyChannelPort;
    }

    public String getInitiatorHostname() {
        return initiatorHostname;
    }

    public void setInitiatorHostname(String initiatorHostname) {
        this.initiatorHostname = initiatorHostname;
    }

    public String getResponderHostname() {
        return responderHostname;
    }

    public void setResponderHostname(String responderHostname) {
        this.responderHostname = responderHostname;
    }

    public Integer getInitiatorPort() {
        return initiatorPort;
    }

    public void setInitiatorPort(Integer initiatorPort) {
        this.initiatorPort = initiatorPort;
    }

    public Integer getResponderPort() {
        return responderPort;
    }

    public void setResponderPort(Integer responderPort) {
        this.responderPort = responderPort;
    }

    public List<AbstractMessage> getSortedMessages() {
        if (sortedMessages == null) {
            sortedMessages = new ArrayList<>(getMessages());
            Collections.sort(sortedMessages);
        }
        return sortedMessages;
    }

    @Override
    public String toString() {
        return "Connection [id=" + id + ", uuid=" + uuid + "]";
    }

}
