package net.ihe.gazelle.proxy.jms;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.util.ProxyConstants;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import javax.jms.*;
import java.net.InetAddress;
import java.util.Date;

/**
 * Sends messages into the proxyMsgQueue.
 */
@Name("proxyMessageSender")
@Scope(ScopeType.APPLICATION)
public class ProxyMessageSender extends AbstractMessageHandler {

    @Logger
    private static Log log;

    /**
     * Handle to the Queue, used to send messages
     */
    private QueueSender queueSender;

    private int maxSecondsForIdleQueueConnection;
    private long timeLastMessageSent;

    @Create
    public void init() {
        super.initialize();
    }

    protected Message createMessage(QueueSession queueSession, AbstractMessage proxyMessage) throws JMSException {
        // Create a message with values
        MapMessage jmsMessage = queueSession.createMapMessage();
        jmsMessage.setIntProperty(ProxyConstants.JMS_PARAM_MESSAGE_ID, proxyMessage.getId());
        jmsMessage.setIntProperty(ProxyConstants.JMS_PARAM_CONNECTION_ID, proxyMessage.getConnection().getId());
        jmsMessage
                .setStringProperty(ProxyConstants.JMS_PARAM_MESSAGE_DIRECTION, proxyMessage.getProxySide().toString());
        jmsMessage.setStringProperty(ProxyConstants.JMS_PARAM_PROXY_TYPE, "GAZELLE");

        jmsMessage.setLongProperty(ProxyConstants.JMS_PARAM_DATE_RECEIVED, proxyMessage.getDateReceived().getTime());
        jmsMessage.setStringProperty(ProxyConstants.JMS_PARAM_SENDER_IP_ADDRESS, proxyMessage.getFromIP());
        jmsMessage.setIntProperty(ProxyConstants.JMS_PARAM_SENDER_PORT, proxyMessage.getLocalPort());
        jmsMessage.setIntProperty(ProxyConstants.JMS_PARAM_PROXY_PORT, proxyMessage.getProxyPort());
        jmsMessage.setStringProperty(ProxyConstants.JMS_PARAM_RECEIVER_IP_ADDRESS, proxyMessage.getToIP());
        jmsMessage.setIntProperty(ProxyConstants.JMS_PARAM_RECEIVER_PORT, proxyMessage.getRemotePort());
        jmsMessage.setStringProperty(ProxyConstants.JMS_PARAM_MESSAGE_CONTENTS,
                proxyMessage.getMessageReceivedAsString());

        try {
            jmsMessage.setStringProperty(ProxyConstants.JMS_PARAM_PROXY_HOST_NAME,
                    InetAddress.getLocalHost().getHostName());
        } catch (Exception e) {
            log.error("getHostName() failed", e);
        }
        logMessage(jmsMessage);
        return jmsMessage;
    }

    public void sendMessage(AbstractMessage proxyMessage) {
        // The concrete implementation of this class is configured as a
        // singleton in seam. Multiple threads may
        // call this instance. Allow only one thread at a time to send a message
        // into the Queue.
        synchronized (this) {
            boolean canSendMessage = true;

            try {
                if ((new Date().getTime() - timeLastMessageSent) > (getMaxSecondsForIdleQueueConnection() * 1000)) {
                    // By getting here, the current Queue connection is open
                    // longer than the prescribed idle period.
                    // Close the connection.
                    stopJmsConnection();

                    // Attempt to open a fresh Queue connection, and indicate
                    // success of this attempt
                    canSendMessage = startJmsConnection();
                }

                if (canSendMessage) {
                    // Place a new message into the Queue
                    queueSender.send(createMessage(getQueueSession(), proxyMessage));

                    // Note the current time that the message was sent
                    timeLastMessageSent = new Date().getTime();

                    if (log.isDebugEnabled()) {
                        log.debug("JMS Sender sent message.");
                    }
                }
            } catch (JMSException e) {
                log.error("Exception occurred while sending message.", e);

                stopJmsConnection();
            }
        }
    }

    protected void createQueueObject(QueueSession queueSession, Queue queue) throws JMSException {
        // Create the Queue sender that produces messages
        queueSender = queueSession.createSender(queue);
    }

    protected int getMaxSecondsForIdleQueueConnection() {
        return maxSecondsForIdleQueueConnection;
    }

    public void setMaxSecondsForIdleQueueConnection(int maxSecondsForIdleQueueConnection) {
        this.maxSecondsForIdleQueueConnection = maxSecondsForIdleQueueConnection;
    }

    private void logMessage(MapMessage jmsMessage) throws JMSException {
        log.info("createMessage() " + ProxyConstants.JMS_PARAM_MESSAGE_ID + "=" + jmsMessage
                        .getIntProperty(ProxyConstants.JMS_PARAM_MESSAGE_ID) + " "

                        + ProxyConstants.JMS_PARAM_PROXY_TYPE + "=" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_PROXY_TYPE) + " "

                        + ProxyConstants.JMS_PARAM_PROXY_HOST_NAME + "=" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_PROXY_HOST_NAME) + " "

                        + ProxyConstants.JMS_PARAM_CONNECTION_ID + "=" + jmsMessage
                        .getIntProperty(ProxyConstants.JMS_PARAM_CONNECTION_ID) + " "

                        + ProxyConstants.JMS_PARAM_MESSAGE_DIRECTION + "=" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_MESSAGE_DIRECTION) + " "

                        + ProxyConstants.JMS_PARAM_DATE_RECEIVED + "=" + jmsMessage
                        .getLongProperty(ProxyConstants.JMS_PARAM_DATE_RECEIVED) + " "

                        + ProxyConstants.JMS_PARAM_SENDER_IP_ADDRESS + "=" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_SENDER_IP_ADDRESS) + " "

                        + ProxyConstants.JMS_PARAM_SENDER_PORT + "=" + jmsMessage
                        .getIntProperty(ProxyConstants.JMS_PARAM_SENDER_PORT) + " "

                        + ProxyConstants.JMS_PARAM_RECEIVER_IP_ADDRESS + "=" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_RECEIVER_IP_ADDRESS) + " "

                        + ProxyConstants.JMS_PARAM_RECEIVER_PORT + "=" + jmsMessage
                        .getIntProperty(ProxyConstants.JMS_PARAM_RECEIVER_PORT) + " "

                        + ProxyConstants.JMS_PARAM_PROXY_PORT + "=" + jmsMessage
                        .getIntProperty(ProxyConstants.JMS_PARAM_PROXY_PORT) + " "

                        + ProxyConstants.JMS_PARAM_MESSAGE_CONTENTS + "=\n" + jmsMessage
                        .getStringProperty(ProxyConstants.JMS_PARAM_MESSAGE_CONTENTS)

        );
    }
}
