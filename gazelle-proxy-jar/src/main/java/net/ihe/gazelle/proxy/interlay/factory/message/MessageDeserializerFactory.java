package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.ObjectDeserializerFactory;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.interlay.factory.connection.DBConnectionProvider;
import net.ihe.gazelle.proxy.interlay.serial.message.MessageDeserializer;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.Message;

public class MessageDeserializerFactory implements ObjectDeserializerFactory {

    @Override
    public <T> ObjectDeserializer<T> createObjectDeserializer() {
        return (ObjectDeserializer<T>) new MessageDeserializer(new DBConnectionProvider());
    }

    @Override
    public boolean enabled(Item item) {
        return Message.TYPE.equals(item.getType());
    }
}
