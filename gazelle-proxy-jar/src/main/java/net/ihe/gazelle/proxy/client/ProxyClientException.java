package net.ihe.gazelle.proxy.client;

public class ProxyClientException extends RuntimeException{

    public ProxyClientException(String message) {
        super(message);
    }

    public ProxyClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProxyClientException(Throwable cause) {
        super(cause);
    }
}
