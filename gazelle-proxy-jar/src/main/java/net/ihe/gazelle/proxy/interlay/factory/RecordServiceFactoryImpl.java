package net.ihe.gazelle.proxy.interlay.factory;

import net.ihe.gazelle.proxy.application.factory.RecordServiceFactory;
import net.ihe.gazelle.proxy.application.record.RecordService;
import net.ihe.gazelle.proxy.interlay.record.ConnectionErrorRecordServiceHibernate;
import net.ihe.gazelle.proxy.interlay.record.RecordServiceHibernate;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;

public class RecordServiceFactoryImpl implements RecordServiceFactory {

    private static final RecordService RECORD_SERVICE_HIBERNATE = new RecordServiceHibernate();

    private static final RecordService CONNECTION_ERROR_RECORD_SERVICE_HIBERNATE = new ConnectionErrorRecordServiceHibernate();

    @Override
    public RecordService createRecordService(Item item) {
        return containsErrorKey(item) ? CONNECTION_ERROR_RECORD_SERVICE_HIBERNATE : RECORD_SERVICE_HIBERNATE;
    }


    private boolean containsErrorKey(Item item) {
        if(item.getAdditionalParameters() == null || item.getAdditionalParameters().isEmpty()){
            return false;
        }
        return item.getAdditionalParameters().containsKey(ConnectionDTO.CONNECTION_ERROR_KEY);
    }
}
