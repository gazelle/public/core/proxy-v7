package net.ihe.gazelle.proxy.application.record;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.ws.Message;

public interface MessageTransformer<R extends Message, T extends AbstractMessage> {

    /**
     * Transform the WS message to an entity message to be stored in the database
     * @param message the WS message
     * @return the entity message
     * @throws IllegalArgumentException if the provided message is null
     */
    T transform(R message, Connection connection);
}
