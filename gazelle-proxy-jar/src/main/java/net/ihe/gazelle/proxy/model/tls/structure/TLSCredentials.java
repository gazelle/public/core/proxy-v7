package net.ihe.gazelle.proxy.model.tls.structure;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class TLSCredentials {

    private X509Certificate[] certificateChain;
    private String alias;
    private PrivateKey privateKey;

    public TLSCredentials(X509Certificate[] certificateChain, String alias, PrivateKey privateKey) {
        this.certificateChain = certificateChain;
        this.alias = alias;
        this.privateKey = privateKey;
    }

    public X509Certificate[] getCertificateChain() {
        return certificateChain;
    }

    public void setCertificateChain(X509Certificate[] certificateChain) {
        this.certificateChain = certificateChain;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }



}
