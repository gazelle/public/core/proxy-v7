package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.application.record.MessageTransformer;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.ws.Message;

import java.util.ServiceLoader;

public class MessageTransformerProvider {


    public MessageTransformer<? extends Message, ? extends AbstractMessage> createTransformer(Class<? extends Message> clazz) {
        ServiceLoader<MessageTransformerFactory> loader = ServiceLoader.load(MessageTransformerFactory.class);
        for (MessageTransformerFactory factory : loader) {
            if (isOneOfTypes(factory, clazz)) {
                return factory.createMessageTransformer();
            }
        }
        throw new IllegalArgumentException("Unknown Message type " + clazz);
    }

    private boolean isOneOfTypes(MessageTransformerFactory factory, Class<? extends Message> clazz) {
        for (Class<? extends Message> type : factory.getTypes()) {
            if (type.equals(clazz)) {
                return true;
            }
        }
        return false;
    }


}
