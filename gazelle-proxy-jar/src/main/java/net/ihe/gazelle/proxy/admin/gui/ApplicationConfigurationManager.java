/*
 * Copyright 2010 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.proxy.admin.gui;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import java.io.Serializable;

/**
 * <b>Class Description : </b>ApplicationConfigurationManager<br>
 * <br>
 * This class contains the methods to access common applications preferences and specific proxy preferences
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes IHE development Project
 * @version 2.0 - 2015, October 23th, (by Cédric Eoche-Duval)
 */

@Name("applicationConfigurationManager")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationManagerLocal")
public class ApplicationConfigurationManager extends AbstractApplicationConfigurationManager implements Serializable, ApplicationConfigurationManagerLocal {

    /**
     *
     */
    private static final long serialVersionUID = -7817765881624341837L;

    private String proxyIpAddresses;
    private Integer minProxyPort;
    private Integer maxProxyPort;
    private String dcmDumpPath;
    private Boolean casEnable;
    private String proxyPersistantChannelsFilePath;

    private String datahouseUrl;

    public static ApplicationConfigurationManager instance() {
        return (ApplicationConfigurationManager) Component.getInstance("applicationConfigurationManager");
    }

    @Remove
    @Destroy
    public void destroy() {
        resetApplicationConfiguration();
    }

    public void resetApplicationConfiguration() {
        super.resetApplicationConfiguration();
        proxyIpAddresses = null;
        maxProxyPort = null;
        minProxyPort = null;
    }

    @Override
    public String getProxyPersistantChannelsFilePath() {
        if (proxyPersistantChannelsFilePath == null) {
            proxyPersistantChannelsFilePath = getApplicationProperty("proxy_persistent_channels_file_path");
        }
        return proxyPersistantChannelsFilePath;
    }


    public String getProxyIpAddresses() {
        if (proxyIpAddresses == null) {
            proxyIpAddresses = getApplicationProperty("proxy_ip_addresses");
        }
        return proxyIpAddresses;
    }

    @Override
    public int getMinProxyPort() {
        if (minProxyPort == null) {
            String port = getApplicationProperty("min_proxy_port");
            if (port != null) {
                minProxyPort = Integer.decode(port);
            } else {
                minProxyPort = 10101;
            }
        }
        return minProxyPort;
    }

    public int getMaxProxyPort() {
        if (maxProxyPort == null) {
            String port = getApplicationProperty("max_proxy_port");
            if (port != null) {
                maxProxyPort = Integer.decode(port);
            } else {
                maxProxyPort = 11000;
            }
        }
        return maxProxyPort;
    }


    public String getDcmDumpPath() {
        
        if (dcmDumpPath == null) {
            dcmDumpPath = getApplicationProperty("dcmdump_path");
        }
        return dcmDumpPath;
    }

    public boolean isCasEnable() {
        if (casEnable == null) {
            String booleanAsString = getApplicationProperty("cas_enabled");
            if (booleanAsString == null) {
                this.casEnable = false;
            } else {
                this.casEnable = Boolean.valueOf(booleanAsString);
            }
        }
        return casEnable;
    }

    public String getDatahouseUrl() {
        if (datahouseUrl == null) {
            datahouseUrl = getApplicationProperty("datahouse_ui_url");
        }
        return datahouseUrl;
    }


}
