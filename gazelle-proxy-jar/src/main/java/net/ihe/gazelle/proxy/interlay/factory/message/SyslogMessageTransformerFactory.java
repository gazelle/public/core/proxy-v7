package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.interlay.serial.message.SyslogMessageTransformer;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.messages.SyslogMessageDTO;

import java.util.ArrayList;
import java.util.List;

public class SyslogMessageTransformerFactory implements MessageTransformerFactory{

    @Override
    public SyslogMessageTransformer createMessageTransformer() {
        return new SyslogMessageTransformer();
    }

    @Override
    public List<Class<? extends Message>> getTypes() {
        ArrayList<Class<? extends Message>> types = new ArrayList<>();
        types.add(SyslogMessageDTO.class);
        return types;
    }
}
