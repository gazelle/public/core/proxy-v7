package net.ihe.gazelle.proxy.application.record;

import net.ihe.gazelle.proxy.model.ws.Item;

public interface ObjectDeserializer <T>{


    /**
     * Deserialize the body of a message to an object
     * @param body the body of the message as string
     * @param items optional items to be used during deserialization
     * @return the deserialized object
     * @throws IllegalArgumentException if the number of items is not correct
     * @throws SerializationException if the deserialization fails
     * @throws IllegalStateException if the type is not the expected one
     */
    T deserialize(String body, Item... items);
}
