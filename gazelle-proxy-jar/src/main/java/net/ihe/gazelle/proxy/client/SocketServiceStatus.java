package net.ihe.gazelle.proxy.client;

public class SocketServiceStatus {

    public static final SocketServiceStatus INSTANCE = new SocketServiceStatus();

    private boolean connected;

    private SocketServiceStatus() {
        this.connected = false;
    }

    public boolean isConnected() {
        return connected;
    }

    public SocketServiceStatus setConnected(boolean connected) {
        this.connected = connected;
        return this;
    }
}
