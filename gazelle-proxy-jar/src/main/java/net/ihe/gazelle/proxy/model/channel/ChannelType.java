package net.ihe.gazelle.proxy.model.channel;


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@XmlType(name = "ChannelType")
@XmlEnum
@XmlRootElement(name = "ChannelType")
public enum ChannelType {

    @XmlEnumValue("DICOM")
    DICOM("DicomMessage", "DICOM"),

    @XmlEnumValue("HL7v2")
    HL7v2("HL7v2Message", "HL7v2"),

    @XmlEnumValue("SYSLOG")
    SYSLOG("SyslogMessage", "SYSLOG"),

    @XmlEnumValue("HTTP")
    HTTP("HTTPMessage", "HTTP"),

    @XmlEnumValue("RAW_TCP")
    RAW_TCP("RawMessage", "RAW_TCP"),

    @XmlEnumValue("TLS_ERROR")
    TLS_ERROR("TlsErrorMessage", "TLS_ERROR"),

    @XmlEnumValue("CONNECTION_ERROR_MESSAGE")
    CONNECTION_ERROR("ConnectionErrorMessage", "CONNECTION_ERROR");

    private String discriminator;
    private String identifier;

    ChannelType(String discriminator, String identifier) {
        this.discriminator = discriminator;
        this.identifier = identifier;
    }

    public static List<ChannelType> getApplicativeValues(){
        List<ChannelType> applicativeValues = new ArrayList<>(Arrays.asList(values()));
        applicativeValues.remove(TLS_ERROR);
        applicativeValues.remove(CONNECTION_ERROR);
        return applicativeValues;
    }

    public static ChannelType getEnumValue(String idenfier) {
        for (ChannelType type : values()) {
            if (type.getIdentifier().equals(idenfier)) {
                return type;
            } else {
                continue;
            }
        }
        return null;
    }

    public String getDiscriminator() {
        return discriminator;
    }


    public String getIdentifier() {
        return identifier;
    }

}
