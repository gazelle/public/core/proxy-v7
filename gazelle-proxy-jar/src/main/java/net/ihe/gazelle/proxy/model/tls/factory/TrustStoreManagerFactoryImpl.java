package net.ihe.gazelle.proxy.model.tls.factory;

import net.ihe.gazelle.proxy.admin.model.TrustStoreDetails;
import net.ihe.gazelle.proxy.managers.TrustStoreManager;
import net.ihe.gazelle.proxy.managers.TrustStoreManagerFactory;
import net.ihe.gazelle.proxy.model.tls.managers.truststore.JVMTrustStoreManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("trustStoreManagerFactory")
public class TrustStoreManagerFactoryImpl implements TrustStoreManagerFactory {

    public TrustStoreManager getTrustStoreManager(TrustStoreDetails trustStoreDetails) {
        // choose TrustStoreManager Implementation by StoreLoading method
        // for now, only JVM loading method is available
        return new JVMTrustStoreManager();
    }
}
