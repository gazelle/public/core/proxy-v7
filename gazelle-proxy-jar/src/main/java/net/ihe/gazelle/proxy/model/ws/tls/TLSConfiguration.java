package net.ihe.gazelle.proxy.model.ws.tls;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class TLSConfiguration {

    private final String id;

    private String[] supportedProtocols;

    private String[] supportedCipherSuites;

    private String tlsCredentialsRef;

    private String[] trustedCertsRefs;

    private TLSCredentialsDTO tlsCredentials;

    private List<Certificate> trustedCerts;

    private boolean isMTLS;

    private boolean sniEnabled;

    public TLSConfiguration() {

        this.id = UUID.randomUUID().toString();
    }

    public TLSConfiguration(TLSConfiguration tlsConfiguration) {
        this(tlsConfiguration, tlsConfiguration.sniEnabled);
    }

    public TLSConfiguration(TLSConfiguration tlsConfiguration, boolean sniEnabled) {
        this.id = UUID.randomUUID().toString();
        this.supportedProtocols = tlsConfiguration.getSupportedProtocols();
        this.supportedCipherSuites = tlsConfiguration.getSupportedCipherSuites();
        this.tlsCredentialsRef = tlsConfiguration.getTlsCredentialsRef();
        this.trustedCertsRefs = tlsConfiguration.getTrustedCertsRefs();
        this.tlsCredentials = tlsConfiguration.getTlsCredentials();
        this.trustedCerts = tlsConfiguration.getTrustedCerts();
        this.isMTLS = tlsConfiguration.isMTLS();
        this.sniEnabled = sniEnabled;
    }




    public String getId() {
        return id;
    }

    public String[] getSupportedProtocols() {
        return supportedProtocols;
    }

    public TLSConfiguration setSupportedProtocols(String[] supportedProtocols) {
        this.supportedProtocols = supportedProtocols;
        return this;
    }

    public String[] getSupportedCipherSuites() {
        return supportedCipherSuites;
    }

    public TLSConfiguration setSupportedCipherSuites(String[] supportedCipherSuites) {
        this.supportedCipherSuites = supportedCipherSuites;
        return this;
    }


    @JsonIgnore
    public TLSCredentialsDTO getTlsCredentials() {
        return tlsCredentials;
    }

    public TLSConfiguration setTlsCredential(TLSCredentialsDTO tlsCredentials) {
        this.tlsCredentialsRef = tlsCredentials.getId();
        this.tlsCredentials = tlsCredentials;
        return this;
    }

    @JsonIgnore
    public List<Certificate> getTrustedCerts() {
        return trustedCerts;
    }

    public TLSConfiguration setTrustedCerts(List<Certificate> trustedCerts) {
        this.trustedCerts = trustedCerts;
        this.trustedCertsRefs = new String[trustedCerts.size()];
        for (int i = 0; i < trustedCerts.size(); i++) {
            this.trustedCertsRefs[i] = trustedCerts.get(i).getId();
        }
        return this;
    }

    public boolean isMTLS() {
        return isMTLS;
    }

    public TLSConfiguration setMTLS(boolean MTLS) {
        isMTLS = MTLS;
        return this;
    }

    public String getTlsCredentialsRef() {
        return tlsCredentialsRef;
    }

    public String[] getTrustedCertsRefs() {
        return trustedCertsRefs;
    }

    public boolean isSniEnabled() {
        return sniEnabled;
    }

    public TLSConfiguration setSniEnabled(boolean sniEnabled) {
        this.sniEnabled = sniEnabled;
        return this;
    }

    @Override
    public String toString() {
        return "TLSConfiguration{" +
                "id='" + id + '\'' +
                ", supportedProtocols=" + Arrays.toString(supportedProtocols) +
                ", supportedCipherSuites=" + Arrays.toString(supportedCipherSuites) +
                ", isMTLS=" + isMTLS +
                ", sniEnabled=" + sniEnabled +
                '}';
    }
}
