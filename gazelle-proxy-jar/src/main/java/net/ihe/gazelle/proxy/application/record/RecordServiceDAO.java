package net.ihe.gazelle.proxy.application.record;

public interface RecordServiceDAO {

    <T> String save(T item);
}
