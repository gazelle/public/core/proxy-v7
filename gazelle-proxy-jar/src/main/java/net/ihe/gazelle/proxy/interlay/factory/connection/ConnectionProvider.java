package net.ihe.gazelle.proxy.interlay.factory.connection;

import net.ihe.gazelle.proxy.model.message.Connection;

public interface ConnectionProvider {

    Connection getConnection(int id);
}
