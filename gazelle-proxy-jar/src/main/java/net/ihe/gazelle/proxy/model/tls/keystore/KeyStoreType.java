package net.ihe.gazelle.proxy.model.tls.keystore;

public enum KeyStoreType {
    JKS("JKS"),
    P12("PKCS12");

    private String name;

    KeyStoreType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
