package net.ihe.gazelle.proxy.model.ws;

public class ParameterDTO {

    private String key;

    private String value;

    public String getKey() {
        return key;
    }

    public ParameterDTO setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public ParameterDTO setValue(String value) {
        this.value = value;
        return this;
    }
}
