package net.ihe.gazelle.proxy.interlay.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.ihe.gazelle.proxy.model.channel.ChannelType;
import net.ihe.gazelle.proxy.model.ws.Actor;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;
import net.ihe.gazelle.proxy.model.ws.UnexpectedError;
import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;

import java.util.*;

public class ProxyChannelDTOJson  {

    private int proxyPort;
    private ChannelType type;

    private Actor responder;

    private List<ParameterDTO> additionalParameters;

    private boolean persistent;

    private boolean secured;

    private boolean sniEnabled;


    private UnexpectedError unexpectedError;

    public ProxyChannelDTOJson(ProxyChannel proxyChannel) {
        this.proxyPort = proxyChannel.getProxyPort();
        this.type = proxyChannel.getType();
        this.responder = proxyChannel.getResponder();
        this.additionalParameters = mapToParametersDTO(proxyChannel.getAdditionalParameters());
        this.secured = proxyChannel.isSecured();
        this.sniEnabled = proxyChannel.isSniEnabled();
        this.persistent = proxyChannel.isPersistent();
    }

    public ProxyChannelDTOJson() {

    }


    public int getProxyPort() {
        return proxyPort;
    }

    public ProxyChannelDTOJson setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
        return this;
    }

    public ChannelType getType() {
        return type;
    }

    public ProxyChannelDTOJson setType(ChannelType type) {
        this.type = type;
        return this;
    }

    public Actor getResponder() {
        return responder;
    }

    public ProxyChannelDTOJson setResponder(Actor responder) {
        this.responder = responder;
        return this;
    }

    public List<ParameterDTO> getAdditionalParameters() {
        return additionalParameters;
    }

    public ProxyChannelDTOJson setAdditionalParameters(List<ParameterDTO> additionalParameters) {
        this.additionalParameters = additionalParameters;
        return this;
    }

    public boolean isPersistent() {
        return persistent;
    }

    public ProxyChannelDTOJson setPersistent(boolean persistent) {
        this.persistent = persistent;
        return this;
    }

    public boolean isSecured() {
        return secured;
    }

    public ProxyChannelDTOJson setSecured(boolean secured) {
        this.secured = secured;
        return this;
    }

    public boolean isSniEnabled() {
        return sniEnabled;
    }

    public ProxyChannelDTOJson setSniEnabled(boolean sniEnabled) {
        this.sniEnabled = sniEnabled;
        return this;
    }

    @JsonIgnore
    public UnexpectedError getUnexpectedError() {
        return unexpectedError;
    }

    @JsonIgnore
    public ProxyChannelDTOJson setUnexpectedError(UnexpectedError unexpectedError) {
        this.unexpectedError = unexpectedError;
        return this;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof ProxyChannelDTOJson)) return false;
        ProxyChannelDTOJson that = (ProxyChannelDTOJson) object;
        return proxyPort == that.proxyPort;
    }

    @Override
    public int hashCode() {
        return Objects.hash(proxyPort);
    }

    private List<ParameterDTO> mapToParametersDTO(Map<String, String> parameters) {
        List<ParameterDTO> parameterDTOs = new ArrayList<>();
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            parameterDTOs.add(
                    new ParameterDTO()
                            .setValue(entry.getValue())
                            .setKey(entry.getKey())
            );
        }
        return parameterDTOs;
    }
}
