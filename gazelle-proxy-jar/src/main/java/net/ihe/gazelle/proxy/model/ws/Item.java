package net.ihe.gazelle.proxy.model.ws;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

//@JsonDeserialize(using = ContentDeserializer.class) Not used
public class Item implements Serializable {

    private String id;

    private List<Reference> references;

    private String type;

    private String content;

    private Map<String, String> additionalParameters;

    public List<Reference> getReferences() {
        return references;
    }

    public String getId() {
        return id;
    }

    public Item setId(String id) {
        this.id = id;
        return this;
    }

    public String getContent() {
        return content;
    }

    public String getType() {
        return type;
    }

    public Item setType(String type) {
        this.type = type;
        return this;
    }

    public Item setReferences(List<Reference> references) {
        this.references = references;
        return this;
    }

    public Item setContent(String content) {
        this.content = content;
        return this;
    }

    public Map<String, String> getAdditionalParameters() {
        return additionalParameters;
    }

    public Item setAdditionalParameters(Map<String, String> additionalParameters) {
        this.additionalParameters = additionalParameters;
        return this;
    }

    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", references=" + references +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                ", additionalParameters=" + additionalParameters +
                '}';
    }
}
