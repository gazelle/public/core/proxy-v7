package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.RawMessage;
import net.ihe.gazelle.proxy.model.ws.messages.TcpMessageDTO;

public class TcpMessageTransformer extends AbstractMessageTransformer<TcpMessageDTO, RawMessage> {

    @Override
    public RawMessage transform(TcpMessageDTO message, Connection connection) {
        return super.initMessage(message, connection);
    }

    @Override
    protected RawMessage createMessage() {
        return new RawMessage();
    }
}
