package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.interlay.serial.message.HL7v2MessageTransformer;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.messages.HL7v2MessageDTO;

import java.util.ArrayList;
import java.util.List;

public class HL7v2MessageTransformerFactory implements MessageTransformerFactory {
    @Override
    public HL7v2MessageTransformer createMessageTransformer() {
        return new HL7v2MessageTransformer();
    }

    @Override
    public List<Class<? extends Message>> getTypes() {
        ArrayList<Class<? extends Message>> types = new ArrayList<>();
        types.add(HL7v2MessageDTO.class);
        return types;
    }
}
