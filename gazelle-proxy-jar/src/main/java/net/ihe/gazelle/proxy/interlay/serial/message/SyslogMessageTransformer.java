package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.SyslogMessage;
import net.ihe.gazelle.proxy.model.ws.messages.SyslogMessageDTO;

public class SyslogMessageTransformer extends AbstractMessageTransformer<SyslogMessageDTO, SyslogMessage> {

    @Override
    public SyslogMessage transform(SyslogMessageDTO message, Connection connection) {
        SyslogMessage syslogMessage = super.initMessage(message, connection);
        syslogMessage.setMessageId(message.getMessageId());
        syslogMessage.setFacility(message.getFacility());
        syslogMessage.setSeverity(message.getSeverity());
        syslogMessage.setAppName(message.getAppName());
        syslogMessage.setProcId(message.getProcId());
        syslogMessage.setTag(message.getTag());
        syslogMessage.setPayLoad(message.getPayLoad());
        syslogMessage.setTimestamp(message.getTimestamp());
        syslogMessage.setHostName(message.getHostName());
        return syslogMessage;
    }

    @Override
    protected SyslogMessage createMessage() {
        return new SyslogMessage();
    }
}
