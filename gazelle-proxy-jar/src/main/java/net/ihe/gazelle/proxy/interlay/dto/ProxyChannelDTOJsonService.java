package net.ihe.gazelle.proxy.interlay.dto;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;

import java.util.List;


public interface ProxyChannelDTOJsonService {

    List<ProxyChannel> convertToProxyChannels(List<ProxyChannelDTOJson> proxyChannelDTOJson);

    List<ProxyChannelDTOJson> convertToProxyDTOJsons(Iterable<ProxyChannel> proxyChannels);

    ProxyChannel convertToProxyChannel(ProxyChannelDTOJson proxyChannelDTOJson);

    String resolveIpIfHost(String host);
}
