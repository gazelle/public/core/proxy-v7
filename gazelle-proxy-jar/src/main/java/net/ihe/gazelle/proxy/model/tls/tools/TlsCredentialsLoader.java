package net.ihe.gazelle.proxy.model.tls.tools;

import net.ihe.gazelle.proxy.model.tls.keystore.KeyStoreType;
import net.ihe.gazelle.proxy.model.tls.structure.TLSCredentials;
import net.ihe.gazelle.proxy.model.tls.trustore.TrustStoreType;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class TlsCredentialsLoader {

    public static final String FAILED_TO_LOAD_CREDENTIALS_FROM = "Failed to load credentials from ";

    public static TLSCredentials loadSpecificKeyCredential(String path, String password, String alias, KeyStoreType keyStoreType) {
        return loadSpecificKeyCredential(path, password, alias, password, keyStoreType);
    }

    public static TLSCredentials loadSpecificKeyCredential(String path, String password, String alias, String privateKeyPassword, KeyStoreType keyStoreType) {

        try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {
            KeyStore ks = KeyStore.getInstance(keyStoreType.getName());
            ks.load(inputStream, password.toCharArray());

            return loadSpecificKeyCredential(ks, alias, privateKeyPassword);

        } catch (Exception e) {
            throw new RuntimeException(FAILED_TO_LOAD_CREDENTIALS_FROM + path, e);
        }
    }

    public static List<TLSCredentials> loadAllKeyEntryCredentials(String path, String password, KeyStoreType keyStoreType) {
        return loadAllKeyEntryCredentials(path, password, password, keyStoreType);
    }

    public static List<X509Certificate> loadAllTrustCertEntryCredentials(String path, String password, TrustStoreType trustStoreType) {
        return loadAllTrustCertEntryCredentials(path, password, password, trustStoreType);
    }

    public static List<TLSCredentials> loadAllKeyEntryCredentials(String path, String password, String privateKeyPassword, KeyStoreType keyStoreType) {

        try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {
            KeyStore ks = KeyStore.getInstance(keyStoreType.getName());
            ks.load(inputStream, password.toCharArray());

            return loadAllKeyEntryCredentials(ks, privateKeyPassword);

        } catch (Exception e) {
            throw new RuntimeException(FAILED_TO_LOAD_CREDENTIALS_FROM + path, e);
        }
    }

    public static List<X509Certificate> loadAllTrustCertEntryCredentials(String path, String password, String privateKeyPassword, TrustStoreType trustStoreType) {

        try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {
            KeyStore ks = KeyStore.getInstance(trustStoreType.getName());
            ks.load(inputStream, password.toCharArray());

            return loadAllTrustCertEntryCredentials(ks, privateKeyPassword);

        } catch (Exception e) {
            throw new RuntimeException(FAILED_TO_LOAD_CREDENTIALS_FROM + path, e);
        }
    }

    private static List<TLSCredentials> loadAllKeyEntryCredentials(KeyStore ks, String privateKeyPassword) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        List<TLSCredentials> credentials = new ArrayList<>();

        X509Certificate[] certificateChain = null;
        PrivateKey privateKey = null;
        String alias = null;

        Enumeration<String> aliases = ks.aliases();
        for (; aliases.hasMoreElements(); ) {
            alias = aliases.nextElement();
            TLSCredentials tlsCredentials = loadSpecificKeyCredential(ks, alias, privateKeyPassword);
            if(tlsCredentials != null){
                credentials.add(tlsCredentials);
            }
        }
        return credentials;
    }

    private static List<X509Certificate> loadAllTrustCertEntryCredentials(KeyStore ks, String privateKeyPassword) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        List<X509Certificate> trustedCertificates = new ArrayList<>();

        String alias = null;

        Enumeration<String> aliases = ks.aliases();
        for (; aliases.hasMoreElements(); ) {
            alias = aliases.nextElement();

            // We only get trustCertENtryCertificate
            X509Certificate certificate = (X509Certificate) ks.getCertificate(alias);

            if(certificate != null && !ks.isKeyEntry(alias)){
                trustedCertificates.add(certificate);
            }
        }
        return trustedCertificates;
    }

    private static TLSCredentials loadSpecificKeyCredential(KeyStore ks, String alias, String privateKeyPassword) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        TLSCredentials tlsCredentials = null
                ;
        Certificate[] certificates = ks.getCertificateChain(alias);
        /* return the certificate chain (ordered with the user's certificate first
        and the root certificate authority last), or null if the given alias
        does not exist or does not contain a certificate chain (i.e., the given
        alias identifies either a trusted certificate entry or a
        key entry without a certificate chain).*/
        if(certificates != null) {
            X509Certificate[] certificateChain = new X509Certificate[certificates.length];
            System.arraycopy(certificates, 0, certificateChain, 0, certificates.length);

            Key key = ks.getKey(alias, privateKeyPassword.toCharArray());
            PrivateKey privateKey = (PrivateKey) key;
            tlsCredentials = new TLSCredentials(certificateChain, alias, privateKey);
        }
        return tlsCredentials;
    }

}
