package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.model.tls.keystore.CipherSuiteType;
import net.ihe.gazelle.proxy.model.tls.keystore.ProtocolType;

import javax.persistence.*;

@Entity
@Table(name = "pxy_connection_tls")
@PrimaryKeyJoinColumn(name = "connection_id")
public class ConnectionTls extends Connection {

    @Column(name = "proxy_certificate_subject")
    private String proxyCertificateSubject;

    @Column(name = "renegociation_counter")
    private int renegociationCounter;

    // Informations between the initiator and the secured channel

    @Column(name = "protocol_between_init_proxy")
    @Enumerated(EnumType.STRING)
    private ProtocolType protocolUsedBetweenInitAndProxy;

    @Column(name = "cipher_suite_between_init_proxy")
    @Enumerated(EnumType.STRING)
    private CipherSuiteType cipherSuiteUsedBetweenInitAndProxy;

    @Column(name = "init_certificate_subject")
    private String initiatorCertificateSubject;

    @Column(name = "handshake_notification_between_init_proxy")
    private String handshakeNotificationBetweenInitAndProxy;

    //Informations between the responder and the secured channel

    @Column(name = "protocol_between_resp_proxy")
    @Enumerated(EnumType.STRING)
    private ProtocolType protocolUsedBetweenRespAndProxy;

    @Column(name = "cipher_suite_between_resp_proxy")
    @Enumerated(EnumType.STRING)
    private CipherSuiteType cipherSuiteUsedBetweenRespAndProxy;

    @Column(name = "resp_certificate_subject")
    private String responderCertificateSubject;

    @Column(name = "handshake_notification_between_resp_proxy")
    private String handshakeNotificationBetweenRespAndProxy;

    public ConnectionTls() {
        super();
        setSecured(true);
    }

    public ConnectionTls(String proxyCertificateSubject, ProtocolType protocolUsedBetweenInitAndProxy, CipherSuiteType cipherSuiteUsedBetweenInitAndProxy, String initiatorCertificateSubject, ProtocolType protocolUsedBetweenRespAndProxy, CipherSuiteType cipherSuiteUsedBetweenRespAndProxy, String responderCertificateSubject) {
        super();
        this.proxyCertificateSubject = proxyCertificateSubject;
        this.protocolUsedBetweenInitAndProxy = protocolUsedBetweenInitAndProxy;
        this.cipherSuiteUsedBetweenInitAndProxy = cipherSuiteUsedBetweenInitAndProxy;
        this.initiatorCertificateSubject = initiatorCertificateSubject;
        this.protocolUsedBetweenRespAndProxy = protocolUsedBetweenRespAndProxy;
        this.cipherSuiteUsedBetweenRespAndProxy = cipherSuiteUsedBetweenRespAndProxy;
        this.responderCertificateSubject = responderCertificateSubject;
    }

    public String getProxyCertificateSubject() {
        return proxyCertificateSubject;
    }

    public void setProxyCertificateSubject(String proxyCertificateSubject) {
        this.proxyCertificateSubject = proxyCertificateSubject;
    }

    public int getRenegociationCounter() {
        return renegociationCounter;
    }

    public void setRenegociationCounter(int renegociationCounter) {
        this.renegociationCounter = renegociationCounter;
    }

    public ProtocolType getProtocolUsedBetweenInitAndProxy() {
        return protocolUsedBetweenInitAndProxy;
    }

    public void setProtocolUsedBetweenInitAndProxy(ProtocolType protocolUsedBetweenInitAndProxy) {
        this.protocolUsedBetweenInitAndProxy = protocolUsedBetweenInitAndProxy;
    }

    public CipherSuiteType getCipherSuiteUsedBetweenInitAndProxy() {
        return cipherSuiteUsedBetweenInitAndProxy;
    }

    public void setCipherSuiteUsedBetweenInitAndProxy(CipherSuiteType cipherSuiteUsedBetweenInitAndProxy) {
        this.cipherSuiteUsedBetweenInitAndProxy = cipherSuiteUsedBetweenInitAndProxy;
    }

    public String getInitiatorCertificateSubject() {
        return initiatorCertificateSubject;
    }

    public void setInitiatorCertificateSubject(String initiatorCertificateSubject) {
        this.initiatorCertificateSubject = initiatorCertificateSubject;
    }

    public String getHandshakeNotificationBetweenInitAndProxy() {
        return handshakeNotificationBetweenInitAndProxy;
    }

    public void setHandshakeNotificationBetweenInitAndProxy(String handshakeNotificationBetweenInitAndProxy) {
        this.handshakeNotificationBetweenInitAndProxy = handshakeNotificationBetweenInitAndProxy;
    }

    public ProtocolType getProtocolUsedBetweenRespAndProxy() {
        return protocolUsedBetweenRespAndProxy;
    }

    public void setProtocolUsedBetweenRespAndProxy(ProtocolType protocolUsedBetweenRespAndProxy) {
        this.protocolUsedBetweenRespAndProxy = protocolUsedBetweenRespAndProxy;
    }

    public CipherSuiteType getCipherSuiteUsedBetweenRespAndProxy() {
        return cipherSuiteUsedBetweenRespAndProxy;
    }

    public void setCipherSuiteUsedBetweenRespAndProxy(CipherSuiteType cipherSuiteUsedBetweenRespAndProxy) {
        this.cipherSuiteUsedBetweenRespAndProxy = cipherSuiteUsedBetweenRespAndProxy;
    }

    public String getResponderCertificateSubject() {
        return responderCertificateSubject;
    }

    public void setResponderCertificateSubject(String responderCertificateSubject) {
        this.responderCertificateSubject = responderCertificateSubject;
    }

    public String getHandshakeNotificationBetweenRespAndProxy() {
        return handshakeNotificationBetweenRespAndProxy;
    }

    public void setHandshakeNotificationBetweenRespAndProxy(String handshakeNotificationBetweenRespAndProxy) {
        this.handshakeNotificationBetweenRespAndProxy = handshakeNotificationBetweenRespAndProxy;
    }
}
