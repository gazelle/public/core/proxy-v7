package net.ihe.gazelle.proxy.interlay.factory.connection;

import net.ihe.gazelle.proxy.application.factory.ObjectDeserializerFactory;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.interlay.serial.ConnectionErrorDeserializer;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;

import java.util.List;

public class ConnectionErrorDeserializerFactory implements ObjectDeserializerFactory {

    @Override
    public <T> ObjectDeserializer<T> createObjectDeserializer() {
        return (ObjectDeserializer<T>) new ConnectionErrorDeserializer();
    }

    @Override
    public boolean enabled(Item item) {
        return ConnectionDTO.TYPE.equals(item.getType()) && item.getAdditionalParameters() != null
                && item.getAdditionalParameters().containsKey(ConnectionDTO.CONNECTION_ERROR_KEY)
                && !ConnectionDTO.TLS_ERROR_TYPE.equals(item.getAdditionalParameters().get(ConnectionDTO.CONNECTION_ERROR_KEY));
    }

}
