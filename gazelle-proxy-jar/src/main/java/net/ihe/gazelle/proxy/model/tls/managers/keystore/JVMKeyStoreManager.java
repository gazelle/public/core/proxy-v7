package net.ihe.gazelle.proxy.model.tls.managers.keystore;

import net.ihe.gazelle.proxy.managers.KeyStoreManager;
import net.ihe.gazelle.proxy.model.tls.keystore.KeyStoreType;
import net.ihe.gazelle.proxy.model.tls.structure.TLSCredentials;
import net.ihe.gazelle.proxy.model.tls.tools.TlsCredentialsLoader;

import java.util.List;

public class JVMKeyStoreManager extends KeyStoreManager {

    @Override
    public TLSCredentials getProxyCredentials() {
        TLSCredentials proxyCredentials = null;

        String keyStorePath = System.getProperty("javax.net.ssl.keyStore");
        String keyStorePassword = System.getProperty("javax.net.ssl.keyStorePassword");
        String keyStoreType = System.getProperty("javax.net.ssl.keyStoreType");
        KeyStoreType keyStoreTypeEnum;

        if(keyStoreType == null){
            keyStoreTypeEnum = KeyStoreType.JKS;
        } else {
            try {
                keyStoreTypeEnum = KeyStoreType.valueOf(keyStoreType);
            } catch (Exception e) {
                throw new Error("KeyStore type is not supported", e);
            }
        }

        List<TLSCredentials> keyStoreCredentials = TlsCredentialsLoader.loadAllKeyEntryCredentials(keyStorePath, keyStorePassword, keyStoreTypeEnum);
        // Because of no specific alias for JVM, we take the first certificate chain associated with a private key
        proxyCredentials = keyStoreCredentials.get(0);

        if(proxyCredentials == null){
            throw new Error("No proxy credential in keystore");
        } else {
            return proxyCredentials;
        }
    }
}
