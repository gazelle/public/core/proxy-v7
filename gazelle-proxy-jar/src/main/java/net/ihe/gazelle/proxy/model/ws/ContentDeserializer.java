package net.ihe.gazelle.proxy.model.ws;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class ContentDeserializer extends StdDeserializer<Object> {
    public ContentDeserializer(){
        super(Object.class);
    }

    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode node = p.getCodec().readTree(p);

        // Determine the type of the content based on the "type" property in the JSON
        JsonNode typeNode = node.get("type");
        if (typeNode != null) {
            String type = typeNode.asText();
            System.out.println("deserialize type:" + type);
            if ("message".equals(type)) {
                return p.getCodec().treeToValue(node.get("content"), Message.class);
            } else if ("connection".equals(type)) {
                return p.getCodec().treeToValue(node.get("content"), ConnectionDTO.class);
            }
        }

        // Return null or handle other cases if needed
        return null;
    }
}
