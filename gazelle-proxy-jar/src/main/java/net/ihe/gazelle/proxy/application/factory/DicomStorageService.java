package net.ihe.gazelle.proxy.application.factory;

public interface DicomStorageService {

    String saveDicomDataset(byte[] dicomDataset);
}
