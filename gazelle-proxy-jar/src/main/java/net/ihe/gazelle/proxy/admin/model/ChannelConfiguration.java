package net.ihe.gazelle.proxy.admin.model;

import net.ihe.gazelle.proxy.model.channel.ChannelType;


public class ChannelConfiguration {

    protected int proxyProviderPort;

    protected String proxyConsumerHost;

    protected int proxyConsumerPort;

    protected ChannelType channelType;

    protected boolean secureChannel;

    protected boolean persistent;

    protected boolean httpRewrite;

    protected boolean sniEnabled;

    public ChannelConfiguration(int proxyProviderPort, String proxyConsumerHost, int proxyConsumerPort, ChannelType channelType, boolean secureChannel, boolean persistent) {
        this.proxyProviderPort = proxyProviderPort;
        this.proxyConsumerHost = proxyConsumerHost;
        this.proxyConsumerPort = proxyConsumerPort;
        this.channelType = channelType;
        this.secureChannel = secureChannel;
        this.persistent = persistent;
    }

    public ChannelConfiguration() {
    }


    public int getProxyProviderPort() {
        return proxyProviderPort;
    }

    public void setProxyProviderPort(int proxyProviderPort) {
        this.proxyProviderPort = proxyProviderPort;
    }

    public String getProxyConsumerHost() {
        return proxyConsumerHost;
    }

    public void setProxyConsumerHost(String proxyConsumerHost) {
        this.proxyConsumerHost = proxyConsumerHost;
    }

    public int getProxyConsumerPort() {
        return proxyConsumerPort;
    }

    public void setProxyConsumerPort(int proxyConsumerPort) {
        this.proxyConsumerPort = proxyConsumerPort;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public boolean isSecureChannel() {
        return secureChannel;
    }

    public void setSecureChannel(boolean secureChannel) {
        this.secureChannel = secureChannel;
    }

    public boolean isPersistent() { return persistent; }

    public void setPersistent(boolean persistent) { this.persistent = persistent; }

    public boolean isHttpRewrite() {
        return httpRewrite;
    }

    public ChannelConfiguration setHttpRewrite(boolean httpRewrite) {
        this.httpRewrite = httpRewrite;
        return this;
    }

    public boolean isSniEnabled() {
        return sniEnabled;
    }

    public ChannelConfiguration setSniEnabled(boolean sniEnabled) {
        this.sniEnabled = sniEnabled;
        return this;
    }
}
