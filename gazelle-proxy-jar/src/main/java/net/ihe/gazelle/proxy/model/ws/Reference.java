package net.ihe.gazelle.proxy.model.ws;

public class Reference {

    private String value;

    private String type;

    public String getValue() {
        return value;
    }

    public Reference setValue(String value) {
        this.value = value;
        return this;
    }

    public String getType() {
        return type;
    }

    public Reference setType(String type) {
        this.type = type;
        return this;
    }

    public String toString() {
        return "Reference{" +
                "value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
