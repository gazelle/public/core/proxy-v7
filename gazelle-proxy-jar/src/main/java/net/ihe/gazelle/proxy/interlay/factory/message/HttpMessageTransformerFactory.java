package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.interlay.serial.message.HttpMessageTransformer;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpRequestDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpResponseDTO;

import java.util.ArrayList;
import java.util.List;

public class HttpMessageTransformerFactory implements MessageTransformerFactory {

    @Override
    public HttpMessageTransformer createMessageTransformer() {
        return new HttpMessageTransformer();
    }

    @Override
    public List<Class<? extends Message>> getTypes() {
        ArrayList<Class<? extends Message>> types = new ArrayList<>();
        types.add(HttpMessageDTO.class);
        types.add(HttpRequestDTO.class);
        types.add(HttpResponseDTO.class);
        return types;
    }
}
