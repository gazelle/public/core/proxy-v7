package net.ihe.gazelle.proxy.application.factory;

import net.ihe.gazelle.proxy.application.record.RecordService;
import net.ihe.gazelle.proxy.model.ws.Item;

public interface RecordServiceFactory {

    RecordService createRecordService(Item item);
}
