package net.ihe.gazelle.proxy.application.channel;

import net.ihe.gazelle.proxy.interlay.dto.ProxyChannelDTOJson;

import java.util.List;

public interface ProxyChannelPersistenceService {

    void save(ProxyChannelDTOJson proxyChannel);

    void delete(int port);

    List<ProxyChannelDTOJson> findAll();

    void deleteAll();

}
