package net.ihe.gazelle.proxy.admin.model;

import net.ihe.gazelle.proxy.model.tls.trustore.TrustStoreLoadingMethod;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "pxy_secured_channel_configuration_truststore_details", schema = "public")
@SequenceGenerator(name = "pxy_secured_channel_configuration_truststore_details_sequence", sequenceName = "pxy_secured_channel_configuration_truststore_details_id_seq", allocationSize = 1)
public class TrustStoreDetails implements Serializable {

    private static final long serialVersionUID = 2475489103172817264L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pxy_secured_channel_configuration_truststore_details_sequence")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "loading_method", nullable = false)
    private TrustStoreLoadingMethod loadingMethod;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TrustStoreLoadingMethod getLoadingMethod() {
        return loadingMethod;
    }

    public void setLoadingMethod(TrustStoreLoadingMethod loadingMethod) {
        this.loadingMethod = loadingMethod;
    }

}
