package net.ihe.gazelle.proxy.interlay.factory.connection;

import net.ihe.gazelle.proxy.interlay.dao.JdbcConnectionProvider;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.ConnectionQuery;
import net.ihe.gazelle.proxy.model.message.RawMessage;

import javax.ws.rs.core.Response;
import java.sql.*;

public class DBConnectionProvider implements ConnectionProvider{

    @Override
    public Connection getConnection(int id) {
        java.sql.Connection jdbcConnection = JdbcConnectionProvider.getInstance().getConnection();
        try(PreparedStatement preparedStatement = jdbcConnection.prepareStatement("select * from pxy_connection where id = ?")) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            Connection connection = new Connection();
            if(resultSet.next()){
                connection.setId(resultSet.getInt("id"));
                connection.setInitiatorHostname(resultSet.getString("initiator_hostname"));
                connection.setInitiatorPort(resultSet.getInt("initiator_port"));
                connection.setResponderHostname(resultSet.getString("responder_hostname"));
                connection.setResponderPort(resultSet.getInt("responder_port"));
                connection.setProxyChannelPort(resultSet.getInt("proxy_channel_port"));
                return connection;
            }
            throw new IllegalArgumentException("Connection not found");
        } catch (SQLException e) {
            throw new IllegalArgumentException("Connection not found");
        }
    }
}
