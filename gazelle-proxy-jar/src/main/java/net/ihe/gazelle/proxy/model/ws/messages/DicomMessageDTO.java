package net.ihe.gazelle.proxy.model.ws.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.proxy.model.ws.Message;

import java.util.Date;


@JsonTypeName(DicomMessageDTO.TYPE)
public class DicomMessageDTO extends Message {

    public static final String TYPE = "DICOM_MESSAGE";

    private  byte[] commandSet;
    private  String transferSyntax;
    private byte[] dataSet;


    public String getTransferSyntax() {
        return transferSyntax;
    }

    public DicomMessageDTO setTransferSyntax(String transferSyntax) {
        this.transferSyntax = transferSyntax;
        return this;
    }

    public byte[] getCommandSet() {
        return commandSet;
    }

    public DicomMessageDTO setCommandSet(byte[] commandSet) {
        this.commandSet = commandSet;
        return this;
    }

    public DicomMessageDTO setContent(byte[] content) {
        super.setContent(content);
        return this;
    }

    public DicomMessageDTO setCaptureDate(Date captureDate) {
        super.setCaptureDate(captureDate);
        return this;
    }

    public byte[] getDataSet() {
        return dataSet;
    }

    public DicomMessageDTO setDataSet(byte[] dataSet) {
        this.dataSet = dataSet;
        return this;
    }
}
