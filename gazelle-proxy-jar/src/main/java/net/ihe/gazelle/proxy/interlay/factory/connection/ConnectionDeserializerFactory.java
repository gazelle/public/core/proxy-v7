package net.ihe.gazelle.proxy.interlay.factory.connection;

import net.ihe.gazelle.proxy.application.factory.ObjectDeserializerFactory;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.interlay.serial.ConnectionDeserializer;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;

public class ConnectionDeserializerFactory implements ObjectDeserializerFactory {

    @Override
    public <T> ObjectDeserializer<T> createObjectDeserializer() {
        return (ObjectDeserializer<T>) new ConnectionDeserializer();
    }

    @Override
    public boolean enabled(Item item) {
        return ConnectionDTO.TYPE.equals(item.getType())
                && (item.getAdditionalParameters() == null || item.getAdditionalParameters().isEmpty())
                ;
    }
}
