package net.ihe.gazelle.proxy.model.ws;

import net.ihe.gazelle.proxy.model.ws.tls.Certificate;

import java.util.List;

public class TLSParameters {
    private String protocol;

    private String cipherSuite;

    private boolean isMTLS;


    private List<Certificate> certificateChain;

    public String getProtocol() {
        return protocol;
    }

    public TLSParameters setProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public String getCipherSuite() {
        return cipherSuite;
    }

    public TLSParameters setCipherSuite(String cipherSuite) {
        this.cipherSuite = cipherSuite;
        return this;
    }

    public boolean isMTLS() {
        return isMTLS;
    }

    public TLSParameters setMTLS(boolean MTLS) {
        isMTLS = MTLS;
        return this;
    }

    public List<Certificate> getCertificateChain() {
        return certificateChain;
    }

    public TLSParameters setCertificateChain(List<Certificate> certificateChain) {
        this.certificateChain = certificateChain;
        return this;
    }

    @Override
    public String toString() {
        return "TLSParameters{" +
                "protocol='" + protocol + '\'' +
                ", cipherSuite='" + cipherSuite + '\'' +
                ", isMTLS=" + isMTLS +
                '}';
    }
}
