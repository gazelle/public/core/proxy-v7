package net.ihe.gazelle.proxy.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

/**
 * Abstract parent class of JMS Queue Receivers and Senders. This parent class
 * contains methods and properties common to all Queue handler instances.
 */
public abstract class AbstractMessageHandler {

    private InitialContext jndiContext = null;
    private QueueConnectionFactory queueConnectionFactory;
    private QueueConnection queueConnection;
    private QueueSession queueSession;
    private Queue queue;

    private static Logger log = LoggerFactory.getLogger(AbstractMessageHandler.class);

    // Properties to initialize JNDI context
    private String javaNamingFactoryInitial;
    private String javaNamingProviderUrl;
    private String javaNamingFactoryUrlPkgs;

    // Names of JNDI objects to lookup
    private String jndiNameConnectionFactory;
    private String jndiNameJmsQueue;

    /**
     * Number of JMS connection retries upon connection error before stopping
     */
    private int maxJmsConnectionAttempts;

    /**
     * Initializes the JNDI context used to lookup Queue objects.
     */
    public void initialize() {
        // Initialize JNDI context
        try {
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put("java.naming.factory.initial", getJavaNamingFactoryInitial());
            env.put("java.naming.provider.url", getJavaNamingProviderUrl());
            env.put("java.naming.factory.url.pkgs", getJavaNamingFactoryUrlPkgs());

            jndiContext = new InitialContext(env);
        } catch (NamingException ex) {
            log.error("initialize() : Could not create JNDI context.", ex.getMessage());
        }

        // Get queue connection factory and queue objects from JNDI context
        try {
            queueConnectionFactory = (QueueConnectionFactory) jndiContext.lookup(getJndiNameConnectionFactory());

            queue = (Queue) jndiContext.lookup("ejb:" + getJndiNameJmsQueue());
        } catch (NamingException ex) {
            log.error("initialize() : JNDI lookup failed.", ex.getMessage());
        }
    }

    protected String getJavaNamingFactoryInitial() {
        return javaNamingFactoryInitial;
    }

    public void setJavaNamingFactoryInitial(String javaNamingFactoryInitial) {
        this.javaNamingFactoryInitial = javaNamingFactoryInitial;
    }

    protected String getJavaNamingFactoryUrlPkgs() {
        return javaNamingFactoryUrlPkgs;
    }

    public void setJavaNamingFactoryUrlPkgs(String javaNamingFactoryUrlPkgs) {
        this.javaNamingFactoryUrlPkgs = javaNamingFactoryUrlPkgs;
    }

    protected String getJavaNamingProviderUrl() {
        return javaNamingProviderUrl;
    }

    public void setJavaNamingProviderUrl(String javaNamingProviderUrl) {
        this.javaNamingProviderUrl = javaNamingProviderUrl;
    }

    protected String getJndiNameConnectionFactory() {
        return jndiNameConnectionFactory;
    }

    public void setJndiNameConnectionFactory(String jndiNameConnectionFactory) {
        this.jndiNameConnectionFactory = jndiNameConnectionFactory;
    }

    public String getJndiNameJmsQueue() {
        return jndiNameJmsQueue;
    }

    public void setJndiNameJmsQueue(String jndiNameJmsQueue) {
        this.jndiNameJmsQueue = jndiNameJmsQueue;
    }

    public int getMaxJmsConnectionAttempts() {
        return maxJmsConnectionAttempts;
    }

    public void setMaxJmsConnectionAttempts(int maxJmsConnectionAttempts) {
        this.maxJmsConnectionAttempts = maxJmsConnectionAttempts;
    }

    protected QueueSession getQueueSession() {
        return queueSession;
    }

    /**
     * Starts an active connection to the JMS Queue associated with this class.
     *
     * @return 'true' if JMS connection created; 'false' if connection was not
     * created
     */
    protected boolean startJmsConnection() {
        boolean isJmsConnectionSuccessful = false;

        // Track how many connection attempts are made
        int numConnectionAttempts = 0;

        // Continue to attempt a JMS connection up to the maximum number of
        // attempts.
        // JMS Connection should be created on the first try, but this loop will
        // reattempt if there
        // was a connection glitch.
        while ((isJmsConnectionSuccessful == false) && (numConnectionAttempts++ < getMaxJmsConnectionAttempts())) {
            try {
                // Create Queue connection
                queueConnection = queueConnectionFactory.createQueueConnection();

                // Use connection to create a Queue session
                queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

                // Create the Queue sender or receiver
                createQueueObject(queueSession, queue);

                // Start the connection, which allows the sender or receiver to
                // work actively with the Queue
                queueConnection.start();

                // Exit out of this loop, since connection is successful
                isJmsConnectionSuccessful = true;
            } catch (Throwable th) {
                log.error("Could not start the JMS sender connection to '" + getJndiNameJmsQueue() + "'.", th);
            }
        }

        return isJmsConnectionSuccessful;
    }

    protected void stopJmsConnection() {
        if (queueConnection != null) {
            try {
                // Closing the connection will also close the session, sender,
                // and receiver objects
                // created from the connection
                queueConnection.close();
            } catch (Throwable th) {
                log.error("Exception occurred while closing JMS connection.", th);
            } finally {
                queueConnection = null;
            }
        }
    }

    protected abstract void createQueueObject(QueueSession queueSession, Queue queue) throws JMSException;

}
