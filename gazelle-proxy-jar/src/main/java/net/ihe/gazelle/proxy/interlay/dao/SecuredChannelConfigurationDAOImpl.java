package net.ihe.gazelle.proxy.interlay.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfigurationQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("securedChannelConfigurationDAO")
public class SecuredChannelConfigurationDAOImpl implements SecuredChannelConfigurationDAO {

    @Override
    public SecuredChannelConfiguration find() {
        SecuredChannelConfigurationQuery securedChannelConfigurationQuery = new SecuredChannelConfigurationQuery();
        return securedChannelConfigurationQuery.getUniqueResult();
    }

    @Override
    public void save(SecuredChannelConfiguration securedChannelConfiguration) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(securedChannelConfiguration);
    }
}
