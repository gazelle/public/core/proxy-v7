package net.ihe.gazelle.proxy.model.ws.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.proxy.model.ws.Message;

@JsonTypeName(TcpMessageDTO.TYPE)
public class TcpMessageDTO extends Message {

    public static final String TYPE = "TCP_MESSAGE";

}
