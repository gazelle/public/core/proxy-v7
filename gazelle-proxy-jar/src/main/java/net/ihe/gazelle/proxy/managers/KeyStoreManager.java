package net.ihe.gazelle.proxy.managers;

import net.ihe.gazelle.proxy.model.tls.keystore.KeyStoreLoadingMethod;
import net.ihe.gazelle.proxy.model.tls.structure.TLSCredentials;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import java.net.Socket;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

public abstract class KeyStoreManager extends X509ExtendedKeyManager {

    private TLSCredentials credentials;

    public KeyStoreManager() {
        super();
        this.credentials = getProxyCredentials();
    }

    public List<KeyStoreLoadingMethod> getAvailableKeyStoreLoadingMethod() {
        return Arrays.asList(KeyStoreLoadingMethod.values());
    }

    public abstract TLSCredentials getProxyCredentials();

    public X509Certificate getProxyCertificate() {
        return credentials.getCertificateChain()[0];
    }

    @Override
    public String[] getClientAliases(String paramString, Principal[] paramArrayOfPrincipal) {
        return new String[] { credentials.getAlias() };
    }

    @Override
    public String chooseClientAlias(String[] paramArrayOfString, Principal[] paramArrayOfPrincipal,
            Socket paramSocket) {
        return credentials.getAlias();
    }

    @Override
    public String[] getServerAliases(String paramString, Principal[] paramArrayOfPrincipal) {
        return new String[] { credentials.getAlias() };
    }

    @Override
    public String chooseServerAlias(String paramString, Principal[] paramArrayOfPrincipal, Socket paramSocket) {
        return credentials.getAlias();
    }

    @Override
    public X509Certificate[] getCertificateChain(String paramString) {
        return credentials.getCertificateChain();
    }

    @Override
    public PrivateKey getPrivateKey(String paramString) {
        return credentials.getPrivateKey();
    }

    @Override
    public String chooseEngineClientAlias(String[] paramArrayOfString, Principal[] paramArrayOfPrincipal,
            SSLEngine paramSSLEngine) {
        return credentials.getAlias();
    }

    @Override
    public String chooseEngineServerAlias(String paramString, Principal[] paramArrayOfPrincipal,
            SSLEngine paramSSLEngine) {
        return credentials.getAlias();
    }

}
