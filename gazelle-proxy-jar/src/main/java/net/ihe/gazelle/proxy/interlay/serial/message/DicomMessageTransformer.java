package net.ihe.gazelle.proxy.interlay.serial.message;

import net.ihe.gazelle.proxy.application.factory.DicomStorageService;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.DicomMessage;
import net.ihe.gazelle.proxy.model.ws.messages.DicomMessageDTO;

import java.nio.charset.StandardCharsets;

public class DicomMessageTransformer extends AbstractMessageTransformer<DicomMessageDTO, DicomMessage> {


    private final DicomStorageService dicomStorageService;

    public DicomMessageTransformer(DicomStorageService dicomStorageService) {
        this.dicomStorageService = dicomStorageService;
    }

    @Override
    public DicomMessage transform(DicomMessageDTO message, Connection connection) {
        DicomMessage dicomMessage =  super.initMessage(message, connection);
        dicomMessage.setFileCommandSet(message.getCommandSet());
        dicomMessage.setFileTransfertSyntax(message.getTransferSyntax());
        if(message.getDataSet() != null){
            dicomMessage.setMessageReceived(dicomStorageService
                    .saveDicomDataset(message.getDataSet()).getBytes(StandardCharsets.UTF_8));
        }
        else{
            dicomMessage.setMessageReceived(null);
        }
        return dicomMessage;
    }

    @Override
    protected DicomMessage createMessage() {
        return new DicomMessage();
    }
}
