package net.ihe.gazelle.proxy.client;

public class ChannelStopException extends RuntimeException{

    public ChannelStopException(String message) {
        super(message);
    }

    public ChannelStopException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelStopException(Throwable cause) {
        super(cause);
    }
}
