package net.ihe.gazelle.proxy.managers;

import net.ihe.gazelle.proxy.admin.model.KeyStoreDetails;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.admin.model.TrustStoreDetails;
import net.ihe.gazelle.proxy.interlay.dao.SecuredChannelConfigurationDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import java.util.ServiceLoader;

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("securedChannelConfigurationManager")
public class SecuredChannelConfigurationManagerImpl implements SecuredChannelConfigurationManager {

    @In
    private SecuredChannelConfigurationDAO securedChannelConfigurationDAO;

    private final KeyStoreManagerFactory keyStoreManagerFactory;

    private final TrustStoreManagerFactory TrustStoreManagerFactory;

    public SecuredChannelConfigurationManagerImpl() {
        super();
        ServiceLoader<KeyStoreManagerFactory> keyStoreManagerFactoryServiceLoader = ServiceLoader.load(KeyStoreManagerFactory.class);
        ServiceLoader<TrustStoreManagerFactory> trustStoreManagerFactoryServiceLoader = ServiceLoader.load(TrustStoreManagerFactory.class);
        if (keyStoreManagerFactoryServiceLoader.iterator().hasNext()) {
            keyStoreManagerFactory = keyStoreManagerFactoryServiceLoader.iterator().next();
        }
        else{
            throw new RuntimeException("No KeyStoreManagerFactory implementation found");
        }
        if (trustStoreManagerFactoryServiceLoader.iterator().hasNext()) {
            TrustStoreManagerFactory = trustStoreManagerFactoryServiceLoader.iterator().next();
        }
        else{
            throw new RuntimeException("No TrustStoreManagerFactory implementation found");
        }
    }

    @Override
    public SecuredChannelConfiguration find() {
        return securedChannelConfigurationDAO.find();
    }

    @Override
    public void save(SecuredChannelConfiguration securedChannelConfiguration) {
        securedChannelConfigurationDAO.save(securedChannelConfiguration);
    }

    @Override
    public KeyStoreManager getKeyStore(KeyStoreDetails keyStoreDetails) {
        return keyStoreManagerFactory.getKeyStoreManager(keyStoreDetails);
    }

    @Override
    public TrustStoreManager getTrustStore(TrustStoreDetails trustStoreDetails) {
        return TrustStoreManagerFactory.getTrustStoreManager(trustStoreDetails);
    }

}
