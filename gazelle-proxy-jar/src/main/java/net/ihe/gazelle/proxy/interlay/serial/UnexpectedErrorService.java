package net.ihe.gazelle.proxy.interlay.serial;

import net.ihe.gazelle.proxy.model.ws.UnexpectedError;

import java.util.List;

public class UnexpectedErrorService {

    // This regex is used to remove the exception type from the message
    private static final String EXCEPTION_REGEX = "([a-zA-Z$#~!%-_0-9/]+\\.)+[a-zA-Z$#~!%-_0-9/]+: +";
    public static final int MAX_MESSAGE_SIZE = 150;


    public String extractFullError(List<UnexpectedError> errors){
        StringBuilder builder = new StringBuilder();
        for(UnexpectedError error : errors){
            builder
                    .append(extractFullError(error))
                    .append("\n\n");
        }
        return builder.toString();
    }

    private String formatThrowableMessage(String message){
        return slice(capitalize(exceptException(message)));
    }

    private String exceptException(String message){
        return message.replaceAll(EXCEPTION_REGEX, "");
    }

    private String capitalize(String message){
        return message.substring(0, 1).toUpperCase() + message.substring(1);
    }

    private String extractFullError(UnexpectedError error){
        StringBuilder builder = new StringBuilder();
        builder.append(formatThrowableMessage(error.getMessage()));
        if(error.getCause() != null && error.getCause().getMessage() != null && !error.getCause().getMessage().isEmpty()){
            builder.append("\n\tcaused by: ");
            builder.append(extractFullError(error.getCause()));
        }
        return builder.toString();
    }


    private String slice(String message){
        if(message == null){
            return "";
        }
        if(message.length()> MAX_MESSAGE_SIZE){
            return message.substring(0, MAX_MESSAGE_SIZE)+"...";
        }
        return message;
    }
}
