package net.ihe.gazelle.proxy.interlay.serial;

import net.ihe.gazelle.proxy.model.channel.ProxySide;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.message.ConnectionErrorMessage;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.UnexpectedError;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

public class ConnectionErrorDeserializer extends ConnectionDeserializer{


    private final UnexpectedErrorService unexpectedErrorService = new UnexpectedErrorService();

    @Override
    public ConnectionError deserialize(String body, Item... items) {

        Connection connection = super.deserialize(body, items);
        AbstractMessage message = instantiateErrorMessage();
        message.setConnection(connection);
        message.setMessageReceived(("Connection Error: "+unexpectedErrorService.extractFullError(super.connectionDTO.getUnexpectedErrors().getList())).getBytes(StandardCharsets.UTF_8));
        message.setFromIP(connection.getInitiatorHostname());
        message.setLocalPort(connection.getInitiatorPort());
        message.setToIP(connection.getResponderHostname());
        message.setRemotePort(connection.getResponderPort());
        message.setDateReceived(new Date());
        message.setProxySide(ProxySide.REQUEST); // Logically the connection issue is on the request side
        message.setProxyPort(connection.getProxyChannelPort());
        return new ConnectionError()
                .setConnection(connection)
                .setMessage(message);
    }

    protected AbstractMessage instantiateErrorMessage(){
        return new ConnectionErrorMessage();
    }






}
