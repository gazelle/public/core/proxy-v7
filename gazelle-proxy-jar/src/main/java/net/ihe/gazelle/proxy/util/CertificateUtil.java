package net.ihe.gazelle.proxy.util;

import java.security.*;
import java.security.cert.*;
import java.util.*;

public class CertificateUtil {

    /**
     * Checks whether given X.509 certificate is self-signed.
     *
     * @param cert {@link X509Certificate} to verify
     * @return true if the certificate is self-signed, false otherwise
     */
    public static boolean isSelfSigned(X509Certificate cert) {
        try {
            // Try to verify certificate signature with its own public key
            PublicKey key = cert.getPublicKey();
            cert.verify(key);
            return true;
        } catch (SignatureException sigEx) {
            // Invalid signature --> not self-signed
            return false;
        } catch (InvalidKeyException keyEx) {
            // Invalid key --> not self-signed
            return false;
        } catch (CertificateException e) {
            return false;
        } catch (NoSuchAlgorithmException e) {
            return false;
        } catch (NoSuchProviderException e) {
            return false;
        }
    }

    public static void doPKIXCheck(X509Certificate[] chain, List<X509Certificate> trustedCertificates) throws CertificateException, CertPathValidatorException {
        try {
            CertificateFactory cfs = CertificateFactory.getInstance("X.509");
            CertPath certPath = cfs.generateCertPath(Arrays.asList(chain));

            Set<TrustAnchor> trustAnchors = new HashSet<TrustAnchor>();
            for (X509Certificate cert : trustedCertificates) {
                trustAnchors.add(new TrustAnchor(cert, null));
            }

            PKIXParameters pkixParameters = new PKIXParameters(trustAnchors);
            pkixParameters.setExplicitPolicyRequired(false);
            pkixParameters.setRevocationEnabled(false);
            pkixParameters.setDate(new Date());

            CertPathValidator cpv = CertPathValidator.getInstance("PKIX");
            cpv.validate(certPath, pkixParameters);

        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e.getLocalizedMessage());
        } catch (InvalidAlgorithmParameterException e) {
            throw new CertificateException(e.getLocalizedMessage());
        }
    }

}
