package net.ihe.gazelle.proxy.model.ws;

import java.util.List;

public class UnexpectedErrors {

    private String rootType;

    private List<UnexpectedError> list;

    public String getRootType() {
        return rootType;
    }

    public UnexpectedErrors setRootType(String rootType) {
        this.rootType = rootType;
        return this;
    }

    public List<UnexpectedError> getList() {
        return list;
    }

    public UnexpectedErrors setList(List<UnexpectedError> list) {
        this.list = list;
        return this;
    }
}
