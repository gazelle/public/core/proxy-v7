package net.ihe.gazelle.proxy.interlay.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.application.record.SerializationException;
import net.ihe.gazelle.proxy.model.ws.Item;

import java.io.IOException;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;


public class ItemDeserializer implements ObjectDeserializer<Item> {

    private final ObjectMapper objectMapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Override
    public Item deserialize(String bodyRequest, Item... items) {
        if(items.length > 0){
            throw new IllegalArgumentException("ItemDeserializer does not accept multiple items");
        }
        try {
            return objectMapper.readValue(bodyRequest, Item.class);
        } catch (IOException e) {
            throw new SerializationException("Error while serializing: "+bodyRequest,e);
        }
    }

}
