package net.ihe.gazelle.proxy.interlay.serial.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.proxy.application.record.MessageTransformer;
import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.interlay.factory.connection.ConnectionProvider;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.Reference;

import java.io.IOException;
import java.util.List;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class MessageDeserializer implements ObjectDeserializer<AbstractMessage> {

    private final ObjectMapper mapper = new ObjectMapper()
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            ;

    private final MessageTransformerProvider transformerProvider = new MessageTransformerProvider();

    private final ConnectionProvider connectionProvider;

    public MessageDeserializer(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }


    /**
     * {@inheritDoc}
     * <ul>
     *     <li>Items is used to get the connection</li>
     * </ul>
     */
    @Override
    public AbstractMessage deserialize(String body, Item... items) {
        if(items.length != 1) {
            throw new RuntimeException("Expected exactly one item");
        }
        try {
            Item item = items[0];
            Message message = mapper.readValue(body, Message.class);
            MessageTransformer<Message,? extends AbstractMessage> transformer =
                    (MessageTransformer<Message, ? extends AbstractMessage>) transformerProvider.createTransformer(message.getClass());
            String connectionId = getConnectionRef(item.getReferences());
            if(connectionId == null){
                throw new IllegalStateException("Connection reference not found");
            }
            Connection connection = connectionProvider.getConnection(Integer.parseInt(connectionId));
            AbstractMessage abstractMessage = transformer.transform(message, connection);

            return abstractMessage;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getConnectionRef(List<Reference> references) {
        for (Reference reference : references) {
            if (reference.getType().equals(ConnectionDTO.TYPE)) {
                return reference.getValue();
            }
        }
        return null;
    }




}
