package net.ihe.gazelle.proxy.model.ws;

public class RawTcpMessage {

    private byte[] rawTcpMessageContent;

    public byte[] getRawTcpMessageContent() {
        return rawTcpMessageContent;
    }

    public void setRawTcpMessageContent(byte[] rawTcpMessageContent) {
        this.rawTcpMessageContent = rawTcpMessageContent;
    }
}
