package net.ihe.gazelle.proxy.client;

import net.ihe.gazelle.proxy.model.ws.channel.ProxyChannel;
import net.ihe.gazelle.proxy.model.ws.channel.SocketRequest;

import java.util.List;

public interface ProxyHttpClient {

    List<ProxyChannel> startChannels(SocketRequest socketRequest) throws ProxyClientException;

    void stopChannel(int port) throws ProxyClientException;

    List<ProxyChannel> getAllStartedChannels() throws ProxyClientException;

    int countChannels() throws ProxyClientException;

    void stopAllChannels() throws ProxyClientException;
}
