package net.ihe.gazelle.proxy.model.tls.trustore;

public enum TrustStoreType {
    JKS("JKS");

    private String name;

    TrustStoreType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
