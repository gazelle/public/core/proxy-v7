package net.ihe.gazelle.proxy.admin.model;

import java.security.Principal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Subject {

    private String commonName;
    private String organizationalUnit;
    private String organization;
    private String locality;
    private String stateOrProvinceName;
    private String countryName;

    public Subject(String commonName, String organizationalUnit, String organization, String locality, String stateOrProvinceName, String countryName) {
        this.commonName = commonName;
        this.organizationalUnit = organizationalUnit;
        this.organization = organization;
        this.locality = locality;
        this.stateOrProvinceName = stateOrProvinceName;
        this.countryName = countryName;
    }

    public Subject(Principal principal) {
        Pattern pattern = Pattern.compile("(?:^|,\\s?)(?:(?<name>[A-Z]+)=(?<val>\"(?:[^\"]|\"\")+\"|[^,]+))+");
        Matcher matcher = pattern.matcher(principal.getName());
        while (matcher.find()) {
            switch (matcher.group(1)) {
                case "CN" :
                    this.commonName = matcher.group(2);
                    break;
                case "OU" :
                    this.organizationalUnit = matcher.group(2);
                    break;
                case "O" :
                    this.organization = matcher.group(2);
                    break;
                case "L" :
                    this.locality = matcher.group(2);
                    break;
                case "ST" :
                    this.stateOrProvinceName = matcher.group(2);
                    break;
                case "C" :
                    this.countryName = matcher.group(2);
                    break;
            }
        }
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getOrganizationalUnit() {
        return organizationalUnit;
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        this.organizationalUnit = organizationalUnit;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStateOrProvinceName() {
        return this.stateOrProvinceName;
    }

    public void setStateOrProvinceName(String stateOrProvinceName) {
        this.stateOrProvinceName = stateOrProvinceName;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
