package net.ihe.gazelle.proxy.model.ws.channel;

public class ChannelStateException extends RuntimeException{

    public ChannelStateException(String message) {
        super(message);
    }

    public ChannelStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelStateException(Throwable cause) {
        super(cause);
    }

    protected ChannelStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
