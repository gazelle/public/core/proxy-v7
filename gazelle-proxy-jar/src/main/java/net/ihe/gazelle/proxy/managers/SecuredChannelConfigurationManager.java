package net.ihe.gazelle.proxy.managers;

import net.ihe.gazelle.proxy.admin.model.KeyStoreDetails;
import net.ihe.gazelle.proxy.admin.model.SecuredChannelConfiguration;
import net.ihe.gazelle.proxy.admin.model.TrustStoreDetails;

import javax.ejb.Local;

@Local
public interface SecuredChannelConfigurationManager {

    public SecuredChannelConfiguration find();

    public void save(SecuredChannelConfiguration securedChannelConfiguration);

    public KeyStoreManager getKeyStore(KeyStoreDetails keyStoreDetails);

    public TrustStoreManager getTrustStore(TrustStoreDetails trustStoreDetails);
}
