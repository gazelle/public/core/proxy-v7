package net.ihe.gazelle.proxy.model.ws.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.proxy.model.ws.Message;

@JsonTypeName(HL7v2MessageDTO.TYPE)
public class HL7v2MessageDTO extends Message {

    public static final String TYPE = "HL7V2_MESSAGE";

    private String hl7MessageType;
    private String hl7version;

    public String getHl7MessageType() {
        return hl7MessageType;
    }

    public HL7v2MessageDTO setHl7MessageType(String hl7MessageType) {
        this.hl7MessageType = hl7MessageType;
        return this;
    }

    public String getHl7version() {
        return hl7version;
    }

    public HL7v2MessageDTO setHl7version(String hl7version) {
        this.hl7version = hl7version;
        return this;
    }



}



