package net.ihe.gazelle.proxy.model.ws.tls;

public class Certificate {

    private final String id;
    private byte[] certPEM;

    public Certificate() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public byte[] getCertPEM() {
        return certPEM;
    }

    public Certificate setCertPEM(byte[] certPEM) {
        this.certPEM = certPEM;
        return this;
    }

    public String toString() {
        int certPemLength = certPEM == null ? 0 : certPEM.length;
        return "Certificate{" +
                "id='" + id + '\'' +
                ", certPEM with " + certPemLength + " bytes" +
                '}';
    }


}
