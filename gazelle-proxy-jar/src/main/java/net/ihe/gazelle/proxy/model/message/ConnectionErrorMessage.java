package net.ihe.gazelle.proxy.model.message;

import net.ihe.gazelle.proxy.model.channel.ChannelType;
import org.jboss.seam.annotations.Name;

import javax.persistence.Entity;

@Entity
@Name("connectionErrorMessage")
public class ConnectionErrorMessage extends AbstractMessage{



    @Override
    public ChannelType getChannelType() {
        return ChannelType.CONNECTION_ERROR;
    }

    @Override
    public String getInfoGUI() {
        return "<span style=\"color:red\"><span class=\"gzl-icon-exclamation-triangle\" style=\"color : red\"></span>Connection Error</span>";
    }

    @Override
    public String getLabel() {
        return getInfoGUI();
    }
}
