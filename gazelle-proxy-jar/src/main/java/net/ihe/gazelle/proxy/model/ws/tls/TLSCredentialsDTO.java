package net.ihe.gazelle.proxy.model.ws.tls;

import java.util.List;

public class TLSCredentialsDTO {

    private final String id;

    private byte[] privateKey;

    private List<Certificate> certificateChain;

    public TLSCredentialsDTO() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public TLSCredentialsDTO setPrivateKey(byte[] privateKey) {
        this.privateKey = privateKey;
        return this;
    }

    public List<Certificate> getCertificateChain() {
        return certificateChain;
    }

    public TLSCredentialsDTO setCertificateChain(List<Certificate> certificateChain) {
        this.certificateChain = certificateChain;
        return this;
    }

}
