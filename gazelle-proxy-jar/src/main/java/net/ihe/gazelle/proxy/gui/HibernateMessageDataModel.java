package net.ihe.gazelle.proxy.gui;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.proxy.interlay.dao.MessageFilter;
import net.ihe.gazelle.proxy.interlay.dao.MessageFilterStandard;
import net.ihe.gazelle.proxy.model.channel.ChannelType;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;

import javax.faces.context.FacesContext;

public class HibernateMessageDataModel<T extends AbstractMessage> extends HibernateDataModel<T> {

    /**
     *
     */
    private static final long serialVersionUID = 1308547516757053507L;
    private MessageFilter<T> messageFilter;

    public HibernateMessageDataModel(MessageFilter<T> messageFilter) {
        super(AbstractMessage.class);
        if (messageFilter.getClass().isAssignableFrom(MessageFilterStandard.class)) {
            MessageFilterStandard<?> messageFilterStandard = (MessageFilterStandard<?>) messageFilter;
            if (messageFilterStandard.getMessageType().equals(ChannelType.DICOM)) {
                if (messageFilterStandard.getDicomAffectedSopClassUID() == "") {
                    messageFilterStandard.setDicomAffectedSopClassUID(null);
                }
                if (messageFilterStandard.getInitiatorIP() == "") {
                    messageFilterStandard.setInitiatorIP(null);
                }
                if (messageFilterStandard.getResponderIP() == "") {
                    messageFilterStandard.setResponderIP(null);
                }
            }
            if (messageFilterStandard.getMessageType().equals(ChannelType.HL7v2)) {
                if (messageFilterStandard.getHl7MessageType() == "") {
                    messageFilterStandard.setHl7MessageType(null);
                }
            }
            if (messageFilterStandard.getMessageType().equals(ChannelType.HTTP)) {
                if (messageFilterStandard.getHttpMessageType() == "") {
                    messageFilterStandard.setHttpMessageType(null);
                }
            }
        }
        this.messageFilter = messageFilter;
    }

    @Override
    protected void appendFilters(FacesContext context, HQLQueryBuilder<T> queryBuilder) {
        super.appendFilters(context, queryBuilder);
        messageFilter.appendFilters(queryBuilder);
        this.resetCache();
    }

    @Override
    protected Object getId(T t) {
        return t.getId();
    }
}
