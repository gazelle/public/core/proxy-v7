package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.interlay.serial.message.TcpMessageTransformer;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.messages.TcpMessageDTO;

import java.util.ArrayList;
import java.util.List;

public class TcpMessageTransformerFactory implements MessageTransformerFactory {

    @Override
    public TcpMessageTransformer createMessageTransformer() {
        return new TcpMessageTransformer();
    }

    @Override
    public List<Class<? extends Message>> getTypes() {
        ArrayList<Class<? extends Message>> types = new ArrayList<>();
        types.add(TcpMessageDTO.class);
        return types;
    }
}
