package net.ihe.gazelle.proxy.model.ws.messages.http;

import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.proxy.model.ws.Message;

import java.util.Map;

@JsonTypeName(HttpMessageDTO.TYPE)
public class HttpMessageDTO extends Message {

    public static final String TYPE = "HTTP_MESSAGE";
    public static final String HTTP_REWRITE_KEY = "HTTP_REWRITE";
    private Map<String, String> headers;
    private String contentType;
    private String uri;
    private String version;
    private byte[] body;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public HttpMessageDTO setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public HttpMessageDTO setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public String getUri() {
        return uri;
    }

    public HttpMessageDTO setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public HttpMessageDTO setVersion(String version) {
        this.version = version;
        return this;
    }

    public byte[] getBody() {
        return body;
    }

    public HttpMessageDTO setBody(byte[] body) {
        this.body = body;
        return this;
    }
}