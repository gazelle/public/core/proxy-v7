package net.ihe.gazelle.proxy.client;

public class ChannelStartException extends RuntimeException{

    public ChannelStartException(String message) {
        super(message);
    }

    public ChannelStartException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChannelStartException(Throwable cause) {
        super(cause);
    }
}
