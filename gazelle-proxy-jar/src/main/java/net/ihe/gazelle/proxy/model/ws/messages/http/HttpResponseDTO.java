package net.ihe.gazelle.proxy.model.ws.messages.http;

import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName(HttpResponseDTO.TYPE)
public class HttpResponseDTO extends HttpMessageDTO {

    public static final String TYPE = "HTTP_RESPONSE";


    private int status;

    public int getStatus() {
        return status;
    }

    public HttpResponseDTO setStatus(int status) {
        this.status = status;
        return this;
    }
}
