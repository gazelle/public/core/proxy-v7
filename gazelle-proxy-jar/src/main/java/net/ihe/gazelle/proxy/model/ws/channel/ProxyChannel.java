package net.ihe.gazelle.proxy.model.ws.channel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.ihe.gazelle.proxy.model.channel.ChannelType;
import net.ihe.gazelle.proxy.model.ws.Actor;
import net.ihe.gazelle.proxy.model.ws.ParameterDTO;
import net.ihe.gazelle.proxy.model.ws.messages.http.HttpMessageDTO;
import net.ihe.gazelle.proxy.model.ws.tls.TLSConfiguration;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ProxyChannel implements Comparable<ProxyChannel>{

    private int proxyPort;
    private ChannelType type;

    private Actor responder;

    private boolean idleMode;

    private int timeout;

    private TLSConfiguration tlsConfiguration;

    private String tlsConfigurationRef;

    private Map<String, String> additionalParameters;

    private boolean persistent;


    public ProxyChannel(ProxyChannel proxyChannel) {
        this.proxyPort = proxyChannel.proxyPort;
        this.type = proxyChannel.type;
        this.responder = proxyChannel.responder;
        this.idleMode = proxyChannel.idleMode;
        this.timeout = proxyChannel.timeout;
        this.tlsConfigurationRef = proxyChannel.tlsConfigurationRef;
        this.tlsConfiguration = proxyChannel.tlsConfiguration;
        this.additionalParameters = proxyChannel.additionalParameters;
        this.persistent = proxyChannel.persistent;
    }

    public ProxyChannel() {

    }

    public int getProxyPort() {
        return proxyPort;
    }

    public ProxyChannel setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
        return this;
    }

    public ChannelType getType() {
        return type;
    }

    public ProxyChannel setType(ChannelType type) {
        this.type = type;
        return this;
    }

    public Actor getResponder() {
        return responder;
    }

    public ProxyChannel setResponder(Actor responder) {
        this.responder = responder;
        return this;
    }



    public TLSConfiguration getTlsConfiguration() {
        return tlsConfiguration;
    }

    public ProxyChannel setTlsConfiguration(TLSConfiguration tlsConfiguration) {
        this.tlsConfiguration = tlsConfiguration;
        if(tlsConfiguration != null)
            this.tlsConfigurationRef = tlsConfiguration.getId();
        return this;
    }

    public boolean isSecured() {
        return tlsConfiguration != null;
    }


    public Map<String, String> getAdditionalParameters() {
        return additionalParameters;
    }

    public ProxyChannel setAdditionalParameters(Map<String, String> additionalParameters) {
        this.additionalParameters = additionalParameters;
        return this;
    }

    public String getTlsConfigurationRef() {
        return tlsConfigurationRef;
    }


    public boolean isPersistent() {
        return persistent;
    }

    public ProxyChannel setPersistent(boolean persistent) {
        this.persistent = persistent;
        return this;
    }

    @Override
    public String toString() {
        return "Channel [" +
                "proxyPort=" + proxyPort +
                ", type=" + type +
                ", secured=" + isSecured() +
                ", responder=" + responder +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProxyChannel)) return false;
        ProxyChannel that = (ProxyChannel) o;
        return proxyPort == that.proxyPort;
    }

    @Override
    public int compareTo(ProxyChannel proxyChannel) {
        String host1;
        String host2;
        if (this.getResponder() == null || this.getResponder().getHostname() == null) {
            host1 = "";
        } else {
            host1 = this.getResponder().getHostname();
        }
        if (proxyChannel == null || proxyChannel.getResponder() == null
                || proxyChannel.getResponder().getHostname() == null) {
            host2 = "";
        } else {
            host2 = proxyChannel.getResponder().getHostname();
        }
        return host1.compareTo(host2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(proxyPort);
    }

    // FIXME: 24/05/2023 TO be removed (this is used to download the list of channels as CSV)
    @Deprecated
    @JsonIgnore
    public String[] getConnectionConfigAsArray() {
        return new String[] {getType().getIdentifier(), Boolean.toString(isSecured()), Integer.toString(getProxyPort()),
                getResponder().getHostname(), Integer.toString(getResponder().getPort()),
                Boolean.toString(isHttpRewrite()) };
    }


    // FIXME: 24/05/2023 only for jsf support, to be removed
    @Deprecated
    public boolean isSecuredChannel(){
        return isSecured();
    }



    // FIXME: 02/11/2023 Just for the encapsulation issue with Additional Parameter & CSV
    @Deprecated
    public boolean isHttpRewrite() {
        if(additionalParameters == null){
            return false;
        }
        return additionalParameters.containsKey(HttpMessageDTO.HTTP_REWRITE_KEY)
                && Boolean.parseBoolean(additionalParameters.get(HttpMessageDTO.HTTP_REWRITE_KEY))
                ;
    }

    public boolean isSniEnabled(){
        return tlsConfiguration != null && tlsConfiguration.isSniEnabled();
    }

    public void validate(){
        if(proxyPort <= 0){
            throw new IllegalArgumentException("Proxy port must be > 0");
        }
        if(responder == null){
            throw new IllegalArgumentException("Responder must be set");
        }
        if((responder.getHostname() == null || responder.getHostname().isEmpty())
        && (responder.getIp() == null || responder.getIp().isEmpty())){
            throw new IllegalArgumentException("Responder hostname or IP must be set");
        }
        if(responder.getPort() <= 0){
            throw new IllegalArgumentException("Responder port must be > 0");
        }
        if(type == null){
            throw new IllegalArgumentException("Channel type must be set");
        }
    }
}
