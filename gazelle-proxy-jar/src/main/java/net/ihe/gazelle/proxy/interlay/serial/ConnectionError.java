package net.ihe.gazelle.proxy.interlay.serial;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.Connection;

public class ConnectionError extends Connection {


    private Connection connection;

    private AbstractMessage message;


    public Connection getConnection() {
        return connection;
    }

    public ConnectionError setConnection(Connection connection) {
        this.connection = connection;
        return this;
    }

    public AbstractMessage getMessage() {
        return message;
    }

    public ConnectionError setMessage(AbstractMessage message) {
        this.message = message;
        return this;
    }


}
