package net.ihe.gazelle.proxy.model.ws.messages.http;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName(HttpRequestDTO.TYPE)
public class HttpRequestDTO extends HttpMessageDTO{

    public static final String TYPE = "HTTP_REQUEST";

    private String method;

    public String getMethod() {
        return method;
    }
    public HttpRequestDTO setMethod(String method) {
        this.method = method;
        return this;
    }
}
