package net.ihe.gazelle.proxy.application.record;

public interface RecordService {

    /**
     * Record the content of the message
     * @param content the content to record
     * @return the id of the record
     */
    String record(final Object content);
}
