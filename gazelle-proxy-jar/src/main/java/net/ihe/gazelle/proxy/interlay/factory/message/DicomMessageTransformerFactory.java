package net.ihe.gazelle.proxy.interlay.factory.message;

import net.ihe.gazelle.proxy.application.factory.MessageTransformerFactory;
import net.ihe.gazelle.proxy.interlay.serial.message.DicomMessageTransformer;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.messages.DicomMessageDTO;

import java.util.ArrayList;
import java.util.List;

public class DicomMessageTransformerFactory implements MessageTransformerFactory {

    @Override
    public DicomMessageTransformer createMessageTransformer() {
        return new DicomMessageTransformer(new FileDicomStorageService());
    }

    @Override
    public List<Class<? extends Message>> getTypes() {
        ArrayList<Class<? extends Message>> types = new ArrayList<>();
        types.add(DicomMessageDTO.class);
        return types;
    }
}
