package net.ihe.gazelle.proxy.model.ws.messages;

import com.fasterxml.jackson.annotation.JsonTypeName;
import net.ihe.gazelle.proxy.model.ws.Message;

@JsonTypeName(SyslogMessageDTO.TYPE)
public class SyslogMessageDTO extends Message {

    public static final String TYPE = "SYSLOG_MESSAGE";

    private String timestamp;
    private String hostName;
    private String appName;
    private String procId;
    private String messageId;
    private String tag;
    private String payLoad;
    private int facility;
    private int severity;

    public String getTimestamp() {
        return timestamp;
    }

    public SyslogMessageDTO setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getHostName() {
        return hostName;
    }

    public SyslogMessageDTO setHostName(String hostName) {
        this.hostName = hostName;
        return this;
    }

    public String getAppName() {
        return appName;
    }

    public SyslogMessageDTO setAppName(String appName) {
        this.appName = appName;
        return this;
    }

    public String getProcId() {
        return procId;
    }

    public SyslogMessageDTO setProcId(String procId) {
        this.procId = procId;
        return this;
    }

    public String getMessageId() {
        return messageId;
    }

    public SyslogMessageDTO setMessageId(String messageId) {
        this.messageId = messageId;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public SyslogMessageDTO setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public String getPayLoad() {
        return payLoad;
    }

    public SyslogMessageDTO setPayLoad(String payLoad) {
        this.payLoad = payLoad;
        return this;
    }



    public int getFacility() {
        return facility;
    }

    public SyslogMessageDTO setFacility(int facility) {
        this.facility = facility;
        return this;
    }

    public int getSeverity() {
        return severity;
    }

    public SyslogMessageDTO setSeverity(int severity) {
        this.severity = severity;
        return this;
    }
}
