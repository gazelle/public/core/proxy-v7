package net.ihe.gazelle.proxy.interlay.serial;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.TlsErrorMessage;

public class ConnectionTLSErrorDeserializer extends ConnectionErrorDeserializer {


    @Override
    protected AbstractMessage instantiateErrorMessage() {
        return new TlsErrorMessage();
    }
}
