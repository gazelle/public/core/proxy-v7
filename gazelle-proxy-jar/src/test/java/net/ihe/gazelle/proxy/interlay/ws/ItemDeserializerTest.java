package net.ihe.gazelle.proxy.interlay.ws;

import net.ihe.gazelle.proxy.application.record.ObjectDeserializer;
import net.ihe.gazelle.proxy.interlay.factory.connection.ConnectionProvider;
import net.ihe.gazelle.proxy.interlay.serial.ObjectDeserializerProvider;
import net.ihe.gazelle.proxy.interlay.serial.message.MessageDeserializer;
import net.ihe.gazelle.proxy.model.message.*;
import net.ihe.gazelle.proxy.model.ws.ConnectionDTO;
import net.ihe.gazelle.proxy.model.ws.Item;
import net.ihe.gazelle.proxy.model.ws.Message;
import net.ihe.gazelle.proxy.model.ws.Reference;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ItemDeserializerTest {

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final ObjectDeserializerProvider objectDeserializerProvider = new ObjectDeserializerProvider();

    // FIXME: 16/08/2023 Tests need to be updated

    @Test
    public void testDeserializeUnsecuredConnection() {


        String connectionBody = "{\"references\":[],\"type\":\"CONNECTION\",\"content\":\"{\\\"channelSummary\\\":{\\\"proxyPort\\\":1234,\\\"type\\\":\\\"tcp\\\",\\\"tlsParametersSummary\\\":null,\\\"responder\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"responder.example.com\\\",\\\"port\\\":1236}},\\\"initiator\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"initiator.example.com\\\",\\\"port\\\":1235}}\"}";


        Item item = new ItemDeserializer().deserialize(connectionBody);

        ObjectDeserializer<Object> objectDeserializer = objectDeserializerProvider.createObjectDeserializer(item);
        Object object =  objectDeserializer.deserialize(item.getContent(), item);
        Assert.assertTrue(object instanceof Connection);
        Connection connection = (Connection) object;
        System.out.println(connection);

        assertEquals(ConnectionDTO.TYPE, item.getType());
        assertEquals(1234, connection.getProxyChannelPort().intValue());
        assertEquals("responder.example.com", connection.getResponderHostname());
        assertEquals("initiator.example.com", connection.getInitiatorHostname());
    }



    @Test
    public void testDeserializeRawMessage() {

        final Connection connectionMock = createConnectionMock();

        ObjectDeserializerProvider messageDeserializerProvider = getProviderMock(connectionMock);

        String messageBody = "{\"references\":[{\"value\":\"1\",\"type\":\"CONNECTION\"}],\"type\":\"MESSAGE\",\"content\":\"{\\\"type\\\":\\\"TCP_MESSAGE\\\",\\\"captureDate\\\":1672527600000,\\\"receiver\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"responder.example.com\\\",\\\"port\\\":1236},\\\"sender\\\":{\\\"ip\\\":\\\"127.0.0.2\\\",\\\"hostname\\\":\\\"initiator.example.com\\\",\\\"port\\\":1235},\\\"content\\\":\\\"Y29udGVudA==\\\"}\"}";
        Item messageItem = new ItemDeserializer().deserialize(messageBody);

        ObjectDeserializer<Object> messageDeserializer = messageDeserializerProvider.createObjectDeserializer(messageItem);

        Reference reference = messageItem.getReferences().get(0);

        assertEquals("1", reference.getValue());
        assertEquals(ConnectionDTO.TYPE, reference.getType());

        assertEquals(Message.TYPE, messageItem.getType());

        RawMessage rawMessage = (RawMessage) messageDeserializer.deserialize(messageItem.getContent(), messageItem);

        //assertEquals(stringToDate("2023-01-01 00:00:00"), rawMessage.getDateReceived());

        assertEquals("127.0.0.2", rawMessage.getFromIP());
        assertEquals(1235, rawMessage.getLocalPort());

        assertEquals("127.0.0.1", rawMessage.getToIP());
        assertEquals(1236, rawMessage.getRemotePort());
    }

    @Test
    public void testDeserializeSyslogMessage() {

        final Connection connectionMock = createConnectionMock();

        ObjectDeserializerProvider messageDeserializerProvider = getProviderMock(connectionMock);

        String messageBody = "{\"references\":[{\"value\":\"1\",\"type\":\"CONNECTION\"}],\"type\":\"MESSAGE\",\"content\":\"{\\\"type\\\":\\\"SYSLOG_MESSAGE\\\",\\\"captureDate\\\":1672527600000,\\\"receiver\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"responder.example.com\\\",\\\"port\\\":1236},\\\"sender\\\":{\\\"ip\\\":\\\"127.0.0.2\\\",\\\"hostname\\\":\\\"initiator.example.com\\\",\\\"port\\\":1235},\\\"content\\\":\\\"NzkgPDg1PjEgMjAxNS0wNi0wOFQxMzo0NjoyOS43NTBaIFRMU3Rlc3QgSUhFX1hEUyAxMjM0IEFUTkFMT0cgLSA8YXRuYT50b3RvPC9hdG5hPg==\\\",\\\"timestamp\\\":\\\"2015-06-08T13:46:29.750Z\\\",\\\"hostName\\\":\\\"TLStest\\\",\\\"appName\\\":\\\"IHE_XDS\\\",\\\"procId\\\":\\\"1234\\\",\\\"messageId\\\":\\\"ATNALOG\\\",\\\"tag\\\":null,\\\"payLoad\\\":\\\"<atna>toto</atna>\\\",\\\"facility\\\":10,\\\"severity\\\":5}\"}";
        Item messageItem = new ItemDeserializer().deserialize(messageBody);

        Reference reference = messageItem.getReferences().get(0);
        assertEquals("1", reference.getValue());
        assertEquals(ConnectionDTO.TYPE, reference.getType());
        assertEquals(Message.TYPE, messageItem.getType());

        ObjectDeserializer<Object> messageDeserializer = messageDeserializerProvider.createObjectDeserializer(messageItem);
        SyslogMessage syslogMessage = (SyslogMessage) messageDeserializer.deserialize(messageItem.getContent(), messageItem);
        //assertEquals(stringToDate("2023-01-01 00:00:00"), syslogMessage.getDateReceived());
        assertEquals("127.0.0.1", syslogMessage.getToIP());
        assertEquals(1236, syslogMessage.getRemotePort());
        assertEquals("127.0.0.2", syslogMessage.getFromIP());
        assertEquals(1235, syslogMessage.getLocalPort());
        assertEquals("2015-06-08T13:46:29.750Z", syslogMessage.getTimestamp());
        assertEquals("TLStest", syslogMessage.getHostName());
        assertEquals("IHE_XDS", syslogMessage.getAppName());
        assertEquals("1234", syslogMessage.getProcId());
        assertEquals("ATNALOG", syslogMessage.getMessageId());
        assertNull(syslogMessage.getTag());
        assertEquals("<atna>toto</atna>", syslogMessage.getPayLoad());
        assertEquals(10, syslogMessage.getFacility().intValue());
        assertEquals(5, syslogMessage.getSeverity().intValue());
    }
    @Test
    public void testDeserializeHttpRequestMessage() {

        final Connection connectionMock = createConnectionMock();

        ObjectDeserializerProvider messageDeserializerProvider = getProviderMock(connectionMock);
        String itemBody = "{\"references\":[{\"value\":\"1\",\"type\":\"CONNECTION\"}],\"type\":\"MESSAGE\",\"content\":\"{\\\"type\\\": \\\"HTTP_REQUEST\\\",\\\"content\\\":null,\\\"captureDate\\\":1695802123671,\\\"sender\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"localhost\\\",\\\"port\\\":46598},\\\"receiver\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"localhost\\\",\\\"port\\\":32783},\\\"method\\\":\\\"PUT\\\",\\\"uri\\\":\\\"http://localhost:32783/sut-mock/rest/pixm/Patient?identifier=urn:oid:1.2.3.4%7Corg-test\\\",\\\"version\\\":\\\"HTTP/1.1\\\",\\\"headers\\\":{\\\"Content-Length\\\":\\\"182\\\",\\\"Content-Type\\\":\\\"application/json\\\"},\\\"body\\\":\\\"eyJyZXNvdXJjZVR5cGUiOiJQYXRpZW50Iiwib2JqZWN0Ijp7ImlkZW50aWZpZXIiOnsic3lzdGVtIjoidXJuOm9pZDoxLjIuMy40IiwidmFsdWUiOiJvcmctdGVzdCJ9LCJuYW1lIjp7ImZhbWlseSI6Ikpob24iLCJnaXZlbiI6Ik1hcnRpbiJ9LCJnZW5kZXIiOiJNYWxlIiwiYmlydGhEYXRlIjoiMTk5OS0wNy0wMSJ9fQo=\\\"}\"}";

        Item messageItem = new ItemDeserializer().deserialize(itemBody);

        Reference reference = messageItem.getReferences().get(0);
        assertEquals("1", reference.getValue());
        assertEquals(ConnectionDTO.TYPE, reference.getType());
        assertEquals(Message.TYPE, messageItem.getType());

        ObjectDeserializer<Object> messageDeserializer = messageDeserializerProvider.createObjectDeserializer(messageItem);

        HTTPMessage httpMessage = (HTTPMessage) messageDeserializer.deserialize(messageItem.getContent(), messageItem);
        assertEquals("127.0.0.1", httpMessage.getToIP());
        assertEquals(46598, httpMessage.getRemotePort());
        assertEquals("127.0.0.1", httpMessage.getFromIP());
        assertEquals(32783, httpMessage.getLocalPort());
        assertEquals(createHeaderAsString(), httpMessage.getHeadersAsString());
        assertEquals(createMessageAsString(), httpMessage.getMessageReceivedAsString());

    }

    @Test
    public void testDeserializeHl7V2(){
        final Connection connectionMock = createConnectionMock();

        ObjectDeserializerProvider messageDeserializerProvider = getProviderMock(connectionMock);
        String itemBody = "{\"references\":[{\"value\":\"1\",\"type\":\"CONNECTION\"}],\"type\":\"MESSAGE\",\"content\":\"{\\\"type\\\":\\\"HL7V2_MESSAGE\\\",\\\"captureDate\\\":1672527600000,\\\"receiver\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"responder.example.com\\\",\\\"port\\\":1236},\\\"sender\\\":{\\\"ip\\\":\\\"127.0.0.1\\\",\\\"hostname\\\":\\\"initiator.example.com\\\",\\\"port\\\":1235},\\\"hl7MessageType\\\":\\\"ADT^A01^ADT_A01\\\", \\\"hl7version\\\":\\\"2.3.1\\\"}\\\"content\\\":\\\"MSH|^~\\\\&|Gazelle|Gazelle|Gazelle|Gazelle|20230101000000||ORM^O01|1|P|2.3.1||||||8859/1\\\\r\\\\nPID|||urn:oid:1234567890|org-test|Jhon^Martin||19990701|M\\\\r\\\\n\\\",}\"}";

        Item messageItem = new ItemDeserializer().deserialize(itemBody);

        Reference reference = messageItem.getReferences().get(0);
        assertEquals("1", reference.getValue());
        assertEquals(ConnectionDTO.TYPE, reference.getType());
        assertEquals(Message.TYPE, messageItem.getType());

        ObjectDeserializer<Object> messageDeserializer = messageDeserializerProvider.createObjectDeserializer(messageItem);

        HL7v2Message hl7v2Message = (HL7v2Message) messageDeserializer.deserialize(messageItem.getContent(), messageItem);

  //      assertEquals(stringToDate("2023-01-01 00:00:00"), hl7v2Message.getDateReceived());
        assertEquals("127.0.0.1", hl7v2Message.getToIP());
        assertEquals(1236, hl7v2Message.getRemotePort());
        assertEquals("127.0.0.1", hl7v2Message.getFromIP());
        assertEquals(1235, hl7v2Message.getLocalPort());
        assertEquals("ADT^A01^ADT_A01", hl7v2Message.getHl7MessageType());
        assertEquals("2.3.1", hl7v2Message.getHl7Version());
    }

    private String createHeaderAsString(){
        return "PUT http://localhost:32783/sut-mock/rest/pixm/Patient?identifier=urn:oid:1.2.3.4%7Corg-test HTTP/1.1\r\n" +
                "Content-Length: 182\r\n" +
                "Content-Type: application/json"+ "\r\n";
    }

    private String createMessageAsString() {
        return "{\"resourceType\":\"Patient\",\"object\":{\"identifier\":{\"system\":\"urn:oid:1.2.3.4\",\"value\":\"org-test\"},\"name\":{\"family\":\"Jhon\",\"given\":\"Martin\"},\"gender\":\"Male\",\"birthDate\":\"1999-07-01\"}}\n";
    }
    private Connection createConnectionMock(){
        Connection connectionMock = new Connection();
        connectionMock.setId(1);
        connectionMock.setInitiatorHostname("initiator.example.com");
        connectionMock.setInitiatorPort(1235);
        connectionMock.setResponderHostname("responder.example.com");
        connectionMock.setResponderPort(1236);
        connectionMock.setProxyChannelPort(1234);
        return connectionMock;
    }

    private ObjectDeserializerProvider getProviderMock(final Connection connectionMock){
        return new ObjectDeserializerProvider() {
            @Override
            public ObjectDeserializer<AbstractMessage> createObjectDeserializer(Item item) {
                return new MessageDeserializer(new ConnectionProvider() {
                    @Override
                    public Connection getConnection(int id) {
                        return connectionMock;
                    }
                });
            }
        };
    }

    // create date from string in the format yyyy-MM-dd HH:mm:ss
    private Date stringToDate(String dateString) {
        try {
            return SIMPLE_DATE_FORMAT.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


}
